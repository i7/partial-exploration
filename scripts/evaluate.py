#!/usr/bin/env python3
import argparse
import csv
import json
import logging
import pathlib
import sys
from typing import List

import yaml

from evaluate.model import ModelType, PrismModel
from evaluate.process import Outcome, Instance, evaluate
from evaluate.query import Query, ValueQuery, QueryType
from evaluate.solver.loader import load_solvers
from evaluate.util import YamlIncludeLoader, flatten_lists, print_results

log = logging.getLogger(__name__)
project_root = pathlib.Path(__file__).parent.parent


def with_progress(iterable):
    try:
        import progressbar

        length = max(map(len, map(str, iterable))) + 1

        experiment_name = progressbar.Variable(
            "experiment", format="Experiment: {formatted_value}", width=length
        )
        widgets = [
            experiment_name,
            " -- ",
            progressbar.Percentage(),
            " (",
            progressbar.Counter(),
            f" of {len(iterable)})",
            " [",
            progressbar.Timer(format="%(elapsed)s"),
            "] ",
            progressbar.Bar(),
            " (",
            progressbar.ETA(),
            ") ",
        ]

        progressbar = progressbar.ProgressBar(
            min_value=0, max_value=len(iterable), widgets=widgets
        )
        for element in iterable:
            progressbar.update(experiment=str(element))
            yield element
            progressbar.increment()
    except ImportError:
        width = 0
        length = len(iterable)
        while length:
            width += 1
            length //= 10
        if not width:
            width = 1
        for i, element in enumerate(iterable):
            print(f"Experiment {i:{width}d}/{len(iterable):{width}d}: {element}")
            yield element


def do(args):
    if not args.models.exists():
        sys.exit(f"No file found at {args.models}")
    with args.models.open(mode="rt") as f:
        models_data = flatten_lists(yaml.load(f, YamlIncludeLoader), [])
    if not args.solvers.exists():
        sys.exit(f"No file found at {args.solvers}")
    with args.solvers.open(mode="rt") as f:
        solvers_data = yaml.load(f, YamlIncludeLoader)

    solvers = load_solvers(solvers_data)
    if args.solver_name:
        solvers = [solver for solver in solvers if solver.key in args.solver_name]

    outcomes_by_key = {}
    if args.store and args.replace:
        with args.store.open("rt") as f:
            for d in json.load(f):
                outcome = Outcome.parse(d)
                key = outcome.unique_key()
                assert key not in outcomes_by_key
                outcomes_by_key[key] = outcome

    if not solvers:
        sys.exit("No solvers found")
    if args.solver_name:
        for name in args.solver_name:
            if not any(solver.key == name for solver in solvers):
                log.warning("Did not find any solver for name %s", name)
    log.info("Using solvers: %s", ", ".join(solver.key for solver in solvers))

    filters = []
    if args.query_type:
        query_set = set(args.query_type)
        filters.append(lambda i: i.query.query_type() in query_set)
    if args.exclude_tag:
        exclude_set = set(args.exclude_tag)
        filters.append(lambda i: all(tag not in exclude_set for tag in i.tags))
    if args.include_tag:
        include_set = set(args.include_tag)
        filters.append(lambda i: any(tag in include_set for tag in i.tags))
    if args.require_tag:
        filters.append(lambda i: all(tag in i.tags for tag in args.require_tag))
    if args.model_name:
        filters.append(
            lambda i: any(name_filter in i.model.key for name_filter in args.model_name)
        )
    if args.restrict_csv:
        key_map = dict()
        for c in args.restrict_csv:
            with c.open(mode="rt") as f:
                for s, model_key, query_key in csv.reader(f):
                    setting = str(s).lower()
                    if setting == "y":
                        include = True
                    elif setting == "n":
                        include = False
                    else:
                        raise ValueError(f"Unknown setting {s}")
                    key = (model_key, query_key)
                    if key in key_map:
                        if include != key_map[key]:
                            raise ValueError(
                                f"Instance {key} both included and excluded"
                            )
                    else:
                        key_map[key] = include

        def check_instance(instance_: Instance):
            instance_key = (instance_.model.key, instance_.query.key)
            if instance_key not in key_map:
                raise KeyError(f"Instance {instance_} not mentioned in csv")
            return key_map[instance_key]

        filters.append(check_instance)

    instances = []
    base_path = args.models.parent
    for description in models_data:
        tags = description.get("tags", [])
        model = description["model"]
        model_relative_path = model["path"]
        model_path: pathlib.Path = (base_path / model_relative_path).resolve()
        if not model_path.exists():
            sys.exit(f"Model {model_relative_path} does not exist")
        queries = description.get("queries", [])
        if not queries:
            log.debug("Model %s has no valid queries", model_relative_path)
            continue

        model_class = model.get("class", None)
        if model_class is None:
            if model_path.suffix == ".prism":
                model_class = "prism"

        if model_class is None:
            sys.exit(f"Could not determine class of {model_path}")

        model_type = ModelType.by_key(model.get("type", None))

        if model_class == "prism":
            if "properties" in model:
                properties_path = (base_path / model["properties"]).resolve()
                if not properties_path.exists():
                    sys.exit(f"Query file {properties_path} does not exist")
            else:
                properties_path = None

            if model_type is None:
                model_type = PrismModel.detect_model_type(model_path)
            if model_type is None:
                sys.exit(f"Failed to determine model type of {model_path}")

            constants = model.get("const", {})
            assert isinstance(constants, dict), model_path
            model = PrismModel(
                model_file=model_path,
                model_type=model_type,
                constants=constants,
                properties_file=properties_path,
            )
        else:
            sys.exit(f"Unknown model class {model_class}")

        tags.append(model.model_type.value)

        for query_description in queries:
            query_tags = query_description.get("tags", [])
            spec = query_description.get("spec", dict())
            query = Query.parse(query_description["type"], spec)
            query_tags.append(query.query_type.value)
            if isinstance(query, ValueQuery) and query.expected_value is not None:
                query_tags.append("has_reference")

            query_instances = []
            all_supported = True
            for solver in solvers:
                if not solver.is_supported(model, query):
                    all_supported = False
                    if args.only_fully_supported:
                        break
                    continue
                instance = Instance(
                    solver=solver,
                    model=model,
                    query=query,
                    tags=frozenset(tags + query_tags),
                )
                if all(f(instance) for f in filters):
                    query_instances.append(instance)
            if not args.only_fully_supported or all_supported:
                instances.extend(query_instances)

    if not instances:
        print("No instances selected!")
        sys.exit()

    outcomes: List[Outcome] = []
    for instance in instances if args.quiet else with_progress(instances):
        outcomes.append(
            Outcome.of(
                instance,
                evaluate(instance.solver, instance.model, instance.query, args),
            )
        )

    if args.store:
        for result in outcomes:
            outcomes_by_key[result.unique_key()] = result

        with args.store.open(mode="wt") as f:
            json.dump(
                [
                    o.serialize()
                    for o in sorted(
                        outcomes_by_key.values(), key=lambda o: o.sort_key()
                    )
                ],
                f,
            )
    else:
        print()
        print()

        print_results(outcomes)

    if args.validate and not all(outcome.is_valid() for outcome in outcomes):
        sys.exit("Validation failed")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--models",
        type=pathlib.Path,
        help="Path to the models file",
        default=project_root / "data" / "models.yml",
    )
    parser.add_argument(
        "--solvers",
        type=pathlib.Path,
        help="Path to the solvers file",
        default=project_root / "data" / "solvers.yml",
    )
    parser.add_argument(
        "--timeout", type=float, help="Timeout (in seconds)", default=30.0
    )
    parser.add_argument(
        "--precision", type=float, help="Precision requirement", default=1e-6
    )
    parser.add_argument("--relative", action="store_true", help="Ensure relative error")

    parser.add_argument(
        "--exclude-tag",
        type=str,
        action="append",
        help="Model and query tags to exclude",
    )
    parser.add_argument(
        "--include-tag",
        type=str,
        action="append",
        help="Model and query tags to include",
    )
    parser.add_argument(
        "--require-tag",
        type=str,
        action="append",
        help="Model and query tags to require",
    )
    parser.add_argument(
        "--query-type",
        type=QueryType,
        action="append",
        help="Type of query to test",
    )
    parser.add_argument(
        "--model",
        type=str,
        dest="model_name",
        action="append",
        help="Model names to test",
    )
    parser.add_argument(
        "--solver",
        type=str,
        dest="solver_name",
        action="append",
        help="Solvers to invoke",
    )
    parser.add_argument("--memory-limit", type=int, help="Memory limit (in megabytes)")

    logging_args = parser.add_mutually_exclusive_group()
    logging_args.add_argument(
        "--quiet",
        action="store_true",
        help="Be quiet",
    )
    logging_args.add_argument(
        "--debug",
        action="store_true",
        help="Increase logging",
    )

    validate_or_store = parser.add_mutually_exclusive_group()
    validate_or_store.add_argument(
        "--validate",
        action="store_true",
        help="Validate results (only considering those with expected value)",
    )
    validate_or_store.add_argument(
        "--store",
        type=pathlib.Path,
        help="Store results (to compare with future runs)",
    )

    parser.add_argument(
        "--only-fully-supported",
        action="store_true",
        help="Only consider models that are supported by all enabled solvers",
    )
    parser.add_argument(
        "--replace",
        action="store_true",
        help=argparse.SUPPRESS,
    )
    parser.add_argument(
        "--restrict-csv",
        type=pathlib.Path,
        action="append",
        help="Only consider that are marked 'y' in the given csv(s)",
    )
    parser.add_argument(
        "--java-gc",
        type=str,
        default="none",
        choices=["Serial", "Parallel", "G1", "Z", "MarkSweepCompact", "none"],
        help=argparse.SUPPRESS,
    )

    arguments = parser.parse_args()

    if arguments.quiet:
        level = logging.ERROR
    elif arguments.debug:
        level = logging.DEBUG
    else:
        level = logging.INFO
    logging.basicConfig(level=level)
    if level == logging.DEBUG:
        logging.getLogger("docker").setLevel(logging.INFO)
        logging.getLogger("urllib3").setLevel(logging.INFO)

    do(arguments)
