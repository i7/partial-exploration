import abc
import argparse
import dataclasses
import json
import logging
import re
import shutil
import sys
import yaml
from pathlib import Path
from typing import List, Dict, Optional, Any, Set, Callable

log = logging.getLogger(__name__)


@dataclasses.dataclass
class FilterArgs(object):
    model_data: Dict
    constants: Dict[str, Any]
    model_size: Optional[int]
    instance_data: Dict
    property_name: str
    expected_value: Any
    prism_file: str
    prism_props: str


class TagFilter(abc.ABC):
    @abc.abstractmethod
    def match(self, args: FilterArgs, tag):
        pass

    def __and__(self, other):
        assert isinstance(other, TagFilter)
        return AndFilter([self, other])

    def __or__(self, other):
        assert isinstance(other, TagFilter)
        return AndFilter([self, other])

    def __invert__(self):
        return NotFilter(self)


@dataclasses.dataclass
class AndFilter(TagFilter):
    filters: List[TagFilter]

    def match(self, args, tag):
        return all(f.match(args, tag) for f in self.filters)

    def __and__(self, other):
        if isinstance(other, AndFilter):
            return AndFilter(self.filters + other.filters)
        return AndFilter(self.filters + other)


@dataclasses.dataclass
class OrFilter(TagFilter):
    filters: List[TagFilter]

    def match(self, args, tag):
        return any(f.match(args, tag) for f in self.filters)

    def __or__(self, other):
        if isinstance(other, OrFilter):
            return AndFilter(self.filters + other.filters)
        return AndFilter(self.filters + other)


@dataclasses.dataclass
class NotFilter(TagFilter):
    filter: TagFilter

    def match(self, args, tag):
        return not self.filter.match(args, tag)

    def __invert__(self):
        assert self.filter


@dataclasses.dataclass
class ForTagFilter(TagFilter):
    tags: Set[str]
    filter: TagFilter

    def match(self, args, tag):
        return tag not in self.tags or self.filter.match(args, tag)


@dataclasses.dataclass
class ModelNameFilter(TagFilter):
    name: re.Pattern

    def match(self, args, tag):
        return self.name.match(args.model_data["short"])


@dataclasses.dataclass
class SizeFilter(TagFilter):
    max_size: int

    def match(self, args, tag):
        return args.model_size <= self.max_size


@dataclasses.dataclass
class PrismFilter(TagFilter):
    prism_file: re.Pattern
    constants: Dict[str, Callable[[Any], bool]]
    property: re.Pattern

    @staticmethod
    def of(name=None, const=None, prop=None):
        return PrismFilter(
            prism_file=re.compile(name) if name is not None else re.compile(r".*"),
            constants=const if const is not None else {},
            property=re.compile(prop) if prop is not None else re.compile(r".*"),
        )

    def match(self, args, tag):
        return (
            self.prism_file.match(args.prism_file)
            and self.property.match(args.property_name)
            and all(r(args.constants[c]) for c, r in self.constants.items())
        )


@dataclasses.dataclass
class ValueTypeFilter(TagFilter):
    t: type

    def match(self, args, tag):
        return isinstance(args.expected_value, self.t)


tag_filter = ForTagFilter(
    {"ci-s", "ci-g"},
    AndFilter(
        [
            SizeFilter(5000),
            ForTagFilter({"ci-s"}, ~ValueTypeFilter(bool)),
            ForTagFilter({"ci-s"}, ~PrismFilter.of(name=r"^consensus\..*")),
            ~PrismFilter.of(name=r"^consensus\..*", const={"K": lambda x: x >= 8}),
            ~PrismFilter.of(name=r"^leader_sync\.\d-3\..*", prop=r"eventually_elected"),
            ~PrismFilter.of(name=r"^firewire\.false\..*"),
        ]
    ),
)


def run(args):
    path: Path = args.input
    folders: List[Path] = [path / f for f in ("ctmc", "dtmc", "mdp")]
    if not all(f.exists() for f in folders):
        sys.exit(f"{path} does not look like a QVBS directory")

    sub_path: Path = args.sub_path
    if sub_path.is_absolute():
        sys.exit("Need relative path for sub_path")

    base_path = Path("qvbs_export")

    models = []
    files_for_copy = {}

    for type_folder in folders:
        for model_dir in type_folder.iterdir():
            if not model_dir.is_dir():
                continue
            index = model_dir / "index.json"
            if not index.exists():
                continue
            with index.open(mode="rt") as f:
                model_class_data = json.load(f)
            if "PRISM" not in model_class_data["original"]:
                continue
            properties = []
            for prop in model_class_data.get("properties", []):
                if prop.get("type", "") != "prob-reach":
                    continue
                properties.append(("reach", prop["name"]))

            instances = []
            model_relative_path = sub_path / type_folder.name / model_dir.name

            for file in model_class_data["files"]:
                if "original-file" not in file:
                    continue
                prism_file, props_file = file["original-file"]

                if (
                    not (model_dir / prism_file).exists()
                    or not (model_dir / props_file).exists()
                ):
                    log.warning("%s / %s missing files", model_dir.name, file["file"])
                    continue
                files_for_copy[model_dir / prism_file] = (
                    model_relative_path / prism_file
                )
                files_for_copy[model_dir / props_file] = (
                    model_relative_path / props_file
                )
                for parameters in file["open-parameter-values"]:
                    constants = {
                        v["name"]: v["value"] for v in parameters.get("values", {})
                    }
                    states = None
                    if "states" in parameters:
                        states = parameters["states"]
                        if isinstance(states, list):
                            if not states:
                                continue
                            states = states[0]
                        if isinstance(states, dict):
                            states = states["number"]

                        if states == "∞":
                            states = None

                    values_by_name = dict()
                    if "results" in parameters:
                        for result in parameters["results"]:
                            value = result["value"]
                            if value == "∞":
                                continue
                            if isinstance(value, dict):
                                if "approx" in value:
                                    value = value["approx"]
                                elif "num" in value and "den" in value:
                                    value = value["num"] / value["den"]
                                elif "lower" in value and "upper" in value:
                                    continue
                            if not (
                                isinstance(value, bool)
                                or isinstance(value, float)
                                or isinstance(value, int)
                            ):
                                log.warning(
                                    "Weird value %s in %s/%s",
                                    result["value"],
                                    prism_file,
                                    constants,
                                )
                                continue
                            values_by_name[result["property"]] = value
                    else:
                        log.debug("No results for %s/%s", prism_file, constants)
                    model_queries = []
                    for prop_type, prop_name in properties:
                        if prop_type == "reach":
                            query_data = {
                                "type": "reach",
                                "spec": {"property": prop_name},
                            }
                            tags = []
                            if prop_name in values_by_name:
                                value = values_by_name[prop_name]
                                query_data["spec"]["expected"] = value
                                for tag in ["ci-s", "ci-g"]:
                                    if tag_filter.match(
                                        FilterArgs(
                                            model_data=model_class_data,
                                            instance_data=file,
                                            model_size=states,
                                            constants=constants,
                                            property_name=prop_name,
                                            expected_value=value,
                                            prism_file=prism_file,
                                            prism_props=props_file,
                                        ),
                                        tag,
                                    ):
                                        tags.append(tag)
                            if tags:
                                query_data["tags"] = tags
                            model_queries.append(query_data)
                        else:
                            raise AssertionError

                    model_data = {
                        "model": {
                            "path": str(model_relative_path / prism_file),
                            "properties": str(model_relative_path / props_file),
                        },
                    }
                    if model_queries:
                        model_data["queries"] = model_queries
                    if constants:
                        model_data["model"]["const"] = constants
                    if states is not None:
                        model_data["model"]["states"] = states
                    model_data["model"]["type"] = model_class_data["type"]
                    model_data["model"]["class"] = "prism"

                    instances.append(model_data)
            models.extend(instances)

    base_path.mkdir(parents=True, exist_ok=True)
    with (base_path / "models.yml").open("wt") as f:
        yaml.dump(models, f)
    if not args.only_description:
        for source, rel_dest in files_for_copy.items():
            destination = base_path / rel_dest
            destination.parent.mkdir(parents=True, exist_ok=True)
            shutil.copy(source, destination)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    sys.set_int_max_str_digits(1000000)
    parser = argparse.ArgumentParser()
    parser.add_argument("input", type=Path, help="Path to the QVBS benchmark folder")
    parser.add_argument("sub_path", type=Path, help="Path where to put the files")
    parser.add_argument(
        "--only-description", action="store_true", help="Only write the .yml"
    )
    run(parser.parse_args())
