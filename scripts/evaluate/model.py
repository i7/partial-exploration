import abc
import dataclasses
import pathlib
from typing import Dict, Optional, Any

from .base import KeyedEnum


class ModelType(KeyedEnum):
    DTMC = "dtmc"
    CTMC = "ctmc"
    MDP = "mdp"
    CTMDP = "ctmdp"
    SMG = "smg"

    def __lt__(self, other):
        return self.key < other.key

    def __str__(self):
        return self.value


@dataclasses.dataclass(frozen=True)
class Model(abc.ABC):
    model_type: ModelType

    @property
    @abc.abstractmethod
    def key(self):
        pass

    @abc.abstractmethod
    def relevant_files(self):
        pass

    def __str__(self):
        return self.key


@dataclasses.dataclass(frozen=True)
class PrismModel(Model):
    model_file: pathlib.Path
    constants: Dict[str, Any]
    properties_file: Optional[pathlib.Path]

    @staticmethod
    def detect_model_type(model_file: pathlib.Path):
        with model_file.open(mode="rt") as f:
            for line in f.readlines():
                if line.strip() == "dtmc":
                    return ModelType.DTMC
                if line.strip() == "ctmc":
                    return ModelType.CTMC
                if line.strip() == "mdp":
                    return ModelType.MDP
                if line.strip() == "smg":
                    return ModelType.SMG
        raise ValueError(f"Could not determine model type of {model_file}")

    @staticmethod
    def _format_constant(v):
        if isinstance(v, bool):
            return str(v).lower()
        return str(v)

    def constants_string(self):
        return ",".join(
            f"{k}={PrismModel._format_constant(v)}"
            for k, v in sorted(self.constants.items())
        )

    @property
    def key(self):
        if self.constants:
            return f"{self.model_file.stem}." + ".".join(
                f"{k}={PrismModel._format_constant(v)}"
                for k, v in sorted(self.constants.items())
            )
        return self.model_file.name

    def relevant_files(self):
        return (
            [self.model_file]
            if self.properties_file is None
            else [self.model_file, self.properties_file]
        )
