import abc
import dataclasses
import logging
import subprocess
import sys
import time
from typing import Dict, Optional, Any, Union

from .model import Model, ModelType
from .query import Query, QueryType
from .solver.base import SolverOutput, Solver, Validation, SolverInvocation


log = logging.getLogger(__name__)


@dataclasses.dataclass
class ProcessResult(abc.ABC):
    output: Union[str, bytes]
    error: Union[str, bytes]
    time: float

    @abc.abstractmethod
    def message(self) -> str:
        pass

    @staticmethod
    def parse(data):
        cls = {"timeout": Timeout, "error": Error, "success": Success}[data["type"]]
        args: Dict[str, Any] = cls._parse(data)
        args.update(
            {"output": data["stdout"], "error": data["stderr"], "time": data["time"]}
        )
        return cls(**args)

    @abc.abstractmethod
    def _serialize(self):
        pass

    @staticmethod
    @abc.abstractmethod
    def _parse(data):
        pass

    def serialize(self):
        data: Dict = self._serialize()
        # noinspection PyTypeChecker
        data["type"] = {Timeout: "timeout", Error: "error", Success: "success"}[
            type(self)
        ]
        data.update({"stdout": self.output, "stderr": self.error, "time": self.time})
        return data


@dataclasses.dataclass
class Timeout(ProcessResult):
    def message(self) -> str:
        return "Timeout"

    def __str__(self):
        return "Timeout"

    def _serialize(self):
        return {}

    @staticmethod
    def _parse(data):
        return {}


@dataclasses.dataclass
class Error(ProcessResult):
    code: int
    error_type: str

    def message(self) -> str:
        return self.error_type

    def __str__(self):
        return f"Error({self.error_type},{self.code})"

    def _serialize(self):
        return {"code": self.code, "error_type": self.error_type}

    @staticmethod
    def _parse(data):
        return {"code": data["code"], "error_type": data["error_type"]}


@dataclasses.dataclass
class Success(ProcessResult):
    solver_output: SolverOutput

    def message(self) -> str:
        return self.solver_output.message

    def __str__(self):
        return f"Success({self.time:.2f})"

    def _serialize(self):
        return {"solver_output": self.solver_output.serialize()}

    @staticmethod
    def _parse(data):
        return {"solver_output": SolverOutput.parse(data["solver_output"])}


@dataclasses.dataclass(frozen=True)
class Instance(object):
    model: Model
    query: Query
    solver: Solver
    tags: frozenset[str]

    def sort_key(self):
        return (
            self.query.query_type.key,
            self.model.key,
            self.query.name,
            self.solver.key,
        )

    def __str__(self):
        return f"{self.solver} / {self.model} / {self.query}"


@dataclasses.dataclass
class Outcome(object):
    model_key: str
    query_key: str
    solver_key: str
    query_type: QueryType
    model_type: ModelType
    result: ProcessResult
    tag: Optional[str]

    @staticmethod
    def of(instance: Instance, result: ProcessResult, tag: Optional[str] = None):
        return Outcome(
            model_type=instance.model.model_type,
            model_key=instance.model.key,
            query_type=instance.query.query_type,
            query_key=instance.query.key,
            solver_key=instance.solver.key,
            result=result,
            tag=tag,
        )

    @staticmethod
    def parse(data, tag: Optional[str] = None):
        return Outcome(
            model_type=ModelType.by_key(data["model_type"]),
            model_key=data["model"],
            query_type=QueryType.by_key(data["query_type"]),
            query_key=data["query"],
            solver_key=data["solver"],
            result=ProcessResult.parse(data["result"]),
            tag=tag,
        )

    def tagged_solver(self):
        if self.tag:
            return f"{self.solver_key}@{self.tag}"
        return self.solver_key

    def serialize(self):
        return {
            "model_type": self.model_type.key,
            "model": self.model_key,
            "query_type": self.query_type.key,
            "query": self.query_key,
            "solver": self.solver_key,
            "result": self.result.serialize(),
        }

    def __lt__(self, other):
        assert isinstance(other, Outcome)
        return self.sort_key() < other.sort_key()

    def sort_key(self):
        return self.query_type.key, self.model_key, self.query_key, self.solver_key

    def unique_key(self):
        return self.model_key, self.query_key, self.solver_key

    def is_valid(self):
        return isinstance(
            self.result, Success
        ) and self.result.solver_output.validation in {
            Validation.Correct,
            Validation.Unknown,
        }


def evaluate(solver: Solver, model: Model, query: Query, args) -> ProcessResult:
    kwargs = {}
    if args.java_gc != "none":
        kwargs["java_gc"] = args.java_gc
    invocation: SolverInvocation = solver.create_invocation(
        model, query, args.memory_limit, args.validate, **kwargs
    )
    log.debug(
        "Solver %s on %s / %s: %s", solver, model, query, " ".join(invocation.execution)
    )

    timeout = False
    if solver.docker_image:
        import requests.exceptions
        import docker
        import docker.errors
        import docker.models.containers

        client = docker.from_env()
        try:
            image = client.images.get(solver.docker_image)
        except docker.errors.ImageNotFound:
            image = client.images.pull(solver.docker_image)
        cpu_count = 1

        environment = invocation.env
        docker_args = dict()
        if cpu_count:
            docker_args.update({"cpu_period": 100000, "cpu_quota": cpu_count * 100000})

        if args.memory_limit:
            docker_args["mem_limit"] = args.memory_limit * 1024 * 1024

        container: docker.models.containers.Container = client.containers.create(
            image=image,
            command=invocation.execution,
            detach=True,
            environment=environment,
            memswap_limit=-1,
            volumes={
                str(f): {"bind": str(f), "mode": "ro"} for f in model.relevant_files()
            },
            **docker_args,
        )
        return_code = None
        timestamp = time.time()
        try:
            container.start()
            result = container.wait(timeout=args.timeout)
            return_code = result["StatusCode"]
        except (requests.exceptions.Timeout, requests.exceptions.ConnectionError):
            try:
                container.kill()
            except docker.errors.APIError:
                pass
            timeout = True
        finally:
            runtime = time.time() - timestamp
            stdout, stderr = (
                container.logs(stdout=True, stderr=False),
                container.logs(stdout=False, stderr=True),
            )
            try:
                container.remove(force=True)
            except docker.errors.APIError:
                pass
    else:
        timestamp = time.time()
        process = subprocess.Popen(
            invocation.execution,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
            env=invocation.env,
        )
        try:
            stdout, stderr = process.communicate(timeout=args.timeout)
        except subprocess.TimeoutExpired:
            timeout, return_code = True, None
            process.kill()
            process.wait()
            stdout, stderr = process.stdout.read(), process.stderr.read()
        finally:
            runtime = time.time() - timestamp

        return_code = process.returncode

    try:
        if isinstance(stdout, bytes):
            stdout = stdout.decode()
        if isinstance(stderr, bytes):
            stderr = stderr.decode()
    except UnicodeDecodeError:
        log.warning("Failed to decode output of %s on %s", solver, model)
        stdout, stderr = "", ""

    if timeout:
        return Timeout(stdout, stderr, runtime)
    error = solver.check_error(stdout, stderr, return_code, model, query)
    if return_code:
        sys.stderr.flush()
        log.error("Unrecognized error:\n%s\n====\n%s", stdout, stderr)
        sys.stderr.flush()
        error = "generic"
    if error:
        return Error(stdout, stderr, runtime, return_code, "generic")
    output = solver.validate_output(stdout, stderr, model, query)
    if output is None:
        return Error(stdout, stderr, runtime, return_code, "no_result")

    return Success(
        output=stdout,
        error=stderr,
        time=runtime,
        solver_output=output,
    )
