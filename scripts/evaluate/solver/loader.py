from .base import SimpleSolver
from .pet import PetSolver, PetReachAsMpSolver
from .prism import PrismSolver
from .storm import StormSolver


def load_solvers(data):
    solvers = []
    for solver in data:
        if solver["type"] == "pet":
            conf = solver["conf"]
            solvers.append(
                PetSolver(
                    **SimpleSolver.get_default_kwargs(solver),
                    sampling=conf.get("sampling", True),
                )
            )
        elif solver["type"] == "pet-reach-mp":
            conf = solver["conf"]
            solvers.append(
                PetReachAsMpSolver(
                    **SimpleSolver.get_default_kwargs(solver),
                    sampling=conf.get("sampling", True),
                )
            )
        elif solver["type"] == "prism":
            conf = solver["conf"]
            solvers.append(
                PrismSolver(
                    **SimpleSolver.get_default_kwargs(solver),
                    explicit=conf.get("explicit", False),
                )
            )
        elif solver["type"] == "storm":
            conf = solver["conf"]
            solvers.append(
                StormSolver(
                    **SimpleSolver.get_default_kwargs(solver),
                    engine=conf.get("engine", None),
                    ddlib=conf.get("ddlib", None),
                )
            )
        else:
            raise ValueError(solver["type"])
    return solvers
