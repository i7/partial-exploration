import dataclasses
import logging
from typing import Optional

from .base import SimpleSolver, SolverInvocation, SolverOutput
from .util import get_java_error
from ..model import Model, PrismModel, ModelType
from ..query import Query, Reachability, ValueQuery


log = logging.getLogger(__name__)


def get_prism_error(output):
    java = get_java_error(output)
    if java:
        return java
    if "Iterative method did not converge" in output:
        return "convergence"
    if "Error: Not enough extra action variables preallocated" in output:
        return "cudd"
    if "Error: Syntax error" in output:
        return "syntax"
    if "java.lang.UnsupportedOperationException: Query" in output:
        return "unsupported"
    return None


@dataclasses.dataclass(frozen=True)
class PrismSolver(SimpleSolver):
    explicit: bool

    def is_supported(self, model: Model, query: Query) -> bool:
        return isinstance(query, Reachability) and (
            self.explicit or model.model_type != ModelType.SMG
        )

    def create_invocation(
        self,
        model: Model,
        query: Query,
        memory_limit: Optional[int],
        validate: bool,
        **kwargs,
    ) -> SolverInvocation:
        assert isinstance(model, PrismModel)
        model: PrismModel
        execution = [str(self.path)]

        execution += [str(model.model_file), str(model.properties_file)]
        if model.constants:
            execution += ["--const", model.constants_string()]
        execution += ["--intervaliter"]
        execution += ["-maxiters", "1000000"]
        if memory_limit is not None:
            execution += [
                "-javamaxmem",
                f"{memory_limit - 128}m",
                "-cuddmaxmem",
                f"{memory_limit // 2}m",
                "-javastack",
                "128m",
            ]
        if "java_gc" in kwargs:
            execution += ["-javaparams", f"-XX:+Use{kwargs.get('java_gc')}GC"]

        execution += ["--epsilon", str(self.precision)]
        if self.relative_error:
            execution.append("--relative")
        if self.explicit:
            execution.append("--explicit")
        else:
            execution += ["-ddextraactionvars", "64"]

        if isinstance(query, Reachability):
            execution += ["--property", query.property_name]
        else:
            raise AssertionError()

        env = {}
        if kwargs.get("java_gc", None):
            env["PRISM_JAVA_PARAMS"] = f"-XX:+Use{kwargs['java_gc']}GC"
        return SolverInvocation(execution, env)

    def check_error(self, stdout, stderr, return_code, model: Model, query: Query):
        return get_prism_error(stdout + "\n" + stderr)

    def validate_output(
        self, stdout, stderr, model: Model, query: Query
    ) -> Optional[SolverOutput]:
        query: ValueQuery

        value_str = None
        for line in stdout.splitlines(keepends=False):
            if line.startswith("Value in the initial state: "):
                length = len("Value in the initial state: ")
                value_str = line[length:]
                break
            if line.startswith("Result: "):
                value_str = line[len("Result: ")]
                break
        if value_str is None:
            log.warning("No output found in:\n%s", stdout)
            return None
        return SolverOutput.default(
            query, value_str, self.precision, self.relative_error
        )
