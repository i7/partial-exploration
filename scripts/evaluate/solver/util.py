import logging

log = logging.getLogger(__name__)


def get_java_error(output):
    if "java.lang.StackOverflowError" in output:
        return "stackoverflow"
    if "java.lang.OutOfMemoryError" in output:
        return "memory"
    if "ArrayIndexOutOfBounds" in output:
        return "array"
    if "NegativeArraySizeException" in output:
        return "memory"
    if "IllegalArgumentException" in output:
        log.debug("Internal error:\n%s", output)
        return "internal"
    if "java.lang.AssertionError" in output:
        log.info("Assertion error:\n%s", output)
        return "assertion"
    return None
