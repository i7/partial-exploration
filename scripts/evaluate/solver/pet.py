import dataclasses
import json
import os
from typing import Optional

from .base import SimpleSolver, SolverInvocation, SolverOutput, Validation, compare
from .util import get_java_error
from ..model import Model, PrismModel
from ..query import Query, Reachability, Core, MeanPayoff


def _create_env(
    memory_limit: Optional[int], inherit: bool = True, gc: Optional[str] = None
):
    os_env = os.environ
    env = {}  # {"SEED": os_env.get("SEED", "1234")}
    if inherit:
        env["PATH"] = os_env["PATH"]
        if "JAVA_HOME" in os_env:
            env["JAVA_HOME"] = os_env["JAVA_HOME"]
        if "JAVA_OPTS" in os_env:
            env["JAVA_OPTS"] = os_env["JAVA_OPTS"]
    if memory_limit:
        env["JAVA_OPTS"] = (
            env.get("JAVA_OPTS", "") + f" -Xmx{memory_limit - 128}m" + " -Xss128m"
        )
    if gc:
        env["JAVA_OPTS"] = env.get("JAVA_OPTS", "") + f" -XX:+Use{gc}GC"
    return env


def _model_options(model: PrismModel):
    model_arguments = ["--model", str(model.model_file)]
    if model.constants:
        model_arguments += ["--const", model.constants_string()]
    if model.properties_file:
        model_arguments += ["--properties", str(model.properties_file)]
    return model_arguments


@dataclasses.dataclass(frozen=True)
class PetSolver(SimpleSolver):
    sampling: bool

    def precision_options(self):
        return ["--precision", str(self.precision)] + (
            ["--relative"] if self.relative_error else []
        )

    def is_supported(self, model: Model, query: Query) -> bool:
        if self.sampling:
            return (
                isinstance(query, Reachability)
                or isinstance(query, Core)
                or isinstance(query, MeanPayoff)
            )
        return isinstance(query, Reachability) or isinstance(query, MeanPayoff)

    def create_invocation(
        self,
        model: Model,
        query: Query,
        memory_limit: Optional[int],
        validate: bool,
        **kwargs,
    ) -> SolverInvocation:
        assert isinstance(model, PrismModel)
        model: PrismModel

        execution = [str(self.path)]
        if isinstance(query, Reachability):
            execution += (
                ["prism"]
                + _model_options(model)
                + ["--property", query.property_name]
                + self.precision_options()
            )
            if not self.sampling:
                execution.append("--global")
        elif isinstance(query, MeanPayoff):
            execution += (
                ["mean-payoff"]
                + _model_options(model)
                + ["--reward", str(query.rewards)]
                + self.precision_options()
            )
            if query.coalition:
                execution.extend(["--coalition", ",".join(map(str, query.coalition))])
            if self.sampling:
                execution += [
                    "--min",
                    str(query.lower_bound),
                    "--max",
                    str(query.upper_bound),
                ]
            else:
                execution.append("--global")
        elif isinstance(query, Core):
            assert self.sampling
            execution += (
                ["core"]
                + _model_options(model)
                + self.precision_options()
                + (["--validate"] if validate else [])
            )
        else:
            raise AssertionError()

        return SolverInvocation(
            execution,
            _create_env(
                memory_limit, not self.docker_image, gc=kwargs.get("java_gc", None)
            ),
        )

    def check_error(self, stdout, stderr, return_code, model: Model, query: Query):
        return get_java_error(stdout + "\n" + stderr)

    def validate_output(
        self, stdout, stderr, model: Model, query: Query
    ) -> SolverOutput:
        try:
            output = json.loads(stdout)
        except json.JSONDecodeError:
            return SolverOutput(Validation.Error, "Failed to parse JSON")
        if isinstance(query, Core):
            if "core_size" not in output:
                return SolverOutput(Validation.Error, "Field missing in JSON")
            return SolverOutput(
                Validation.Correct,
                f"{output['core_size']} states",
            )
        if isinstance(query, Reachability) or isinstance(query, MeanPayoff):
            values = output["values"]
            if len(values) == 1:
                state, value = next(iter(values.items()))
            else:
                return SolverOutput(
                    Validation.Error,
                    f"Expected single state, got {' '.join(values.keys())}",
                )
            if query.expected_value is None:
                return SolverOutput(Validation.Unknown, value)
            return compare(
                value,
                query.expected_value,
                precision=self.precision,
                relative=self.relative_error,
            )
        raise AssertionError()


@dataclasses.dataclass(frozen=True)
class PetReachAsMpSolver(PetSolver):
    def is_supported(self, model: Model, query: Query) -> bool:
        return isinstance(query, Reachability)

    def create_invocation(
        self,
        model: Model,
        query: Query,
        memory_limit: Optional[int],
        validate: bool,
        **kwargs,
    ) -> SolverInvocation:
        assert isinstance(model, PrismModel)

        execution = [str(self.path)]
        if isinstance(query, Reachability):
            execution += (
                ["mean-payoff"]
                + _model_options(model)
                + ["--reachability", query.property_name]
                + self.precision_options()
            )
            if not self.sampling:
                execution.append("--global")
        else:
            raise AssertionError()

        return SolverInvocation(
            execution,
            _create_env(
                memory_limit, not self.docker_image, gc=kwargs.get("java_gc", None)
            ),
        )
