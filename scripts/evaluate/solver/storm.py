import dataclasses
import logging
from typing import Optional

from .base import SimpleSolver, SolverInvocation, SolverOutput
from ..model import Model, PrismModel, ModelType
from ..query import Query, Reachability, MeanPayoff, ValueQuery

log = logging.getLogger(__name__)


def get_storm_error(output):
    if "Received signal 11" in output:
        return "memory"
    if "ERROR (PrismParser.cpp:458): Parsing error in" in output:
        return "syntax"
    return None


@dataclasses.dataclass(frozen=True)
class StormSolver(SimpleSolver):
    engine: str
    ddlib: Optional[str]

    def is_supported(self, model: Model, query: Query) -> bool:
        if self.engine == "expl":
            return isinstance(query, Reachability) and model.model_type in {
                ModelType.MDP,
                ModelType.DTMC,
            }
        if self.engine == "hybrid":
            return isinstance(query, Reachability) and model.model_type in {
                ModelType.MDP,
                ModelType.DTMC,
            }
        if isinstance(query, MeanPayoff):
            return isinstance(query.rewards, str) and model.model_type in {
                ModelType.MDP,
                ModelType.DTMC,
            }
        if isinstance(query, Reachability):
            return model.model_type != ModelType.SMG
        return False

    def create_invocation(
        self,
        model: Model,
        query: Query,
        memory_limit: Optional[int],
        validate: bool,
        **kwargs,
    ) -> SolverInvocation:
        assert isinstance(model, PrismModel)
        model: PrismModel
        execution = [str(self.path)]

        execution += ["--io:prism", str(model.model_file), "-pc"]
        if model.constants:
            execution += ["--io:constants", model.constants_string()]

        if isinstance(query, Reachability):
            execution += ["--io:prop", str(model.properties_file), query.property_name]
        elif isinstance(query, MeanPayoff):
            execution += ["--io:prop", f'R{{"{query.rewards}"}}max=? [ LRA ]']
        else:
            raise AssertionError()

        execution += ["--sound"]
        execution += ["--precision", str(self.precision)]
        if self.engine:
            execution += ["--core:engine", self.engine]
        if not self.relative_error:
            execution += ["--absolute"]
        if self.ddlib:
            execution += ["--ddlib", self.ddlib]

        return SolverInvocation(execution, {})

    def check_error(self, stdout, stderr, return_code, model: Model, query: Query):
        return get_storm_error(stdout + "\n" + stderr)

    def validate_output(
        self, stdout, stderr, model: Model, query: Query
    ) -> Optional[SolverOutput]:
        query: ValueQuery

        value_str = None
        for line in stdout.splitlines(keepends=False):
            if line.startswith("ERROR"):
                if self.engine != "expl" or (
                    "The selected engine" not in line
                    and "The selected combination" not in line
                ):
                    logging.error("Storm error: %s", line)
            if line.startswith("Result (for initial states): "):
                length = len("Result (for initial states): ")
                value_str = line[length:]
                break
        if value_str is None:
            log.warning("No output found in:\n%s", stdout)
            return None
        return SolverOutput.default(
            query, value_str, self.precision, self.relative_error
        )
