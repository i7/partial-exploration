import abc
import dataclasses
import pathlib
from typing import Dict, List, Optional

from ..model import Model
from ..query import Query, ValueQuery
from ..base import KeyedEnum


class Validation(KeyedEnum):
    Correct = "correct"
    Incorrect = "incorrect"
    Unknown = "unknown"
    Error = "error"


@dataclasses.dataclass
class SolverOutput(object):
    validation: Validation
    message: str

    def serialize(self):
        return {"validation": self.validation.key, "message": self.message}

    @staticmethod
    def parse(data):
        return SolverOutput(
            validation=Validation.by_key(data["validation"]), message=data["message"]
        )

    @staticmethod
    def default(
        query: "ValueQuery", value_str: str, precision: float, relative_error: bool
    ):
        if query.expected_value is None:
            return SolverOutput(Validation.Unknown, value_str)
        try:
            if isinstance(query.expected_value, bool):
                value = bool(value_str)
            elif isinstance(query.expected_value, float):
                value = float(value_str)
            else:
                raise AssertionError()
            return compare(
                value,
                query.expected_value,
                precision=precision,
                relative=relative_error,
            )
        except ValueError:
            return SolverOutput(
                Validation.Error, f"Failed to parse value string {value_str}"
            )


@dataclasses.dataclass
class SolverInvocation(object):
    execution: List[str]
    env: Dict[str, str]


@dataclasses.dataclass(frozen=True)
class Solver(abc.ABC):
    docker_image: Optional[str]

    @property
    @abc.abstractmethod
    def key(self) -> str:
        pass

    @abc.abstractmethod
    def is_supported(self, model: Model, query: Query) -> bool:
        pass

    @abc.abstractmethod
    def create_invocation(
        self,
        model: Model,
        query: Query,
        memory_limit: Optional[int],
        validate: bool,
        **kwargs,
    ) -> SolverInvocation:
        pass

    @abc.abstractmethod
    def check_error(
        self, stdout, stderr, return_code: int, model: Model, query: Query
    ) -> Optional[str]:
        pass

    @abc.abstractmethod
    def validate_output(
        self, stdout, stderr, model: Model, query: Query
    ) -> Optional[SolverOutput]:
        pass

    def __str__(self):
        return self.key


@dataclasses.dataclass(frozen=True)
class SimpleSolver(Solver, metaclass=abc.ABCMeta):
    solver_name: str
    path: pathlib.Path
    precision: float
    relative_error: bool

    @staticmethod
    def get_default_kwargs(solver):
        conf = solver.get("conf", {})
        return {
            "solver_name": solver["name"],
            "path": conf["path"],
            "precision": float(conf.get("precision", 1e-6)),
            "relative_error": conf.get("error", "absolute") == "relative",
            "docker_image": solver.get("image", None),
        }

    @property
    def key(self) -> str:
        return self.solver_name


def compare(value, expected, precision=1e-6, relative=False):
    if value is None:
        message = "No value present"
        return SolverOutput(Validation.Error, message)
    if type(value) is not type(expected):
        return SolverOutput(
            Validation.Error,
            f"Expected type {type(expected)} but got {type(value)}",
        )
    if isinstance(expected, float):
        if relative:
            expected_lower = (1 - precision) * expected
            expected_upper = (1 + precision) * expected
        else:
            expected_lower = expected - precision
            expected_upper = expected + precision
        center = (expected_lower + expected_upper) / 2
        deviation = (expected_upper - expected_lower) / 2

        if expected_lower <= value <= expected_upper:
            return SolverOutput(
                Validation.Correct,
                f"Value {value:.6g} within {center:.6g}±{deviation:.3g}",
            )
        difference = min(abs(expected_upper - value), abs(expected_lower - value))
        return SolverOutput(
            Validation.Incorrect,
            f"Value {value:.6g} outside {center:.6g}±{deviation:.3g} (diff {difference:.3g})",
        )
    if expected == value:
        return SolverOutput(Validation.Correct, f"Got expected value {expected}")
    return SolverOutput(Validation.Incorrect, f"Expected {expected}, got {value}")
