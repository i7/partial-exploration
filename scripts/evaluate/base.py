import enum


class KeyedEnum(enum.Enum):
    @classmethod
    def by_key(cls, key):
        if key is None:
            return None
        for t in cls:
            assert hasattr(t, "key")
            if t.key == key:
                return t
        raise KeyError(f"Type {key} of {cls} unknown")

    @property
    def key(self):
        return self.value

    def __lt__(self, other):
        return self.key < other.key
