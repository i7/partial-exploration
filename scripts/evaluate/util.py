import pathlib
from typing import List

import yaml

from evaluate.process import Outcome, Success
from evaluate.solver.base import Validation


class YamlIncludeLoader(yaml.SafeLoader):
    def __init__(self, stream):
        self._root = pathlib.Path(stream.name).parent.absolute()
        super(YamlIncludeLoader, self).__init__(stream)

    def include(self, node):
        with (self._root / self.construct_scalar(node)).open(mode="rt") as f:
            return yaml.load(f, YamlIncludeLoader)


# noinspection PyTypeChecker
YamlIncludeLoader.add_constructor("!include", YamlIncludeLoader.include)


def flatten_lists(data, result):
    assert isinstance(data, list)
    for d in data:
        if isinstance(d, list):
            flatten_lists(d, result)
        else:
            result.append(d)
    return result


def print_results(data: List[Outcome]):
    def format_data(outcome: Outcome):
        row = [
            outcome.tag if outcome.tag else "",
            outcome.model_key,
            outcome.query_key,
            outcome.solver_key,
            outcome.result.time,
        ]
        if (
            not isinstance(outcome.result, Success)
            or outcome.result.solver_output.validation == Validation.Unknown
        ):
            row.append("?")
        else:
            row.append("Yes" if outcome.is_valid() else "No")
        row.append(outcome.result.message())
        return row

    try:
        import tabulate

        headers = ["", "model", "query", "solver", "time", "correct", "message"]
        print(tabulate.tabulate([format_data(d) for d in sorted(data)], headers))
    except ModuleNotFoundError:
        for d in sorted(data):
            print(" ".join(map(str, format_data(d))))
