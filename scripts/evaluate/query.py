import abc
import dataclasses
from typing import List, Optional

from .base import KeyedEnum


def parse_expected(val):
    v = str(val).lower()
    if v == "true":
        return True
    if v == "false":
        return False
    try:
        return float(val)
    except TypeError:
        return val


class QueryType(KeyedEnum):
    Core = "core"
    Reachability = "reach"
    MeanPayoff = "mean-payoff"

    def __str__(self):
        return self.value


@dataclasses.dataclass(frozen=True)
class Query(abc.ABC):
    @property
    @abc.abstractmethod
    def query_type(self) -> QueryType:
        pass

    @property
    def name(self) -> str:
        return self.key

    @property
    @abc.abstractmethod
    def key(self) -> str:
        pass

    @staticmethod
    def parse(query_type, spec):
        if query_type == "core":
            return Core.do_parse(spec)
        if query_type == "reach":
            return Reachability.do_parse(spec)
        if query_type == "mean_payoff":
            return MeanPayoff.do_parse(spec)
        raise ValueError(f"Unknown query type {query_type}")

    def __str__(self):
        return self.name


@dataclasses.dataclass(frozen=True)
class Core(Query):
    @staticmethod
    def do_parse(_):
        return Core()

    @property
    def query_type(self):
        return QueryType.Core

    @property
    def key(self):
        return "core"


@dataclasses.dataclass(frozen=True)
class ValueQuery(Query, abc.ABC):
    expected_value: Optional


@dataclasses.dataclass(frozen=True)
class Reachability(ValueQuery):
    property_name: str

    @staticmethod
    def do_parse(data):
        expected_value = (
            parse_expected(data["expected"]) if "expected" in data else None
        )
        return Reachability(
            expected_value=expected_value,
            property_name=str(data["property"]),
        )

    @property
    def query_type(self):
        return QueryType.Reachability

    @property
    def key(self):
        return f"reach.{self.property_name}"

    @property
    def name(self) -> str:
        return f"reach({self.property_name})"


@dataclasses.dataclass(frozen=True)
class MeanPayoff(ValueQuery):
    rewards: str
    lower_bound: float
    upper_bound: float
    coalition: Optional[List[str]]

    @staticmethod
    def do_parse(data):
        expected_value = (
            parse_expected(data["expected"]) if "expected" in data else None
        )
        if "coalition" in data:
            coalition = list(map(str, data["coalition"]))
        else:
            coalition = None
        return MeanPayoff(
            rewards=data["rewards"],
            lower_bound=data["reward_min"],
            upper_bound=data["reward_max"],
            expected_value=expected_value,
            coalition=coalition,
        )

    @property
    def query_type(self):
        return QueryType.MeanPayoff

    @property
    def name(self):
        return (
            f"MP({self.rewards})"
            if self.coalition is None
            else f"MP({self.rewards},max={','.join(map(str, self.coalition))})"
        )

    @property
    def key(self):
        if self.coalition is not None:
            return f"mean_payoff.{self.rewards}.{'_'.join(self.coalition)}"
        return f"mean_payoff.{self.rewards}"
