import argparse
import csv
import dataclasses
import itertools
import json
import logging
import math
import pathlib
from collections import defaultdict
from typing import List, Collection, Optional, Dict

from evaluate.model import ModelType
from evaluate.process import Outcome, Success, Timeout, Error
from evaluate.query import QueryType
from evaluate.solver.base import Validation

log = logging.getLogger(__name__)

TIMEOUT_VALUE_FOR_RATIO = 80
TIMEOUT_PRINT_VALUE = 80
ERROR_PRINT_VALUE = 120


@dataclasses.dataclass
class Comparison(object):
    solver_x: str
    solver_y: str
    ratios: List[float] = dataclasses.field(default_factory=list)
    ratios_with_timeout: List[float] = dataclasses.field(default_factory=list)
    y_times_with_x_timeout: List[float] = dataclasses.field(default_factory=list)
    x_times_with_y_timeout: List[float] = dataclasses.field(default_factory=list)

    @staticmethod
    def check_types(solver: str, outcomes: Collection[Outcome]):
        model_key, query_key = next((o.model_key, o.query_key) for o in outcomes)
        types = set(type(o.result) for o in outcomes)
        if Error in types:
            if len(types) > 1:
                log.warning(
                    "Some but not all executions of %s failed on %s / %s",
                    solver,
                    model_key,
                    query_key,
                )
            return

        times = [o.result.time for o in outcomes]
        if Timeout in types:
            if len(types) > 1:
                print(
                    f"Some but not all executions of {solver} timed out on {model_key} / {query_key}, {min(times):.1f} minimum time"
                )
            return

    def add_outcomes(self, outcomes: Dict[str, List[Outcome]]):
        outcomes_x = outcomes[self.solver_x]
        outcomes_y = outcomes[self.solver_y]

        if not outcomes_x or not outcomes_y:
            return

        assert (
            len(
                set(
                    (o.model_key, o.query_key)
                    for o in itertools.chain(outcomes_x, outcomes_y)
                )
            )
            == 1
        )
        model_key, query_key = next((o.model_key, o.query_key) for o in outcomes_x)

        x_types, y_types = (
            set(type(o.result) for o in outcomes_x),
            set(type(o.result) for o in outcomes_y),
        )

        if Error in x_types or Error in y_types:
            log.warning(
                "Ignoring %s vs %s on %s / %s, some error occurred",
                self.solver_x,
                self.solver_y,
                model_key,
                query_key,
            )
            return

        x_times = (
            geom([o.result.time for o in outcomes_x if isinstance(o.result, Success)])
            if Timeout not in x_types
            else math.nan
        )
        y_times = (
            geom([o.result.time for o in outcomes_y if isinstance(o.result, Success)])
            if Timeout not in y_types
            else math.nan
        )
        if math.isnan(x_times) and math.isnan(y_times):
            return

        if math.isnan(x_times):
            self.y_times_with_x_timeout.append(y_times)
            self.ratios_with_timeout.append(y_times / TIMEOUT_VALUE_FOR_RATIO)
        elif math.isnan(y_times):
            self.x_times_with_y_timeout.append(x_times)
            self.ratios_with_timeout.append(TIMEOUT_VALUE_FOR_RATIO / x_times)
        else:
            self.ratios.append(y_times / x_times)
            self.ratios_with_timeout.append(y_times / x_times)

    def geom(self):
        return geom(self.ratios)

    def x_only(self):
        return len(self.x_times_with_y_timeout)

    def y_only(self):
        return len(self.y_times_with_x_timeout)

    def both(self):
        return len(self.ratios)

    def max(self):
        return max(self.ratios, default=math.nan)

    def min(self):
        return min(self.ratios, default=math.nan)

    def compare_string(self):
        return f"{self.geom():.2f} ({self.both()}+{self.x_only()}/{self.y_only()}) ({geom(self.ratios_with_timeout):.2f})"


def avg(values: Collection[float]) -> float:
    if not values:
        return math.nan
    return sum(values) / len(values)


def geom(values: Collection[float]) -> float:
    if not values:
        return math.nan
    return math.exp(sum(math.log(v) for v in values) / len(values))


def geom_outcomes(
    outcomes: List[Outcome], non_success_time: Optional[float] = None
) -> float:
    if non_success_time is not None:
        values = [
            o.result.time if isinstance(o.result, Success) else non_success_time
            for o in outcomes
        ]
    else:
        values = [o.result.time for o in outcomes]
    return geom(values)


def do(args):
    outcomes = []
    for path in args.files:
        with pathlib.Path(path).open("rt") as f:
            for d in json.load(f):
                outcomes.append(Outcome.parse(d))
    for path in args.gc_run:
        with pathlib.Path(path).open("rt") as f:
            for d in json.load(f):
                parse = Outcome.parse(d, tag="gc")
                outcomes.append(parse)
    print(f"Loaded {len(outcomes)} results")

    outcome_by_type_and_key = defaultdict(
        lambda: defaultdict(lambda: defaultdict(list))
    )
    for outcome in outcomes:
        by_key = outcome_by_type_and_key[(outcome.query_type, outcome.model_type)]
        key = outcome.model_key, outcome.query_key
        by_key[key][outcome.tagged_solver()].append(outcome)

    reach_overall = set()
    reach_solved_by_graph_pet, reach_success_pet = set(), set()
    reach_solved_by_graph_prism, reach_success_prism = set(), set()
    keys_by_approach = defaultdict(set)

    for outcome in outcomes:
        if outcome.query_type != QueryType.Reachability:
            continue
        if outcome.tagged_solver() not in {"PETg", "PRISMe"}:
            continue

        key = outcome.model_key, outcome.query_key
        reach_overall.add(key)

        if not isinstance(outcome.result, Success):
            continue

        if outcome.tagged_solver() == "PETg":
            reach_success_pet.add(key)
            try:
                output = json.loads(outcome.result.output)
            except json.JSONDecodeError as e:
                log.warning(
                    "Failed to parse PET output for instance %s / %s (%s)",
                    outcome.model_key,
                    outcome.query_key,
                    e,
                )
                continue
            solution = output["statistics"]["solution"]
            if solution != "iteration":
                reach_solved_by_graph_pet.add(key)
        if outcome.tagged_solver() == "PRISMe":
            reach_success_prism.add(key)
            if "maybe=0" in outcome.result.output:
                reach_solved_by_graph_prism.add(key)
    print(f"Graph analysis statistics for {len(reach_overall)} instances")
    both_solved = reach_success_pet & reach_success_prism
    print(f"  {len(both_solved)} instances solved by both")
    print(
        f"  PET solved {len(reach_solved_by_graph_pet)} / {len(reach_success_pet)} by graph analysis"
    )
    print(
        f"  PRISM solved {len(reach_solved_by_graph_prism)} / {len(reach_success_prism)} by graph analysis"
    )
    only_pet_by_graph = (
        both_solved - reach_solved_by_graph_prism
    ) & reach_solved_by_graph_pet
    only_prism_by_graph = (
        both_solved - reach_solved_by_graph_pet
    ) & reach_solved_by_graph_prism
    print(f"  {len(only_pet_by_graph)} PET solved by graph, PRISM by iteration")
    print(f"  {len(only_prism_by_graph)} PRISM solved by graph, PET by iteration")
    print()

    pet_global = Comparison("PETg", "PET-1.0g")
    pet_sampling = Comparison("PETs", "PET-1.0s")
    pet_vs_storm_sampling = Comparison("PETs", "Storm-explore")
    pet_vs_storm_hybrid = Comparison("PETg", "Storm-hybrid")
    pet_vs_prism = Comparison("PETg", "PRISMe")
    pet_vs_prism_h = Comparison("PETg", "PRISMh")
    pet_vs_storm = Comparison("PETg", "Storm")
    pet_vs_tempest = Comparison("PETg", "TEMPEST")
    pet_vs_prism_ext = Comparison("PETg", "PRISM-extensions")
    pet_rmp_vs_prism = Comparison("PETg-rmp", "PRISMe")
    pet_rmp_vs_pet = Comparison("PETg", "PETg-rmp")

    petg_vs_petg_gc = Comparison("PETg", "PETg@gc")
    pets_vs_pets_gc = Comparison("PETs", "PETs@gc")
    # prism_vs_prism_gc = Comparison("PRISMe", "PRISMe@gc")

    comparisons = [
        pet_global,
        pet_sampling,
        pet_vs_storm_sampling,
        pet_vs_storm_hybrid,
        pet_vs_prism,
        pet_vs_prism_ext,
        pet_rmp_vs_prism,
        pet_rmp_vs_pet,
        pet_vs_storm,
        pet_vs_tempest,
        pet_vs_prism_h,
    ]
    if args.gc_run:
        comparisons += [petg_vs_petg_gc, pets_vs_pets_gc]

    for (query_type, model_type), type_outcomes in outcome_by_type_and_key.items():
        for instance_key, instance_outcomes in type_outcomes.items():
            keys_by_approach[query_type].add(instance_key)
            for solver_key, solver_outcomes in instance_outcomes.items():
                Comparison.check_types(solver_key, solver_outcomes)
            for comparison in comparisons:
                comparison.add_outcomes(instance_outcomes)

    print("Comparison scores:")
    print(f"  PET1 CE / PET2 CE: {pet_global.compare_string()}")
    print(f"  PET1 PE / PET2 PE: {pet_sampling.compare_string()}")
    print(f"  Storm-explore / PET2 PE: {pet_vs_storm_sampling.compare_string()}")
    print(f"  Storm-hybrid / PET2 CE: {pet_vs_storm_hybrid.compare_string()}")
    print(f"  PRISM-e / PET2 CE: {pet_vs_prism.compare_string()}")
    print(f"  PRISM-e / PET2 reach as mp CE: {pet_rmp_vs_prism.compare_string()}")
    print(f"  PRISM-h / PET2 CE: {pet_vs_prism_h.compare_string()}")
    print(f"  PRISM-ext / PET2 CE: {pet_vs_prism_ext.compare_string()}")
    print(f"  Storm / PET2 CE: {pet_vs_storm.compare_string()}")
    print(f"  TEMPEST / PET2 CE: {pet_vs_tempest.compare_string()}")
    print(f"  PET2 reach as mp CE / PET2 CE: {pet_rmp_vs_pet.compare_string()}")

    if args.gc_run:
        print(
            f"  PET CE GC: {petg_vs_petg_gc.compare_string()} (max {petg_vs_petg_gc.max():.2f} / min {petg_vs_petg_gc.min():.2f})"
        )
        print(
            f"  PET PE GC: {pets_vs_pets_gc.compare_string()} (max {pets_vs_pets_gc.max():.2f} / min {pets_vs_pets_gc.min():.2f})"
        )
        # print(f"  PRISM GC: {prism_vs_prism_gc.compare_string()}")

    print()

    reference_instances_by_solvers = defaultdict(set)
    wrong_answers_by_solvers = defaultdict(set)

    for type_key, type_outcomes in outcome_by_type_and_key.items():
        for instance_key, instance_outcomes in type_outcomes.items():
            for solver_key, solver_outcomes in instance_outcomes.items():
                if not any(isinstance(o.result, Success) for o in solver_outcomes):
                    continue
                if any(
                    isinstance(o.result, Success)
                    and o.result.solver_output.validation == Validation.Unknown
                    for o in solver_outcomes
                ):
                    continue
                reference_instances_by_solvers[solver_key].add(instance_key)
                wrong_message = None
                for o in solver_outcomes:
                    if (
                        isinstance(o.result, Success)
                        and o.result.solver_output.validation == Validation.Incorrect
                    ):
                        wrong_message = o.result.solver_output.message
                        break
                if wrong_message:
                    wrong_answers_by_solvers[solver_key].add(
                        (instance_key, wrong_message)
                    )

    if wrong_answers_by_solvers:
        print("Wrong answers:")
        for solver_key, solver_instances in sorted(
            reference_instances_by_solvers.items(), key=lambda x: x[0]
        ):
            if solver_key in wrong_answers_by_solvers:
                print(
                    f"  Solver {solver_key} has {len(wrong_answers_by_solvers[solver_key])} / {len(solver_instances)} wrong answers"
                )
                for (model, query), message in sorted(
                    wrong_answers_by_solvers[solver_key], key=lambda x: x[0]
                ):
                    print(f"    {model}({query}): {message}")
            else:
                print(
                    f"  Solver {solver_key} has no wrong answers on {len(solver_instances)} instances"
                )
        print()

    solver_times = defaultdict(dict)

    for type_key, type_outcomes in outcome_by_type_and_key.items():
        for instance_key, instance_outcomes in type_outcomes.items():
            for solver_key, solver_outcomes in instance_outcomes.items():
                if not solver_outcomes:
                    continue
                key = type_key, instance_key
                if any(isinstance(o.result, Error) for o in solver_outcomes):
                    solver_times[key][solver_key] = ERROR_PRINT_VALUE
                elif any(isinstance(o.result, Timeout) for o in solver_outcomes):
                    solver_times[key][solver_key] = TIMEOUT_PRINT_VALUE
                # elif not all(isinstance(o.result, Success) for o in solver_outcomes):
                #    solver_times[key][solver_key] = ERROR_PRINT_VALUE
                else:
                    assert all(isinstance(o.result, Success) for o in solver_outcomes)
                    solver_times[key][solver_key] = geom_outcomes(solver_outcomes)

    # pet_sampling_times_by_type = defaultdict(list)
    # pet_global_times_by_type = defaultdict(list)
    # sampling_reach_times = list()
    # global_reach_times = list()
    # all_mdp_data = list()
    # sg_reach = list()
    # sg_mp = list()

    solver_rename = {
        "PET-1.0s": "pet_1_s",
        "PET-1.0g": "pet_1_g",
        "PETs": "pet_2_s",
        "PETg": "pet_2_g",
        "PETs-rmp": "pet_2_s_rmp",
        "PETg-rmp": "pet_2_g_rmp",
    }

    pairwise_comparisons = [
        ("1_vs_2_pe", "PET-1.0s", "PETs", None),
        ("1_vs_2_ce", "PET-1.0g", "PETg", None),
        ("pe_vs_ce", "PETs", "PETg", None),
        ("pe_vs_ce", "PETs", "PETg", None),
        ("pe_vs_storm", "Storm-explore", "PETs", None),
        (
            "ce_vs_prism_e_smg",
            "PRISMe",
            "PETg",
            lambda x: x["model_type"] == ModelType.SMG,
        ),
        (
            "ce_vs_prism_e_mdp",
            "PRISMe",
            "PETg",
            lambda x: x["model_type"] != ModelType.SMG,
        ),
        ("ce_vs_prism_ext", "PRISM-extensions", "PETg", None),
        ("ce_vs_prism_h", "PRISMh", "PETg", None),
        ("ce_vs_storm", "Storm", "PETg", None),
        ("ce_vs_tempest", "TEMPEST", "PETg", None),
        ("ce_vs_storm_h", "Storm-hybrid", "PETg", None),
        ("ce_rmp_vs_prism", "PRISMe", "PETg-rmp", None),
        ("ce_vs_ce_rmp", "PETg-rmp", "PETg", None),
    ]

    for f, x, y, model_filter in pairwise_comparisons:
        rows = []

        for (
            (query_type, model_type),
            (model_key, query_key),
        ), times in solver_times.items():
            if model_filter is not None and not model_filter(
                {"model_type": model_type, "query_type": query_type}
            ):
                continue
            x_time, y_time = times.get(x, None), times.get(y, None)
            if x_time and y_time:
                rows.append(
                    [
                        f"{model_key}_{query_key.replace(',', '_')}",
                        str(model_type),
                        x_time,
                        y_time,
                    ]
                )
        if not rows:
            log.warning("No results for %s vs %s", x, y)
            continue

        with pathlib.Path(f"csv_pair_{f}.csv").open("wt") as f:
            writer = csv.writer(f)
            writer.writerow(
                ["model", "type", solver_rename.get(x, x), solver_rename.get(y, y)]
            )
            writer.writerows(rows)
    #
    # for (
    #     (query_type, model_type),
    #     (model_key, query_key),
    # ), times in solver_times.items():
    #     pet_1_s_time = times.get("PET-1.0s", None)
    #     pet_1_g_time = times.get("PET-1.0g", None)
    #     pet_s_time = times.get("PETs", None)
    #     pet_g_time = times.get("PETg", None)
    #     pet_s_rmp_time = times.get("PETs-rmp", None)
    #     pet_g_rmp_time = times.get("PETg-rmp", None)
    #     storm_time = times.get("Storm", None)
    #     storm_h_time = times.get("Storm-hybrid", None)
    #     storm_expl_time = times.get("Storm-explore", None)
    #     prism_e_time = times.get("PRISMe", None)
    #     prism_h_time = times.get("PRISMh", None)
    #     prism_ext_time = times.get("PRISM-extensions", None)
    #     tempest_time = times.get("TEMPEST", None)
    #
    #     if pet_s_time is not None and pet_1_s_time is not None:
    #         pet_sampling_times_by_type[query_type].append(
    #             [f"{model_key}_{query_key}", str(model_type), pet_1_s_time, pet_s_time]
    #         )
    #     if pet_g_time is not None and pet_1_g_time is not None:
    #         pet_global_times_by_type[query_type].append(
    #             [f"{model_key}_{query_key}", str(model_type), pet_1_g_time, pet_g_time]
    #         )
    #     if query_type == QueryType.Reachability:
    #         sampling_reach_row = [
    #             pet_s_time,
    #             pet_s_rmp_time,
    #             pet_1_s_time,
    #             storm_expl_time,
    #         ]
    #         if all(sampling_reach_row):
    #             sampling_reach_times.append(
    #                 [f"{model_key}_{query_key}", str(model_type)] + sampling_reach_row
    #             )
    #
    #         global_reach_row = [
    #             pet_g_time,
    #             pet_g_rmp_time,
    #             prism_e_time
    #         ]
    #         if all(global_reach_row):
    #             global_reach_times.append(
    #                 [f"{model_key}_{query_key}", str(model_type)] + global_reach_row
    #             )
    #         if model_type == ModelType.SMG:
    #             sg_row = [
    #                 pet_g_time,
    #                 pet_g_rmp_time,
    #                 pet_s_time,
    #                 pet_s_rmp_time,
    #                 prism_e_time,
    #                 prism_ext_time,
    #                 tempest_time,
    #             ]
    #             if all(sg_row):
    #                 sg_reach.append(
    #                     [f"{model_key}_{query_key}", str(model_type)] + sg_row
    #                 )
    #         else:
    #             mdp_row = [
    #                 pet_1_s_time,
    #                 pet_1_g_time,
    #                 pet_s_time,
    #                 pet_g_time,
    #                 prism_e_time,
    #                 prism_h_time,
    #                 storm_time,
    #                 storm_expl_time,
    #                 prism_h_time,
    #                 storm_time,
    #                 storm_h_time,
    #             ]
    #             if all(mdp_row):
    #                 all_mdp_data.append(
    #                     [f"{model_key}_{query_key}", str(model_type)] + mdp_row
    #                 )
    #     elif query_type == QueryType.MeanPayoff:
    #         if model_type == ModelType.SMG:
    #             sg_mp_row = [pet_g_time, pet_s_time, tempest_time]
    #             if all(sg_mp_row):
    #                 sg_mp.append(
    #                     [
    #                         f"{model_key}_{query_key.replace(',', '_')}",
    #                         str(model_type),
    #                     ]
    #                     + sg_mp_row
    #                 )
    #
    # for t in QueryType:
    #     with pathlib.Path(f"csv_pet_compare_sampling_{str(t).lower()}.csv").open(
    #         "wt"
    #     ) as f:
    #         writer = csv.writer(f)
    #         writer.writerow(["model", "type", "pet_1_s", "pet_2_s", "pet_2_rmp_s"])
    #         writer.writerows(pet_sampling_times_by_type[t])
    #     with pathlib.Path(f"csv_pet_compare_global_{str(t).lower()}.csv").open(
    #         "wt"
    #     ) as f:
    #         writer = csv.writer(f)
    #         writer.writerow(["model", "type", "pet_1_g", "pet_2_g", "pet_2_rmp_g"])
    #         writer.writerows(pet_global_times_by_type[t])
    #
    # with pathlib.Path("csv_sampling_reach.csv").open("wt") as f:
    #     writer = csv.writer(f)
    #     writer.writerow(
    #         ["model", "type", "pet_s", "pet_s_rmp", "pet_1_s", "storm_expl"]
    #     )
    #     writer.writerows(sampling_reach_times)
    # with pathlib.Path("csv_global_reach.csv").open("wt") as f:
    #     writer = csv.writer(f)
    #     writer.writerow(
    #         [
    #             "model",
    #             "type",
    #             "pet_g",
    #             "pet_g_rmp",
    #             "prism_e",
    #             "prism_h",
    #             "storm",
    #             "storm_h",
    #         ]
    #     )
    #     writer.writerows(global_reach_times)
    # with pathlib.Path("csv_mdp_all.csv").open("wt") as f:
    #     writer = csv.writer(f)
    #     writer.writerow(
    #         [
    #             "model",
    #             "type",
    #             "pet_1_s",
    #             "pet_1_g",
    #             "pet_s",
    #             "pet_g",
    #             "prism_e",
    #             "prism_h",
    #             "storm",
    #             "storm_expl",
    #         ]
    #     )
    #     writer.writerows(all_mdp_data)
    # with pathlib.Path("csv_sg_reach.csv").open("wt") as f:
    #     writer = csv.writer(f)
    #     writer.writerow(
    #         [
    #             "model",
    #             "type",
    #             "pet_g",
    #             "pet_g_rmp",
    #             "pet_s",
    #             "pet_s_rmp",
    #             "prism_e",
    #             "prism_ext",
    #             "tempest",
    #         ]
    #     )
    #     writer.writerows(sg_reach)
    # with pathlib.Path("csv_sg_mp.csv").open("wt") as f:
    #     writer = csv.writer(f)
    #     writer.writerow(["model", "type", "pet_g", "pet_s", "tempest"])
    #     writer.writerows(sg_mp)

    if args.print_models:
        for type_key, instances in keys_by_approach.items():
            print()
            print(type_key)
            for model_key, query_key in sorted(instances):
                print(model_key, query_key)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument("files", type=pathlib.Path, nargs="+", help="Files to load")
    parser.add_argument("--print-models", action="store_true", help=argparse.SUPPRESS)
    parser.add_argument(
        "--gc-run", type=pathlib.Path, nargs="*", help=argparse.SUPPRESS
    )
    do(parser.parse_args())
