import argparse
import json
import pathlib
import sys

from evaluate.process import Outcome
from evaluate.util import print_results


def do(args):
    results = []
    for path in args.load:
        if "@" in path:
            path, tag = path.rsplit("@", 2)
        else:
            tag = None

        with pathlib.Path(path).open("rt") as f:
            results.extend([Outcome.parse(d, tag) for d in json.load(f)])

    print_results(results)
    if args.validate and not all(outcome.is_valid() for outcome in results):
        sys.exit("Validation failed")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "load",
        type=str,
        nargs="*",
        help="Load results. Add an @ to tag the results.",
    )
    parser.add_argument(
        "--validate",
        action="store_true",
        help="Validate results (only considering those with expected value)",
    )
    do(parser.parse_args())
