#!/usr/bin/env python3
import argparse
import json
import logging
import math
import pathlib
from collections import defaultdict
from typing import List

import tabulate

from evaluate.process import Success, Outcome, Error, ProcessResult, Timeout
from evaluate.solver.base import Validation

log = logging.getLogger(__name__)
project_root = pathlib.Path(__file__).parent.parent


def do(args):
    outcomes = []
    for path in args.files:
        with pathlib.Path(path).open("rt") as f:
            for d in json.load(f):
                outcomes.append(Outcome.parse(d))
    print(f"Loaded {len(outcomes)} results")

    outcome_by_type_and_key = defaultdict(dict)
    for result in outcomes:
        by_key = outcome_by_type_and_key[(result.query_type, result.model_type)]
        key = result.model_key, result.query_key
        if key not in by_key:
            by_key[key] = dict()
        if result.solver_key not in by_key[key]:
            by_key[key][result.solver_key] = []
        by_key[key][result.solver_key].append(result)

    for (query_type, model_type), query_outcomes in sorted(
        outcome_by_type_and_key.items(), key=lambda x: x[0]
    ):
        solvers_with_solutions = set()
        for solver_results in query_outcomes.values():
            solvers_with_solutions.update(solver_results.keys())
        solver_header = list(sorted(solvers_with_solutions))

        rows = []
        for (model_key, query_key), solvers in sorted(
            query_outcomes.items(), key=lambda x: x[0]
        ):
            row = [
                model_key,
                query_key.split(".", maxsplit=2)[1] if "." in query_key else query_key,
            ]
            for solver in solver_header:
                solver_outcomes: List[Outcome] = solvers.get(solver)

                solve_time, solve_deviation, solve_message = None, None, None
                if solver_outcomes:
                    process_results: List[ProcessResult] = [
                        s.result for s in solver_outcomes
                    ]
                    if all(isinstance(p, Success) for p in process_results):
                        process_results: List[Success]
                        solve_time = math.exp(
                            sum(math.log(p.time) for p in process_results)
                            / len(process_results)
                        )
                        if len(process_results) > 1:
                            solve_deviation = math.exp(
                                math.sqrt(
                                    sum(
                                        math.log(p.time / solve_time) ** 2
                                        for p in process_results
                                    )
                                    / len(process_results)
                                )
                            )
                        if any(
                            p.solver_output.validation == Validation.Incorrect
                            for p in process_results
                        ):
                            logging.info(
                                "WA by %s on %s / %s: %s",
                                solver,
                                model_key,
                                query_key,
                                process_results[0].message(),
                            )
                            solve_message = "WA"
                    elif all(isinstance(p, Error) for p in process_results):
                        process_results: List[Error]
                        errors = set(p.error_type for p in process_results)
                        solve_message = f"Err({','.join(errors)})"
                    elif all(isinstance(p, Timeout) for p in process_results):
                        solve_message = "T/O"
                    else:
                        solve_message = "?"
                else:
                    solve_message = "N/A"
                row.extend([solve_time, solve_deviation, solve_message])
            rows.append(row)

        headers = ["model", "query"]
        for h in solver_header:
            headers.extend([h, "", ""])

        if args.format and args.format.startswith("latex"):
            print("\\begin{table}")
            print("\\centering")
        print(f" ==== {model_type} / {query_type} ====")
        print(
            f"{sum(map(len, query_outcomes.values()))} results from {len(solvers_with_solutions)} solvers"
        )
        print()

        print(
            tabulate.tabulate(
                rows,
                headers=headers,
                tablefmt=args.format,
                stralign="right",
                floatfmt=".2f",
            )
        )
        if args.format and args.format.startswith("latex"):
            print("\\end{table}")
        print()
        print()

        if args.csv:
            import csv

            with pathlib.Path(
                f"{args.csv}_{str(model_type).lower()}_{str(query_type).lower()}.csv"
            ).open("wt") as f:
                w = csv.writer(f)
                w.writerow(headers)
                w.writerows(rows)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument("files", type=pathlib.Path, nargs="+", help="Files to load")
    parser.add_argument(
        "--format", type=str, help="Format for tabulate", default="simple"
    )
    parser.add_argument("--csv", type=str, help="Write to csv")
    do(parser.parse_args())
