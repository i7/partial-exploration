import argparse
import csv
import json
import pathlib
import sys
from collections import defaultdict
from typing import Dict, Tuple, List

from evaluate.process import Outcome, Success


def do(args):
    results = []
    for path in args.file:
        with pathlib.Path(path).open("rt") as f:
            for d in json.load(f):
                results.append(Outcome.parse(d))
    results_by_instance_key: Dict[Tuple, List[Outcome]] = defaultdict(list)
    for result in results:
        key = (result.model_key, result.query_key)
        results_by_instance_key[key].append(result)

    rows = []
    filters = []

    if args.any_success:
        filters.append(lambda outs: any(isinstance(o.result, Success) for o in outs))
    if args.not_all_below:
        filters.append(
            lambda outs: all(
                not isinstance(o.result, Success) or o.result.time > args.not_all_below
                for o in outs
            )
        )

    for (model_key, query_key), instance_results in results_by_instance_key.items():
        include = all(f(instance_results) for f in filters)
        rows.append(["y" if include else "n"] + [model_key, query_key])

    csv.writer(sys.stdout).writerows(rows)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "file",
        type=pathlib.Path,
        nargs="*",
        help="Files to filter",
    )
    parser.add_argument(
        "--any-success",
        action="store_true",
        help="Exclude models with no success",
    )
    parser.add_argument(
        "--not-all-below",
        type=float,
        help="Exclude models where all runtimes below given value",
    )
    do(parser.parse_args())
