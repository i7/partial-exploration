#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

directory="$(dirname "$(dirname "$(readlink -f "$0")")")"

rm -f sources.tar.gz
tar -czf sources.tar.gz -C "$directory" \
  --exclude=".idea" --exclude=".git" \
  src/graal/java \
  src/main/java \
  src/main/resources \
  config/template-unix.txt \
  lib/models/src/main/java \
  scripts/ \
  data/ \
  \
  build.gradle.kts settings.gradle.kts \
  lib/models/build.gradle.kts lib/models/settings.gradle.kts
