#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

directory="$(dirname "$(dirname "$(readlink -f "$0")")")"
build_directory="${directory}/build"
extract_directory="${build_directory}/extract"

(cd "${directory}" && ./gradlew distTar)
mkdir -p "${extract_directory}"
tar -xf "${build_directory}/distributions/pet-"*".tar" -C "${extract_directory}"
rm -rf "${build_directory}/pet" 2>/dev/null
mv "${extract_directory}/pet-"* "${build_directory}/pet"