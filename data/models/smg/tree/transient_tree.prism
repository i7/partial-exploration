smg

// This SMG starts with a full binary tree of depth N. One player controls all even levels, the other all odd levels. Every state can choose between its two children.
// Every leaf is a MEC of size K. For now, it is just a cycle of length K, where every state has a reward of mod(node,N) + K.


// Number of levels in the tree. Levels are counted from 0 to N-1. state space size is 2^{N} - 1 (full binary tree of depth N)
const int N;
// Number of states per MEC
const int K;

// Nodes are numbered. 1 is the root on the 0-th level. The i-th level (i in [1,N-1]) contains the nodes from (2^i) to 2^(i+1)-1
global node: [1 .. pow(2,N-1)] init 1;
// in leaves, we start counting up the position from 0 (in the tree) through the nodes of the cycle.
global pos: [0 .. K] init 0;

// Player 1 controls the odd levels, deciding between the two children of the current node.
player p1
	odd_level
endplayer

// Player 2 controls the even levels.
player p2
	even_level
endplayer

module odd_level
	// not last level: choose between two children; chance of 0.2 to get wrong child.
	[] mod(node,2)=1 & node < pow(2,N-2) -> 0.8 : (node'=2*node) + 0.2 : (node'=2*node + 1);
	[] mod(node,2)=1 & node < pow(2,N-2) -> 0.2 : (node'=2*node) + 0.8 : (node'=2*node + 1);

	// last level: go through cycle
	[] mod(node,2)=1 & node >= pow(2,N-2) -> (pos'=mod(pos+1,K));
endmodule

module even_level
	// not last level: choose between two children
	[] mod(node,2)=0 & node < pow(2,N-2) -> 0.8 : (node'=2*node) + 0.2 : (node'=2*node + 1);
	[] mod(node,2)=0 & node < pow(2,N-2) -> 0.2 : (node'=2*node) + 0.8 : (node'=2*node + 1);

	// last level: go through cycle
	[] mod(node,2)=0 & node >= pow(2,N-2) -> (pos'=mod(pos+1,K+1));
endmodule

// Rewards should be kinda randomly distributed throughout the tree, so that the choices are nontrivial
rewards "statereward"
	true : mod(node,N) + pos;
endrewards