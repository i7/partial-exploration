smg

// This SMG is a full binary tree of depth N. Every node consists of a simple MEC containing one player 1 and one player 2 state. Every state can choose to stay in the MEC or use an action to go down one level. Player 1 always chooses the left child node, Player 2 the right one. A coin flip determines whose turn it is first in the next MEC. The leaf nodes of the tree have no exits (otherwise the whole thing would be a MEC; this is TreeMulSEC.prism).


// Number of levels in the tree. Levels are counted from 0 to N-1. state space size is 2 * (2^{N} - 1) (full binary tree of depth N, with two states per node)
const int N;

// Nodes are numbered. 1 is the root on the 0-th level. The i-th level (i in [1,N-1]) contains the nodes from (2^i) to 2^(i+1)-1
global node: [1..pow(2,N-1)] init 1;
// state determines whose turn it is inside a node
global state: [1..2] init 1;

player p1
	module1
endplayer

player p2
	module2
endplayer

module module1
	// stay and give control to other in node is always available
	[] state=1 -> (state'=2);

	// not last level: can choose to exit
	[] state=1 & node < pow(2,N-2) -> 0.5 : (node'=2*node)&(state'=1) + 0.5 : (node'=2*node)&(state'=2); // exit: go to left child (2*node) and randomize whose turn it is
endmodule

module module2
	// stay and give control to other in node is always available
	[] state=2 -> (state'=1);

	// not last level: can choose to exit
	[] state=2 & node < pow(2,N-2) -> 0.5 : (node'=2*node+1)&(state'=1) + 0.5 : (node'=2*node+1)&(state'=2); // exit: go to right child (2*node+1) and randomize whose turn it is
endmodule

// Rewards should be kinda randomly distributed throughout the tree, so that the choices are nontrivial
rewards "statereward"
	true : mod(node,N);
endrewards