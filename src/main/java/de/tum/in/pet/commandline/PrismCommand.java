package de.tum.in.pet.commandline;

import static picocli.CommandLine.Command;
import static picocli.CommandLine.Mixin;

import de.tum.in.pet.util.DefaultResult;
import de.tum.in.pet.util.PrecisionMixin;
import de.tum.in.probmodels.cli.DefaultCli;
import de.tum.in.probmodels.impl.prism.PrismModelMixin;
import de.tum.in.probmodels.problem.Problem;
import de.tum.in.probmodels.problem.verdict.BoundHandler;
import java.io.IOException;
import parser.State;
import picocli.CommandLine.Option;

@SuppressWarnings("PMD.ImmutableField")
@Command(name = "prism", mixinStandardHelpOptions = true)
public final class PrismCommand extends DefaultCli<DefaultResult<?>> {
    @Mixin
    private PrecisionMixin precisionOption;

    @Mixin
    private PrismModelMixin modelOption;

    @Option(names = "--global", description = "Use global solver")
    private boolean global;

    @Option(names = "--property", required = true, description = "Property name / index to check")
    private String expressionName;

    private PrismCommand() {
        // Empty
    }

    @Override
    protected DefaultResult<?> run() throws IOException {
        Problem<State, Object> problem = modelOption.parse().problemByName(expressionName);
        BoundHandler<?> verdict = PrecisionMixin.defaultBoundHandler(problem.query(), precisionOption);

        return global
                ? Checker.solveGlobal(problem, verdict, statistics())
                : Checker.solveSampling(problem, verdict, statistics());
    }
}
