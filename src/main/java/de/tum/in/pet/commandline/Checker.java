package de.tum.in.pet.commandline;

import de.tum.in.pet.global.GlobalMeanPayoffAnalyser;
import de.tum.in.pet.global.GlobalReachAnalyser;
import de.tum.in.pet.global.GlobalResult;
import de.tum.in.pet.sampling.Analyser;
import de.tum.in.pet.sampling.SamplingRunner;
import de.tum.in.pet.sampling.SamplingSelection;
import de.tum.in.pet.sampling.implementation.meanpayoff.MeanPayoffAnalyser;
import de.tum.in.pet.sampling.implementation.reachability.ReachAnalyser;
import de.tum.in.pet.util.DefaultResult;
import de.tum.in.probmodels.cli.StatisticsVerbosity;
import de.tum.in.probmodels.generator.Generator;
import de.tum.in.probmodels.problem.Problem;
import de.tum.in.probmodels.problem.property.MeanPayoffQuantity;
import de.tum.in.probmodels.problem.property.UntilQuantity;
import de.tum.in.probmodels.problem.query.Optimization;
import de.tum.in.probmodels.problem.verdict.BoundHandler;
import de.tum.in.probmodels.values.Bounds;
import java.util.Map;
import java.util.function.Function;
import org.tinylog.Logger;

public final class Checker {
    private Checker() {}

    public static <S> DefaultResult<S> solveGlobal(
            Problem<S, ?> problem, BoundHandler<?> verdict, StatisticsVerbosity verbosity) {
        Logger.info("Checking {} [{}]", problem, verdict);

        Generator<S, ?> generator = problem.model();
        var query = problem.query();

        GlobalResult<S, ?> result;
        if (problem.quantity() instanceof UntilQuantity<S> untilQuantity) {
            if (untilQuantity.hasStepBounds()) {
                throw new UnsupportedOperationException();
            }
            result = GlobalReachAnalyser.solve(
                    generator, untilQuantity, query.optimization().maximizer(), verdict, verbosity);
        } else if (problem.quantity() instanceof MeanPayoffQuantity<S> meanPayoffQuantity) {
            result = GlobalMeanPayoffAnalyser.solve(
                    generator, meanPayoffQuantity, query.optimization().maximizer(), verdict, verbosity);
        } else {
            throw new UnsupportedOperationException("Query of type %s not supported"
                    .formatted(problem.quantity().getClass().getSimpleName()));
        }

        return new DefaultResult<>(
                problem.name(), result.statistics(), Map.copyOf(result.result().asMap()));
    }

    public static <S> DefaultResult<S> solveSampling(
            Problem<S, ?> problem, BoundHandler<?> verdict, StatisticsVerbosity verbosity) {
        Logger.info("Checking {} [{}]", problem, verdict);

        SamplingSelection selection;
        Analyser<S, ?> analyser;
        Function<Bounds, Bounds> boundsMapper;
        if (problem.quantity() instanceof UntilQuantity<S> untilQuantity) {
            if (untilQuantity.hasStepBounds()) {
                throw new UnsupportedOperationException();
            }

            Optimization optimization = problem.query().optimization();
            boundsMapper = untilQuantity.isNegated()
                    ? b -> Bounds.of(1.0 - b.upperBound(), 1.0 - b.lowerBound())
                    : Function.identity();
            analyser = new ReachAnalyser<>(
                    problem.model(),
                    untilQuantity.type(),
                    untilQuantity.isNegated() ? optimization.complement() : optimization);
        } else if (problem.quantity() instanceof MeanPayoffQuantity<S> meanPayoffQuantity) {
            boundsMapper = Function.identity();
            analyser = new MeanPayoffAnalyser<>(
                    problem.model(),
                    meanPayoffQuantity.reward(),
                    meanPayoffQuantity.rewardBounds().orElseThrow(),
                    problem.query().optimization());
        } else {
            throw new UnsupportedOperationException();
        }
        selection = new SamplingSelection(analyser);
        SamplingRunner<S, ?> runner = new SamplingRunner<>(analyser, selection, verdict, boundsMapper);
        var result = runner.solve();
        return new DefaultResult<>(problem.name(), runner.statistics(verbosity), Map.copyOf(result.asMap()));
    }
}
