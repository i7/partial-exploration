package de.tum.in.pet.commandline;

import static picocli.CommandLine.Command;
import static picocli.CommandLine.Mixin;

import com.google.common.base.Stopwatch;
import com.google.gson.annotations.SerializedName;
import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.pet.Main;
import de.tum.in.pet.global.GlobalReachAnalyser;
import de.tum.in.pet.sampling.SamplingRunner;
import de.tum.in.pet.sampling.SamplingSelection;
import de.tum.in.pet.sampling.implementation.core.CoreBuilder;
import de.tum.in.probmodels.cli.DefaultCli;
import de.tum.in.probmodels.cli.StatisticsVerbosity;
import de.tum.in.probmodels.explorer.ModelBuilder;
import de.tum.in.probmodels.explorer.SelfLoopHandling;
import de.tum.in.probmodels.generator.Explore;
import de.tum.in.probmodels.generator.Generator;
import de.tum.in.probmodels.impl.prism.PrismModelMixin;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.model.HyperSystem;
import de.tum.in.probmodels.model.choice.Choice;
import de.tum.in.probmodels.model.impl.DenseCollapsingSystem;
import de.tum.in.probmodels.model.index.DefaultStateBijection;
import de.tum.in.probmodels.output.DefaultStatistics;
import de.tum.in.probmodels.output.ModelStatistics;
import de.tum.in.probmodels.problem.query.Comparison;
import de.tum.in.probmodels.problem.verdict.BoundHandler;
import de.tum.in.probmodels.problem.verdict.QualitativeVerdict;
import de.tum.in.probmodels.util.FloatUtil;
import de.tum.in.probmodels.values.Bounds;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.io.IOException;
import java.time.Duration;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.tinylog.Logger;
import picocli.CommandLine.Option;

@SuppressWarnings("PMD.ImmutableField")
@Command(name = "core", mixinStandardHelpOptions = true)
public final class CoreCommand extends DefaultCli<CoreCommand.CoreStatistics> {
    @Mixin
    private PrismModelMixin modelOption;

    @Option(names = "--precision", description = "Precision (default: ${DEFAULT-VALUE})")
    private double precision = Main.DEFAULT_PRECISION;

    @Option(names = "--validate", description = "Validate core property")
    private boolean validateCoreProperty = false;

    @Option(names = "--components", description = "Analyse components")
    private boolean componentAnalysis = false;

    private record CoreVerdict(double precision) implements BoundHandler<Double> {
        @Override
        public Double interpret(Bounds bounds) {
            return bounds.upperBound();
        }

        @Override
        public boolean isSolved(Bounds bounds) {
            return bounds.upperBound() < precision;
        }
    }

    private CoreCommand() {
        // Empty
    }

    private static <L> void checkCoreProperty(
            double precision, HyperSystem<Choice<L>> system, IntSet coreStates) { // NOPMD
        Logger.info("Checking core property");

        NatBitSet fringeStates = NatBitSets.ensureModifiable(NatBitSets.copyOf(system.states()));
        fringeStates.andNot(coreStates);

        double validationPrecision = precision * 1.001;
        QualitativeVerdict verdict = new QualitativeVerdict(Comparison.LESS_OR_EQUAL, validationPrecision);
        Int2ObjectMap<Bounds> bounds = GlobalReachAnalyser.solve(system, Actor.PROTAGONIST, fringeStates, verdict);
        Logger.info(() -> String.format("Reachability: %s", bounds));

        for (int state : system.initialStates()) {
            double upperBound = bounds.get(state).upperBound();
            if (!FloatUtil.lessOrEqual(upperBound, validationPrecision)) {
                throw new AssertionError(
                        "Core property violated in state %d: %f > %f".formatted(state, upperBound, precision));
            }
        }
    }

    private <S, L> Optional<CoreStatistics> solve(Generator<S, L> model) {
        BoundHandler<Double> verdict = new CoreVerdict(precision);

        Logger.info("Building unbounded core");

        Stopwatch buildingTimer = Stopwatch.createStarted();
        var analyser = new CoreBuilder<>(model);
        var selection = new SamplingSelection(analyser);
        SamplingRunner<S, L> runner = new SamplingRunner<>(analyser, selection, verdict, Function.identity());
        runner.solve();
        var buildingTime = buildingTimer.elapsed();

        Stopwatch extractionTimer = Stopwatch.createStarted();
        Set<S> coreStates = analyser.coreStates();
        var extractionTime = extractionTimer.elapsed();

        if (validateCoreProperty) {
            Logger.info("Building core sub-model");

            NatBitSet coreStateIds = NatBitSets.set();
            DenseCollapsingSystem<Choice<L>> coreSystem = DenseCollapsingSystem.choiceBased();
            DefaultStateBijection<S> indexMap = DefaultStateBijection.withIndices(s -> {
                int state = coreSystem.stateCount();
                coreSystem.addState(state);
                return state;
            });
            for (S coreState : coreStates) {
                Explore<S, L> explore = model.explore(coreState);
                int state = indexMap.addIfAbsent(coreState);
                Collection<Choice<L>> choices = ModelBuilder.explore(
                        state,
                        explore,
                        SelfLoopHandling.KEEP_LOOPS,
                        ModelBuilder.Labels.IGNORE_LABELS,
                        indexMap::addIfAbsent);
                if (choices.isEmpty()) {
                    coreSystem.setState(state, Actor.PROTAGONIST, Choice.onlySingleton(state));
                } else {
                    coreSystem.setState(state, Actor.PROTAGONIST, choices);
                }
                coreStateIds.set(state);
            }
            assert coreSystem.definedStates().equals(coreStateIds);
            NatBitSets.copyOf(coreSystem.undefinedStates())
                    .forEach((int s) -> coreSystem.setState(s, Actor.PROTAGONIST, Choice.onlySingleton(s)));
            coreSystem.setInitialStates(
                    model.initialStates().stream().map(indexMap::indexOf).collect(Collectors.toSet()));

            checkCoreProperty(precision, coreSystem, coreStateIds);
        }

        StatisticsVerbosity verbosity = statistics();
        return verbosity.ifAny(() -> {
            Optional<ModelStatistics> modelStatistics = verbosity.doMore()
                    ? Optional.of(DefaultStatistics.statistics(
                            analyser.system(), analyser.exploredStates(), componentAnalysis))
                    : Optional.empty();
            return new CoreStatistics(
                    modelStatistics, runner.statistics(verbosity), coreStates.size(), buildingTime, extractionTime);
        });
    }

    @Override
    protected CoreStatistics run() throws IOException {
        return solve(modelOption.parse().standardModel()).orElse(null);
    }

    public record CoreStatistics(
            @SerializedName("model") Optional<ModelStatistics> modelStatistics,
            @SerializedName("sampling") Optional<?> samplingStatistics,
            @SerializedName("core_size") int coreSize,
            @SerializedName("building_time") Duration buildingTime,
            @SerializedName("extraction_time") Duration extractionTime) {}
}
