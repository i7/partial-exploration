package de.tum.in.pet.commandline;

import static picocli.CommandLine.Command;
import static picocli.CommandLine.Mixin;

import de.tum.in.pet.util.DefaultResult;
import de.tum.in.pet.util.PrecisionMixin;
import de.tum.in.probmodels.cli.DefaultCli;
import de.tum.in.probmodels.generator.Action;
import de.tum.in.probmodels.generator.Explore;
import de.tum.in.probmodels.generator.Generator;
import de.tum.in.probmodels.generator.RewardGenerator;
import de.tum.in.probmodels.impl.prism.PrismModelMixin;
import de.tum.in.probmodels.impl.prism.PrismProblemInstance;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.problem.Problem;
import de.tum.in.probmodels.problem.property.DefaultMeanPayoffQuantity;
import de.tum.in.probmodels.problem.property.MeanPayoffQuantity;
import de.tum.in.probmodels.problem.property.UntilQuantity;
import de.tum.in.probmodels.problem.property.UntilType;
import de.tum.in.probmodels.problem.verdict.BoundHandler;
import de.tum.in.probmodels.values.Bounds;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import javax.annotation.Nullable;
import parser.State;
import parser.ast.RelOp;
import picocli.CommandLine;
import picocli.CommandLine.Option;

@SuppressWarnings("PMD.ImmutableField")
@Command(name = "mean-payoff", mixinStandardHelpOptions = true)
public final class MeanPayoffCommand extends DefaultCli<DefaultResult<?>> {
    @Mixin
    private PrecisionMixin precisionOption;

    @Mixin
    private PrismModelMixin modelOption;

    @CommandLine.ArgGroup
    private RewardSource rewardSource;

    @Nullable
    @CommandLine.ArgGroup
    private SolverApproach solverApproach;

    private static class FromRewardProperty {
        @Option(names = "--property", required = true, description = "Reward property name / index to check")
        String rewardName;
    }

    private static class FromReachabilityProperty {
        @Option(names = "--reachability", required = true, description = "Reachability property name / index to check")
        String propertyName;
    }

    private static class FromReward {
        @Option(names = "--reward", required = true, description = "Reward property name / index to check")
        String rewardName;

        @Option(names = "--coalition", description = "Names of players in coalition", split = ",")
        List<String> coalitionPlayerNames = List.of();
    }

    private static class RewardSource {
        @Nullable
        @CommandLine.ArgGroup(exclusive = false)
        FromRewardProperty fromRewardProperty;

        @Nullable
        @CommandLine.ArgGroup(exclusive = false)
        FromReachabilityProperty fromReachabilityProperty;

        @Nullable
        @CommandLine.ArgGroup(exclusive = false)
        FromReward fromReward;
    }

    private static class RewardBounds {
        @Option(names = "--min", required = true, description = "Minimal occurring reward")
        double minimalReward = Double.NaN;

        @Option(names = "--max", required = true, description = "Maximal occurring reward")
        double maximalReward = Double.NaN;

        @Nullable
        Bounds bounds() {
            return Double.isFinite(minimalReward) && Double.isFinite(maximalReward)
                    ? Bounds.of(minimalReward, maximalReward)
                    : null;
        }
    }

    private static class SolverApproach {
        @Option(names = "--global", description = "Use global solver")
        boolean global = false;

        @Nullable
        @CommandLine.ArgGroup(exclusive = false)
        private RewardBounds bounds;
    }

    private MeanPayoffCommand() {
        // Empty
    }

    @Override
    protected DefaultResult<?> run() throws IOException {
        PrismProblemInstance problemInstance = modelOption.parse();
        Problem<State, Object> expression;

        @Nullable var fromReward = rewardSource.fromReward;
        @Nullable var fromRewardProperty = rewardSource.fromRewardProperty;
        @Nullable var fromReachProperty = rewardSource.fromReachabilityProperty;
        boolean global = solverApproach != null && solverApproach.global;
        if (fromReachProperty != null) {
            Problem<State, Object> reachExpression = problemInstance.problemByName(fromReachProperty.propertyName);
            var quantity = (UntilQuantity<State>) reachExpression.quantity();
            var model = reachExpression.model();

            Function<State, UntilType> reachability = quantity.type();
            var absorbingModel = new ReachAbsorbingGenerator<>(model, reachability);
            var rewards = new ReachRewardGenerator<>(reachExpression, reachability, quantity.isNegated());

            expression = new Problem<>(
                    reachExpression.name(),
                    absorbingModel,
                    reachExpression.query(),
                    new DefaultMeanPayoffQuantity<>(rewards, Optional.of(Bounds.unknownReach())));
        } else {
            if (fromReward != null) {
                RelOp relOp = fromReward.coalitionPlayerNames.isEmpty() ? RelOp.MAX : RelOp.MAXMIN;
                expression = problemInstance.meanPayoff(
                        fromReward.rewardName,
                        relOp,
                        fromReward.coalitionPlayerNames.isEmpty() ? null : fromReward.coalitionPlayerNames);
            } else if (fromRewardProperty != null) {
                expression = problemInstance.meanPayoffFromReward(fromRewardProperty.rewardName);
            } else {
                throw new IllegalArgumentException();
            }

            if (solverApproach == null) {
                System.err.println("Require a solver approach (either --min & --max or --global)"); // NOPMD
                System.exit(1);
            }
            if (solverApproach.bounds != null) {
                Bounds bounds = Objects.requireNonNull(solverApproach.bounds.bounds());
                expression =
                        expression.withQuantity(((MeanPayoffQuantity<State>) expression.quantity()).withBounds(bounds));
            }
        }

        BoundHandler<?> verdict = PrecisionMixin.defaultBoundHandler(expression.query(), precisionOption);
        return global
                ? Checker.solveGlobal(expression, verdict, statistics())
                : Checker.solveSampling(expression, verdict, statistics());
    }

    private record ReachAbsorbingGenerator<S, L>(Generator<S, L> model, Function<S, UntilType> reachability)
            implements Generator<S, L> {
        @Override
        public Set<S> initialStates() {
            return model.initialStates();
        }

        @Override
        public Explore<S, L> explore(S state) {
            UntilType reachType = reachability.apply(state);
            if (reachType == UntilType.SINK || reachType == UntilType.GOAL) {
                return new Explore<>(Actor.PROTAGONIST, List.of(Action.selfLoop(state)));
            }
            return model.explore(state);
        }

        @Override
        public boolean isDeterministic() {
            return model.isDeterministic();
        }
    }

    private record ReachRewardGenerator<S>(
            Problem<S, ?> reachExpression, Function<S, UntilType> reachability, boolean negated)
            implements RewardGenerator<S> {
        @Override
        public String name() {
            return reachExpression.name();
        }

        @Override
        public double stateReward(S state) {
            if (negated) {
                return reachability.apply(state) == UntilType.GOAL ? 0.0 : 1.0;
            }
            return reachability.apply(state) == UntilType.GOAL ? 1.0 : 0.0;
        }

        @Override
        public double actionReward(S state, @Nullable Object label) {
            return 0;
        }

        @Override
        public boolean mayHaveChoiceRewards() {
            return false;
        }
    }
}
