package de.tum.in.pet;

import static picocli.CommandLine.Command;
import static picocli.CommandLine.Model;
import static picocli.CommandLine.ParameterException;
import static picocli.CommandLine.Spec;

import de.tum.in.pet.commandline.CoreCommand;
import de.tum.in.pet.commandline.MeanPayoffCommand;
import de.tum.in.pet.commandline.PrismCommand;
import de.tum.in.probmodels.cli.PrismModelStatistics;
import picocli.CommandLine;

@SuppressWarnings("PMD.ImmutableField")
@Command(
        name = "pet",
        synopsisSubcommandLabel = "COMMAND",
        version = "2.0",
        mixinStandardHelpOptions = true,
        subcommands = {
            PrismCommand.class,
            CoreCommand.class,
            MeanPayoffCommand.class,
            PrismModelStatistics.class,
        })
public final class Main implements Runnable {
    public static final double DEFAULT_PRECISION = 1.0e-6;

    @Spec
    private Model.CommandSpec spec;

    @Override
    public void run() {
        throw new ParameterException(spec.commandLine(), "Missing required subcommand");
    }

    public static void main(String... args) {
        System.err.println("Invocation: " + String.join(" ", args)); // NOPMD
        System.exit(new CommandLine(new Main()).execute(args));
    }
}
