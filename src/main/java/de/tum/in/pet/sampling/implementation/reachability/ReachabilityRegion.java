package de.tum.in.pet.sampling.implementation.reachability;

import de.tum.in.pet.sampling.SimpleSEC;
import de.tum.in.probmodels.graph.Component;
import de.tum.in.probmodels.model.choice.Choice;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.ArrayList;
import java.util.List;

final class ReachabilityRegion<L> {
    private final int id;
    final Component<Choice<L>> component;
    final List<SimpleSEC<L>> secs = new ArrayList<>();
    final Int2ObjectMap<SimpleSEC<L>> stateToSEC = new Int2ObjectOpenHashMap<>();
    final Int2ObjectMap<Choice<L>> strategy = new Int2ObjectOpenHashMap<>();
    long strategyStale = 0;

    ReachabilityRegion(int id, Component<Choice<L>> component) {
        this.id = id;
        this.component = component;
    }

    @Override
    public boolean equals(Object obj) {
        return this == obj || (obj instanceof ReachabilityRegion<?> other) && id == other.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    public boolean isStale() {
        return strategyStale > 0;
    }

    public IntSet states() {
        return component.states();
    }

    public Component<Choice<L>> component() {
        return component;
    }
}
