package de.tum.in.pet.sampling;

import de.tum.in.pet.global.LogProgress;
import de.tum.in.probmodels.cli.StatisticsVerbosity;
import de.tum.in.probmodels.problem.verdict.BoundHandler;
import de.tum.in.probmodels.problem.verdict.Result;
import de.tum.in.probmodels.values.Bounds;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import javax.annotation.Nullable;
import org.tinylog.Logger;

public class SamplingRunner<S, L> {
    private final Analyser<S, L> analyser;
    private final SamplingSelection selection;
    private final BoundHandler<?> verdict;
    private final Function<Bounds, Bounds> boundMapper;
    private long lastLog;
    private long selections = 0;

    public SamplingRunner(
            Analyser<S, L> analyser,
            SamplingSelection selection,
            BoundHandler<?> verdict,
            Function<Bounds, Bounds> boundMapper) {
        this.analyser = analyser;
        this.selection = selection;
        this.verdict = verdict;
        this.boundMapper = boundMapper;
    }

    private void logProgress(boolean ignoreTime) {
        if (!Logger.isInfoEnabled()) {
            return;
        }
        long now = System.currentTimeMillis();
        if (!ignoreTime && now - lastLog < 5000) {
            return;
        }
        lastLog = now;

        String initialString = LogProgress.formatInitialStates(
                analyser.initialStates(), Function.identity(), state -> analyser.bounds(analyser.state(state)));
        @Nullable String analyserProgress = analyser.progressString();
        @Nullable String selectionProgress = selection.progressString();

        Logger.info(
                "Step {} Progress report:\nBounds: {}{}{}",
                selections,
                initialString,
                analyserProgress == null ? "" : "\n" + analyserProgress,
                selectionProgress == null ? "" : "\n" + selectionProgress);
    }

    public Result<S, ?> solve() {
        var initial = analyser.initialStates();

        lastLog = System.currentTimeMillis();
        Map<S, Bounds> stateBounds = new HashMap<>();
        for (S state : initial) {
            while (true) {
                selections += 1;
                Bounds bounds = selection.select(analyser.state(state));
                Bounds mapped = boundMapper.apply(bounds);
                if (verdict.isSolved(mapped)) {
                    stateBounds.put(state, mapped);
                    Logger.debug("Solved state {} with bounds {} ({})", state, mapped, verdict.interpret(mapped));
                    break;
                }
                logProgress(false);
            }
        }
        logProgress(true);
        return Result.of(initial, verdict, stateBounds::get);
    }

    public Optional<?> statistics(StatisticsVerbosity verbosity) {
        return verbosity.ifAny(() ->
                new SamplingStatistics(selection.statistics(verbosity), analyser.statistics(verbosity), selections));
    }
}
