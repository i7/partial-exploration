package de.tum.in.pet.sampling.implementation.meanpayoff;

import de.tum.in.probmodels.graph.Component;
import de.tum.in.probmodels.model.choice.Choice;

sealed interface MeanPayoffRegion<L> permits GameRegion, NontrivialRegion, OwnedRegion, SingletonRegion {
    Component<Choice<L>> component();
}
