package de.tum.in.pet.sampling.implementation.meanpayoff;

import com.google.common.collect.Iterables;
import de.tum.in.pet.sampling.SEC;
import de.tum.in.pet.sampling.SimpleTrackingSEC;
import de.tum.in.pet.sampling.TrackingSEC;
import de.tum.in.probmodels.graph.Component;
import de.tum.in.probmodels.graph.ComponentAnalyser;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.model.ControlledSystem;
import de.tum.in.probmodels.model.RewardFunction;
import de.tum.in.probmodels.model.choice.Choice;
import de.tum.in.probmodels.util.FloatUtil;
import de.tum.in.probmodels.values.Bounds;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.Nullable;

@SuppressWarnings("PMD.TooManyFields")
final class GameRegion<L> extends NontrivialRegion<L> implements MeanPayoffRegion<L> {
    // TODO Adaptively choose
    private static final double alpha = 0.9;

    private final ControlledSystem<Choice<L>> system;
    private final Actor maximizer;

    private Bounds overallBounds;
    private int iterationBound;
    private boolean boundsConvergedPastInitial = false;
    private boolean strategyChanged = true;

    private double targetPrecision;
    private final Int2ObjectMap<Choice<L>> strategy;
    private final ComponentAnalyser<Choice<L>, ? extends Component<Choice<L>>> componentAnalyser;
    private final List<SimpleTrackingSEC<L, Bounds>> maximizerSECs = new ArrayList<>();
    private final List<SimpleTrackingSEC<L, Bounds>> minimizerSECs = new ArrayList<>();
    private final Int2ObjectMap<SimpleTrackingSEC<L, Bounds>> stateToSEC = new Int2ObjectOpenHashMap<>();

    GameRegion(
            ControlledSystem<Choice<L>> system,
            Component<Choice<L>> component,
            Actor maximizer,
            RewardFunction rewards,
            Bounds rewardBounds,
            Int2ObjectMap<Choice<L>> strategy,
            Int2DoubleMap rewardOne,
            Int2DoubleMap rewardOther,
            ComponentAnalyser<Choice<L>, ? extends Component<Choice<L>>> componentAnalyser) {
        super(component, rewards, rewardOne, rewardOther);
        this.system = system;
        this.maximizer = maximizer;
        this.componentAnalyser = componentAnalyser;
        this.strategy = strategy;
        iterationBound = (component.size() / 2 + 1);
        this.overallBounds = rewardBounds;
        updateSECs();
    }

    private Bounds iterate(
            Component<Choice<L>> component,
            Int2DoubleMap currentValues,
            Int2DoubleMap nextValues,
            boolean trackStrategy,
            boolean update) {
        IntIterator stateIterator = component.states().iterator();
        double minimalDifference = Double.POSITIVE_INFINITY;
        double maximalDifference = Double.NEGATIVE_INFINITY;
        boolean anyChange = strategyChanged;

        if (trackStrategy) {
            strategy.clear();
        }

        while (stateIterator.hasNext()) {
            int state = stateIterator.nextInt();
            double currentValue = currentValues.get(state);
            double stateValue = rewards.stateReward(state);

            boolean maximizing = system.control(state) == maximizer;
            double optimum = maximizing ? Double.NEGATIVE_INFINITY : Double.POSITIVE_INFINITY;
            Choice<L> witness = null;
            List<Choice<L>> choices = component.choices(state);
            assert !choices.isEmpty();
            for (Choice<L> choice : choices) {
                double reward = rewards.choiceReward(state, choice.label()) / alpha;
                double value = choice.sumWeighted(currentValues) + reward;
                if (maximizing) {
                    if (value > optimum) {
                        optimum = value;
                        witness = choice;
                    }
                } else {
                    if (value < optimum) {
                        optimum = value;
                        witness = choice;
                    }
                }
            }
            if (trackStrategy && choices.size() > 1) {
                Choice oldWitness = strategy.put(state, witness);
                anyChange = anyChange || !Objects.equals(witness, oldWitness);
            }

            FloatUtil.KahanSum valueSum = new FloatUtil.KahanSum(stateValue);
            valueSum.add(alpha * optimum);
            double nextValue = valueSum.add((1 - alpha) * currentValue);
            if (update) {
                nextValues.put(state, nextValue);
            }
            double difference = valueSum.add(-currentValue);
            if (difference < minimalDifference) {
                minimalDifference = difference;
            }
            if (difference > maximalDifference) {
                maximalDifference = difference;
            }
        }
        strategyChanged = anyChange;
        return Bounds.of(minimalDifference, maximalDifference);
    }

    private Iteration iterateTotalReward(int iterationBound) {
        Int2DoubleMap currentValues = swapped ? totalRewardOne : totalRewardOther;
        Int2DoubleMap nextValues = swapped ? totalRewardOther : totalRewardOne;

        int steps = 0;
        Bounds currentBounds = this.overallBounds;
        //noinspection ObjectEquality
        while (steps < iterationBound && currentBounds.difference() >= targetPrecision) {
            Bounds nextBounds = iterate(component, currentValues, nextValues, false, true);
            Int2DoubleMap swap = nextValues;
            nextValues = currentValues;
            currentValues = swap;

            if (boundsConvergedPastInitial) {
                assert currentBounds.contains(nextBounds, FloatUtil.WEAK_EPS)
                        : "%s does not contain %s".formatted(currentBounds, nextBounds);
                currentBounds = nextBounds;
            } else {
                if (currentBounds.contains(nextBounds)) {
                    currentBounds = nextBounds;
                    boundsConvergedPastInitial = true;
                } else {
                    currentBounds = nextBounds.shrink(currentBounds);
                }
            }
            steps += 1;
        }
        swapped = currentValues != totalRewardOne; // NOPMD
        iterate(component, currentValues, nextValues, true, true);
        swapped = !swapped;
        if (strategyChanged) {
            updateSECs();
        }
        return new Iteration(currentBounds, steps);
    }

    @Override
    public Component<Choice<L>> component() {
        return component;
    }

    private void addComponents(Actor control, List<SimpleTrackingSEC<L, Bounds>> list) {
        for (Component<Choice<L>> sec : componentAnalyser.findComponents(
                SEC.control(component, control, system::control, s -> true, strategy::get), component.states())) {
            SimpleTrackingSEC<L, Bounds> secBounds = SimpleTrackingSEC.of(system, sec, control, overallBounds);
            list.add(secBounds);
            sec.states().forEach(s -> {
                if (system.control(s) == control) {
                    stateToSEC.put(s, secBounds);
                }
            });
        }
    }

    private void updateSECs() {
        Int2DoubleMap currentValues = swapped ? totalRewardOne : totalRewardOther;
        Int2DoubleMap nextValues = swapped ? totalRewardOther : totalRewardOne;

        if (strategyChanged) {
            stateToSEC.clear();
            maximizerSECs.clear();
            minimizerSECs.clear();

            addComponents(maximizer, maximizerSECs);
            addComponents(maximizer.opponent(), minimizerSECs);
            strategyChanged = false;
        }

        for (var secBounds : Iterables.concat(minimizerSECs, maximizerSECs)) {
            secBounds.set(iterate(secBounds.component(), nextValues, currentValues, false, false));
        }
    }

    @Override
    public int update() {
        if (FloatUtil.isZero(overallBounds.difference())) {
            return 0;
        }
        Iteration iteration = iterateTotalReward(this.iterationBound);
        Bounds resultBounds = iteration.result();
        assert this.overallBounds.contains(resultBounds, FloatUtil.WEAK_EPS);
        this.overallBounds = resultBounds;
        if (iteration.iterations >= iterationBound) {
            iterationBound += component.size() * (maximizerSECs.size() + minimizerSECs.size() + 1);
        }
        if (overallBounds.difference() < targetPrecision) {
            targetPrecision /= 2;
        }
        return (iteration.iterations + 1) * component.size();
    }

    @Nullable
    public Bounds staying(int state) {
        var sec = stateToSEC.get(state);
        return sec == null ? null : sec.get();
    }

    public Iterable<TrackingSEC<L, Bounds>> secs() {
        return Iterables.concat(maximizerSECs, minimizerSECs);
    }

    @Nullable
    public SEC<L> sec(int state) {
        return stateToSEC.get(state);
    }

    private record Iteration(Bounds result, int iterations) {}

    @Override
    public String toString() {
        return "NT[%s / %s]".formatted(overallBounds, component);
    }
}
