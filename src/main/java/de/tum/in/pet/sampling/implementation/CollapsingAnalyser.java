package de.tum.in.pet.sampling.implementation;

import de.tum.in.pet.sampling.Analyser;
import de.tum.in.probmodels.generator.Generator;
import de.tum.in.probmodels.graph.ComponentAnalyser;
import de.tum.in.probmodels.model.HyperSystem;
import de.tum.in.probmodels.model.choice.Choice;
import de.tum.in.probmodels.model.impl.DenseCollapsingSystem;
import de.tum.in.probmodels.model.index.DefaultStateBijection;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.Collection;

public abstract class CollapsingAnalyser<S, L> implements Analyser<S, L> {
    protected final Generator<S, L> model;
    protected final DefaultStateBijection<S> indexMap;
    protected final DenseCollapsingSystem<Choice<L>> system;
    protected final ComponentAnalyser<Choice<L>, ?> componentAnalyser;

    protected CollapsingAnalyser(Generator<S, L> model) {
        this.model = model;
        this.system = DenseCollapsingSystem.choiceBased();
        this.componentAnalyser = ComponentAnalyser.defaultAnalyser(system);

        indexMap = DefaultStateBijection.withIndices(s -> {
            int id = makeState(s);
            system.addState(id);
            return id;
        });
    }

    protected abstract int makeState(S state);

    public HyperSystem<Choice<L>> system() {
        return system;
    }

    @Override
    public boolean isExplored(int state) {
        return system.isDefinedState(state);
    }

    @Override
    public IntSet exploredStates() {
        return system.definedStates();
    }

    @Override
    public Collection<S> initialStates() {
        return model.initialStates();
    }

    @Override
    public int state(S state) {
        return system.representative(indexMap.indexOf(state));
    }
}
