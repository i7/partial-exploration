package de.tum.in.pet.sampling;

import de.tum.in.probmodels.graph.Component;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.model.ControlledSystem;
import de.tum.in.probmodels.model.choice.Choice;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.IntUnaryOperator;

public record SimpleSEC<L>(Component<Choice<L>> component, Actor owner, List<Choice<L>> ownerExits) implements SEC<L> {
    public static <L> SimpleSEC<L> of(ControlledSystem<Choice<L>> system, Component<Choice<L>> component, Actor owner) {
        return new SimpleSEC<>(component, owner, SEC.exits(system, component.states(), owner));
    }

    @Override
    public String toString() {
        return component.toString();
    }

    public void mapExits(IntUnaryOperator operator) {
        Set<Choice<L>> mapped = new HashSet<>(ownerExits.size());
        if (SEC.mapExits(this, operator, mapped::add)) {
            ownerExits.clear();
            ownerExits.addAll(mapped);
        } else {
            assert Set.copyOf(ownerExits).equals(mapped);
        }
    }
}
