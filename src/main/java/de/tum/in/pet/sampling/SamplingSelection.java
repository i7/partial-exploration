package de.tum.in.pet.sampling;

import de.tum.in.probmodels.cli.StatisticsVerbosity;
import de.tum.in.probmodels.util.FloatUtil;
import de.tum.in.probmodels.values.Bounds;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.Optional;

@SuppressWarnings({"PMD.TooManyFields", "PMD.UnusedAssignment", "PMD.UnusedLocalVariable"})
public final class SamplingSelection implements UpdateSelection {
    private static final int MAX_BACKTRACK_PER_SAMPLE = 4;
    private static final int MAX_EXPLORES_PER_SAMPLE = 4;

    private final IntSet visitedStateSet = new IntOpenHashSet(128);

    @SuppressWarnings("PMD.LooseCoupling")
    private final IntArrayList samples = new IntArrayList(128);

    private long loopCount = 0;
    private long backtrackCount = 0;
    private long backtrackToInitialCount = 0;
    private final SelectionGuidance<?> selection;

    public SamplingSelection(SelectionGuidance<?> selection) {
        this.selection = selection;
    }

    @Override
    public Bounds select(int initialState) {
        assert selection.isExplored(initialState);
        @SuppressWarnings("PMD.LooseCoupling")
        IntArrayList samples = this.samples;
        IntSet visitedStateSet = this.visitedStateSet;

        assert samples.isEmpty();
        visitedStateSet.clear();

        int exploreCount = 0;
        int samplingState = initialState;
        int sampleBacktraceCount = 0;
        int stateRevisit = 0;
        while (stateRevisit < 10) {
            int currentState = samplingState;
            assert selection.isExplored(currentState);

            if (!visitedStateSet.add(currentState)) {
                stateRevisit += 1;
            }
            int nextState;
            var optimal = selection.selectOptimal(currentState);
            if (optimal == null) {
                nextState = -1;
            } else {
                nextState =
                        optimal.sampleWeightedExcept((s, p) -> p * selection.difference(s), visitedStateSet::contains);
                assert nextState == -1 || optimal.hasSuccessor(nextState);
            }

            if (nextState == -1) {
                selection.update(currentState);
                if (sampleBacktraceCount == MAX_BACKTRACK_PER_SAMPLE || currentState == initialState) {
                    break;
                }

                // We won't find anything of value if we continue to follow this path, backtrace until we find an
                // interesting state again
                // Note: We might as well completely restart the sampling here, but then we potentially have to move to
                // this interesting "fringe" region again
                double difference;
                double updatedDifference;
                do {
                    backtrackCount += 1;
                    samplingState = samples.popInt();
                    visitedStateSet.remove(samplingState);
                    difference = selection.difference(samplingState);
                    updatedDifference = selection.update(samplingState).difference();
                    loopCount += 1;
                } while (FloatUtil.isEqual(difference, updatedDifference) && samplingState != initialState);
                if (samplingState == initialState) {
                    backtrackToInitialCount += 1;
                    break;
                }
                sampleBacktraceCount += 1;
            } else {
                samples.push(currentState);
                if (!selection.isExplored(nextState)) {
                    if (exploreCount == MAX_EXPLORES_PER_SAMPLE) {
                        break;
                    }
                    exploreCount += 1;
                    selection.explore(nextState);
                }
                samplingState = nextState;
            }
        }

        while (!samples.isEmpty()) {
            int state = samples.popInt();
            selection.update(state);
        }
        Bounds bounds = selection.bounds(initialState);
        selection.afterIteration(initialState);
        return bounds;
    }

    @Override
    public Optional<SamplingSelectionStatistics> statistics(StatisticsVerbosity verbosity) {
        return verbosity.ifAny(
                () -> new SamplingSelectionStatistics(loopCount, backtrackCount, backtrackToInitialCount));
    }

    @Override
    public String progressString() {
        return "Sampling: %d loops, %d backtracks (%d to initial)"
                .formatted(loopCount, backtrackCount, backtrackToInitialCount);
    }
}
