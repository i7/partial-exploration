package de.tum.in.pet.sampling.implementation.meanpayoff;

import de.tum.in.pet.sampling.SEC;
import de.tum.in.probmodels.graph.Component;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.model.Hypergraph;
import de.tum.in.probmodels.model.RewardFunction;
import de.tum.in.probmodels.model.choice.Choice;
import de.tum.in.probmodels.values.Bounds;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

final class SingletonRegion<L> implements MeanPayoffRegion<L>, Component<Choice<L>>, SEC<L> {
    private final Bounds bounds;
    private final int state;
    private final List<Choice<L>> exits;
    private final Actor owner;

    SingletonRegion(Hypergraph<Choice<L>> system, int state, RewardFunction rewards, Actor owner, Actor maximizer) {
        this.state = state;
        this.owner = owner;

        List<Choice<L>> choices = system.choices(state);

        if (choices.size() == 1) {
            var c = choices.get(0);
            assert c.isOnlySuccessor(state);
            exits = List.of();
            bounds = Bounds.of(rewards.stateReward(state) + rewards.choiceReward(state, c.label()));
        } else {
            double value;
            Set<Choice<L>> exits = new HashSet<>();
            if (owner == maximizer) {
                double maximum = Double.NEGATIVE_INFINITY;
                for (Choice<L> choice : choices) {
                    if (choice.isOnlySuccessor(state)) {
                        double val = rewards.stateReward(state) + rewards.choiceReward(state, choice.label());
                        if (val > maximum) {
                            maximum = val;
                        }
                    } else {
                        exits.add(choice.removeSuccessor(state).orElseThrow());
                    }
                }
                assert Double.isFinite(maximum);
                value = maximum;
            } else {
                double minimum = Double.POSITIVE_INFINITY;
                for (Choice<L> choice : choices) {
                    if (choice.isOnlySuccessor(state)) {
                        double val = rewards.stateReward(state) + rewards.choiceReward(state, choice.label());
                        if (val < minimum) {
                            minimum = val;
                        }
                    } else {
                        exits.add(choice.removeSuccessor(state).orElseThrow());
                    }
                }
                assert Double.isFinite(minimum);
                value = minimum;
            }
            this.exits = List.copyOf(exits);
            bounds = Bounds.of(value);
        }
    }

    @Override
    public IntSet states() {
        return IntSet.of(state);
    }

    @Override
    public List<Choice<L>> choices(int state) {
        assert state == this.state;
        return Choice.onlySingleton(state);
    }

    public Bounds bounds() {
        return bounds;
    }

    @Override
    public Component<Choice<L>> component() {
        return this;
    }

    @Override
    public Actor owner() {
        return owner;
    }

    @Override
    public List<Choice<L>> ownerExits() {
        return exits;
    }

    public int state() {
        return state;
    }
}
