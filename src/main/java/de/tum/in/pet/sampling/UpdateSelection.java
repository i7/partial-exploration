package de.tum.in.pet.sampling;

import de.tum.in.probmodels.cli.StatisticsVerbosity;
import de.tum.in.probmodels.values.Bounds;
import java.util.Optional;
import javax.annotation.Nullable;

public interface UpdateSelection {
    Bounds select(int initialState);

    default Optional<?> statistics(StatisticsVerbosity verbosity) {
        return Optional.empty();
    }

    @Nullable
    default String progressString() {
        return null;
    }
}
