package de.tum.in.pet.sampling;

import de.tum.in.probmodels.graph.Component;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.model.ControlledSystem;
import de.tum.in.probmodels.model.choice.Choice;
import java.util.List;
import org.tinylog.Logger;

public final class SimpleTrackingSEC<L, T> implements TrackingSEC<L, T> {
    private final Component<Choice<L>> component;
    private final Actor owner;
    private final List<Choice<L>> ownerExits;
    private T value;

    public SimpleTrackingSEC(Component<Choice<L>> component, Actor owner, List<Choice<L>> ownerExits, T value) {
        this.component = component;
        this.owner = owner;
        this.ownerExits = ownerExits;
        this.value = value;
    }

    public static <L, T> SimpleTrackingSEC<L, T> of(
            ControlledSystem<Choice<L>> system, Component<Choice<L>> component, Actor owner, T value) {
        return new SimpleTrackingSEC<>(component, owner, SEC.exits(system, component.states(), owner), value);
    }

    @Override
    public String toString() {
        return component.toString();
    }

    public void set(T value) {
        this.value = value;
    }

    @Override
    public T get() {
        return this.value;
    }

    @Override
    public Component<Choice<L>> component() {
        return component;
    }

    @Override
    public Actor owner() {
        return owner;
    }

    @Override
    public List<Choice<L>> ownerExits() {
        return ownerExits;
    }

    @Override
    public boolean equals(Object obj) {
        Logger.warn("Equals on tracking SEC");
        return this == obj;
    }

    @Override
    public int hashCode() {
        Logger.warn("HashCode on tracking SEC");
        return super.hashCode();
    }
}
