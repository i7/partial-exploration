package de.tum.in.pet.sampling.implementation.meanpayoff;

import static de.tum.in.probmodels.explorer.ModelBuilder.Labels.EXPLORE_LABELS;
import static de.tum.in.probmodels.explorer.SelfLoopHandling.KEEP_LOOPS;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Collections2;
import com.google.gson.annotations.SerializedName;
import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.pet.sampling.SEC;
import de.tum.in.pet.sampling.TrackingSEC;
import de.tum.in.pet.sampling.implementation.CheckHeuristics;
import de.tum.in.pet.sampling.implementation.CollapsingAnalyser;
import de.tum.in.pet.sampling.implementation.ComponentTracker;
import de.tum.in.probmodels.cli.StatisticsVerbosity;
import de.tum.in.probmodels.explorer.ModelBuilder;
import de.tum.in.probmodels.explorer.RewardCache;
import de.tum.in.probmodels.generator.Generator;
import de.tum.in.probmodels.generator.RewardGenerator;
import de.tum.in.probmodels.graph.Component;
import de.tum.in.probmodels.graph.ComponentAnalyser;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.model.choice.Choice;
import de.tum.in.probmodels.model.impl.RestrictedGraphView;
import de.tum.in.probmodels.output.ComponentsStatistics;
import de.tum.in.probmodels.output.DefaultStatistics;
import de.tum.in.probmodels.problem.query.Optimization;
import de.tum.in.probmodels.util.FloatUtil;
import de.tum.in.probmodels.util.Sample;
import de.tum.in.probmodels.values.Bounds;
import de.tum.in.probmodels.values.Selection;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntIterator;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import javax.annotation.Nullable;
import org.tinylog.Logger;

@SuppressWarnings({"PMD.TooManyFields", "PMD.UnusedAssignment", "PMD.UnusedLocalVariable"})
public final class MeanPayoffAnalyser<S, L> extends CollapsingAnalyser<S, L> {
    // TODO Collapse converged states
    // TODO Full "two-level" approach

    private final Optimization optimization;
    private final Actor maximizingActor;

    // Total reward data
    private final Int2DoubleMap totalRewardOne = new Int2DoubleOpenHashMap();
    private final Int2DoubleMap totalRewardOther = new Int2DoubleOpenHashMap();
    private final Int2ObjectMap<Choice<L>> strategy = new Int2ObjectOpenHashMap<>();

    // Propagation data
    private final Bounds rewardBounds;
    private final Int2ObjectMap<Bounds> propagationBounds = new Int2ObjectOpenHashMap<>();

    private final NatBitSet modifiedSinceComponentSearch = NatBitSets.set();
    private final RewardCache<S> rewardCache;
    private final ComponentTracker<MeanPayoffRegion<L>, Choice<L>> regions =
            new ComponentTracker<>(MeanPayoffRegion::component);
    private int stateCounter = 0;

    private final CheckHeuristics checkHeuristics;
    private final Set<NontrivialRegion<L>> staleRegions = new HashSet<>();
    private final MeanPayoffStatistics s = new MeanPayoffStatistics();

    public MeanPayoffAnalyser(
            Generator<S, L> model, RewardGenerator<S> rewards, Bounds rewardBounds, Optimization optimization) {
        super(model);
        this.checkHeuristics = new CheckHeuristics(system);
        this.rewardCache = new RewardCache<>(rewards);
        this.optimization = optimization;
        maximizingActor = optimization.maximizer();
        this.rewardBounds = rewardBounds;

        for (S initialState : model.initialStates()) {
            explore(indexMap.addState(initialState));
        }
    }

    @Override
    protected int makeState(S state) {
        return stateCounter++;
    }

    @Override
    public void explore(int state) {
        assert system.representative(state) == state : state;
        assert indexMap.check(state);
        s.exploredStates += 1;

        S stateObject = indexMap.getState(state);
        var explore = model.explore(stateObject);
        Collection<Choice<L>> choices = ModelBuilder.explore(
                state, explore, KEEP_LOOPS, EXPLORE_LABELS, s -> system.representative(indexMap.addIfAbsent(s)));

        modifiedSinceComponentSearch.set(state);
        system.setState(state, explore.actor(), choices.isEmpty() ? Choice.onlySingleton(state) : choices);
        rewardCache.exploreReward(stateObject, state, choices);
    }

    @Nullable
    @Override
    public Choice<L> selectOptimal(int state) {
        var region = regions.component(state);
        var choices = choices(region, state);
        if (choices.isEmpty()) {
            return null;
        }
        if (choices.size() == 1) {
            Choice<L> choice = choices.get(0);
            return choice.isOnlySuccessor(state) ? null : choice;
        }
        // TODO We have staying bounds available here -- can we somehow mix it in? Return null
        //      if staying is selected and mark SEC/Region for update
        boolean maximizerState = system.control(state) == maximizingActor;
        ToDoubleFunction<Choice<L>> scoreFunction = maximizerState
                ? d -> d.sumWeightedExceptJacobi(this::upperBound, state)
                : d -> -d.sumWeightedExceptJacobi(this::lowerBound, state);
        return Sample.getOptimal(choices, scoreFunction);
    }

    @Override
    public Bounds bounds(int state) {
        return propagationBounds.getOrDefault(state, rewardBounds);
    }

    @Override
    public boolean isUnknown(int state) {
        var bounds = this.propagationBounds.get(state);
        return bounds == null || rewardBounds.equalsUpTo(bounds);
    }

    @Nullable
    private Bounds getStaying(@Nullable MeanPayoffRegion<L> region, int state) {
        assert region == null || region.component().contains(state);
        if (region instanceof SingletonRegion<L> singleton) {
            return singleton.bounds();
        }
        if (region instanceof OwnedRegion<L> ownedRegion) {
            return ownedRegion.staying();
        }
        if (region instanceof GameRegion<L> gameRegion) {
            return gameRegion.staying(state);
        }
        assert region == null;
        return null;
    }

    private void markRegionForUpdate(NontrivialRegion<L> region, boolean flate) {
        region.markStale(flate);
        staleRegions.add(region);
    }

    @Override
    public Bounds update(int state) {
        assert isExplored(state);
        checkHeuristics.onStateUpdate();
        s.reachUpdates += 1;

        var choices = system.choices(state);
        assert !choices.isEmpty();
        assert optimization != Optimization.UNIQUE_VALUE || choices.size() == 1;

        // Note: The .orElse can be reached if, e.g., this state is an absorbing state
        // but has not yet been identified as component
        // TODO It should *only* happen in this case? Can we use it?
        Function<Choice<L>, Bounds> bounds =
                d -> d.sumWeightedExceptJacobiBounds(this::bounds, state).orElse(rewardBounds);
        Selection selection = optimization.selection(system.control(state));

        var region = regions.component(state);
        List<Choice<L>> distributions = choices(region, state);
        if (distributions.isEmpty()) {
            Bounds staying = getStaying(region, state);
            if (staying == null) {
                return rewardBounds;
            }
            assert region != null;
            if (region instanceof NontrivialRegion<L> nontrivial) {
                markRegionForUpdate(nontrivial, false);
            }
            // TODO Can this be delayed?
            region.component().states().forEach((s -> propagationBounds.put(s, staying)));
            //  flateBounds(selection, propagationBounds.getOrDefault(s, rewardBounds), staying))));
            return staying;
        }

        Bounds newBounds = selection.select(distributions, bounds);
        @Nullable Bounds staying = getStaying(region, state);
        if (staying != null) {
            if (region instanceof NontrivialRegion<L> nontrivial) {
                boolean update;
                boolean flate;
                if (selection == Selection.MAX_VALUE) {
                    update = FloatUtil.lessOrEqual(newBounds.upperBound(), staying.upperBound());
                    flate = !FloatUtil.strictlyLess(newBounds.upperBound(), staying.lowerBound());
                } else {
                    update = FloatUtil.lessOrEqual(staying.lowerBound(), newBounds.lowerBound());
                    flate = !FloatUtil.strictlyLess(staying.upperBound(), newBounds.lowerBound());
                }
                if (update || flate) {
                    markRegionForUpdate(nontrivial, true);
                }
            }
            if (!(region instanceof GameRegion)) {
                // Note: This automatically takes care of flating single-state components
                newBounds = selection.select(List.of(newBounds, staying));
            }
        }

        if (newBounds.equalsUpTo(rewardBounds)) {
            return rewardBounds;
        }

        Bounds oldBounds = this.propagationBounds.put(state, newBounds);
        // assert oldBounds == null || oldBounds.contains(newBounds, Util.WEAK_EPS) : oldBounds + " " + newBounds;
        return newBounds;
    }

    @Override
    public void afterIteration(int state) {
        if (checkHeuristics.shouldCheckComponents(modifiedSinceComponentSearch)) {
            checkHeuristics.afterComponentCheck(discoverComponents());
        }
        for (NontrivialRegion<L> region : staleRegions) {
            if (!region.needsUpdate()) {
                continue;
            }
            NontrivialRegion.UpdateMark mark = region.getAndResetMark();
            assert mark != NontrivialRegion.UpdateMark.NONE;

            s.totalRewardIterationTime.start();
            int stateUpdates = region.update();
            s.totalRewardIterationTime.stop();
            s.totalRewardUpdates += stateUpdates;
            checkHeuristics.onStateUpdates(stateUpdates);
            s.flateTime.start();
            if (mark == NontrivialRegion.UpdateMark.UPDATE_AND_FLATE) {
                if (region instanceof GameRegion<L> game) {
                    for (TrackingSEC<L, Bounds> sec : game.secs()) {
                        flate(sec, sec.get());
                    }
                } else if (region instanceof OwnedRegion<L> nontrivial) {
                    flate(nontrivial, nontrivial.staying());
                }
            }
            s.flateTime.stop();
        }
        staleRegions.clear();
    }

    private List<Choice<L>> choices(@Nullable MeanPayoffRegion<L> region, int state) {
        if (region instanceof SingletonRegion<L> singleton) {
            return singleton.ownerExits();
        } else if (region instanceof OwnedRegion<L> owned) {
            return owned.ownerExits();
        }
        // TODO The below is wrong!
        /*else if (region instanceof GameRegion game) {

            var sec = game.sec(state);
            if (sec != null) {
                return sec.ownerExits();
            }
        }*/
        return system.choices(state);
    }

    private boolean flate(SEC<L> sec, Bounds stayingBounds) {
        if (sec.owner() == maximizingActor) {
            s.deflateCount += 1;
            double bestValue = stayingBounds.upperBound();
            for (Choice<L> choice : sec.ownerExits()) {
                double exit = choice.sumWeighted(this::upperBound);
                if (exit > bestValue) {
                    bestValue = exit;
                }
            }
            if (FloatUtil.isEqual(bestValue, rewardBounds.upperBound())) {
                return false;
            }
            // TODO Case where bestValue == rewardBounds.lowerBound()

            Logger.trace("Deflating SEC to {} (staying {}): {}", bestValue, stayingBounds, sec);
            IntIterator secStateIterator = sec.component().states().intIterator();
            while (secStateIterator.hasNext()) {
                int s = secStateIterator.nextInt();
                var bounds = this.propagationBounds.getOrDefault(s, rewardBounds);
                if (bounds.upperBound() > bestValue) {
                    checkHeuristics.onStateUpdate();
                    this.propagationBounds.put(s, bounds.withUpper(bestValue));
                }
            }
            return bestValue > stayingBounds.upperBound();
        } else {
            s.inflateCount += 1;
            double bestValue = stayingBounds.lowerBound();
            for (Choice<L> choice : sec.ownerExits()) {
                double exit = choice.sumWeighted(this::lowerBound);
                if (exit < bestValue) {
                    bestValue = exit;
                }
            }
            if (FloatUtil.isEqual(bestValue, rewardBounds.lowerBound())) {
                return false;
            }

            Logger.trace("Inflating SEC to {} (staying {}): {}", bestValue, stayingBounds, sec);
            IntIterator secStateIterator = sec.component().states().intIterator();
            while (secStateIterator.hasNext()) {
                int s = secStateIterator.nextInt();
                var bounds = this.propagationBounds.getOrDefault(s, rewardBounds);
                if (bounds.lowerBound() < bestValue) {
                    checkHeuristics.onStateUpdate();
                    this.propagationBounds.put(s, bounds.withLower(bestValue));
                }
            }
            return bestValue < stayingBounds.lowerBound();
        }
    }

    private boolean discoverComponents() {
        assert system.areDefinedStates(modifiedSinceComponentSearch);

        Logger.debug(
                "Searching for components in overall system with {} states, starting from {} states",
                system.definedStates().size(),
                modifiedSinceComponentSearch.size());
        s.componentSearches += 1;
        s.componentSearchTime.start();
        var exploredSystem = RestrictedGraphView.of(system, system.definedStates());
        var foundComponents = componentAnalyser.findComponents(
                exploredSystem, modifiedSinceComponentSearch, ComponentAnalyser.ComponentCopyType.EAGER);
        s.componentSearchTime.stop();
        modifiedSinceComponentSearch.clear();

        List<Component<Choice<L>>> newRegions = regions.filterNew(foundComponents);
        if (newRegions.isEmpty()) {
            Logger.debug("Found no new components");
            return false;
        }
        Logger.debug("Found {} new regions", newRegions.size());
        s.discoveredRegions += newRegions.size();

        for (Component<Choice<L>> component : newRegions) {
            // TODO Special region if all states-action pairs have the same reward

            MeanPayoffRegion<L> region;
            if (component.size() == 1) {
                int state = component.states().intIterator().nextInt();
                region = new SingletonRegion<>(system, state, rewardCache, system.control(state), maximizingActor);
            } else {
                boolean antagonistControl = system.antagonistControlledStates().intersects(component.states());
                boolean game = antagonistControl
                        && system.protagonistControlledStates().intersects(component.states());

                if (game) {
                    region = new GameRegion<>(
                            system,
                            component,
                            maximizingActor,
                            rewardCache,
                            rewardBounds,
                            strategy,
                            totalRewardOne,
                            totalRewardOther,
                            componentAnalyser);
                } else {
                    // TODO Is this right? Somewhat depends on the fact that no choice -> protagonist I think
                    region = new OwnedRegion<>(
                            system,
                            component,
                            antagonistControl ? Actor.ANTAGONIST : Actor.PROTAGONIST,
                            maximizingActor,
                            rewardCache,
                            rewardBounds,
                            totalRewardOne,
                            totalRewardOther);
                }
            }
            regions.add(region);
        }

        return true;
    }

    @Override
    public Optional<?> statistics(StatisticsVerbosity verbosity) {
        if (verbosity.doMore()) {
            s.componentsStatistics = DefaultStatistics.extract(
                    Collections2.transform(regions.regions(), r -> r.component().states()));
        }
        return Optional.of(s);
    }

    private static final class MeanPayoffStatistics {
        public long reachUpdates = 0;
        public long totalRewardUpdates = 0;
        public int exploredStates = 0;
        public int componentSearches = 0;
        public int discoveredRegions = 0;
        public int inflateCount = 0;
        public int deflateCount = 0;
        public final Stopwatch componentSearchTime = Stopwatch.createUnstarted();
        public final Stopwatch totalRewardIterationTime = Stopwatch.createUnstarted();
        public final Stopwatch flateTime = Stopwatch.createUnstarted();

        @Nullable
        @SerializedName("regions")
        public ComponentsStatistics componentsStatistics;
    }
}
