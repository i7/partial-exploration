package de.tum.in.pet.sampling.implementation;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.probmodels.graph.Component;
import de.tum.in.probmodels.model.HyperEdge;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import javax.annotation.Nullable;

public final class ComponentTracker<R, H extends HyperEdge> {
    private final NatBitSet statesInComponents = NatBitSets.set();
    private final Int2ObjectMap<R> stateToComponentMap = new Int2ObjectOpenHashMap<>();
    private final Set<R> components = new HashSet<>();
    private final Function<R, ? extends Component<H>> componentFunction;

    public ComponentTracker(Function<R, ? extends Component<H>> componentFunction) {
        this.componentFunction = componentFunction;
    }

    @Nullable
    public R component(int state) {
        return stateToComponentMap.get(state);
    }

    public Set<R> regions() {
        return components;
    }

    public Set<R> removeAll(IntSet states) {
        Set<R> affected = new HashSet<>();
        states.forEach((int s) -> {
            var region = stateToComponentMap.remove(s);
            if (region != null) {
                affected.add(region);
            }
        });
        for (R affectedRegion : affected) {
            IntSet regionStates = componentFunction.apply(affectedRegion).states();
            regionStates.forEach(stateToComponentMap::remove);
            statesInComponents.andNot(regionStates);
        }
        components.removeAll(affected);

        assert !statesInComponents.intersects(states);
        assert components.equals(Set.copyOf(stateToComponentMap.values()));
        return affected;
    }

    public boolean inComponent(int state) {
        return stateToComponentMap.containsKey(state);
    }

    public List<Component<H>> filterNew(Collection<? extends Component<H>> components) {
        // TODO This assumes that regions are identified by their sets of states -- which isn't true when we remove
        //    actions
        //     -> need to make sure that we always purge / update regions when we modify states
        //     -> Should be able to update be running a search localized to the region?

        List<Component<H>> newComponents = new ArrayList<>(components.size());
        for (Component<H> component : components) {
            assert !component.states().isEmpty();
            if (!statesInComponents.containsAll(component.states())) {
                newComponents.add(component);
            }
        }
        return newComponents;
    }

    public R add(R component) {
        IntSet states = componentFunction.apply(component).states();
        statesInComponents.or(states);
        states.forEach((int s) -> {
            var oldRegion = stateToComponentMap.put(s, component);
            if (oldRegion != null) {
                components.remove(oldRegion);
            }
        });
        components.add(component);
        assert components.equals(Set.copyOf(stateToComponentMap.values()));
        return component;
    }

    public NatBitSet statesInComponents() {
        return statesInComponents;
    }
}
