package de.tum.in.pet.sampling;

public interface TrackingSEC<L, T> extends SEC<L> {
    T get();
}
