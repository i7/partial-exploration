package de.tum.in.pet.sampling;

import de.tum.in.probmodels.graph.Component;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.model.ControlledSystem;
import de.tum.in.probmodels.model.HyperEdge;
import de.tum.in.probmodels.model.Hypergraph;
import de.tum.in.probmodels.model.choice.Choice;
import it.unimi.dsi.fastutil.ints.IntPredicate;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;

public interface SEC<L> {
    static <L> List<Choice<L>> exits(ControlledSystem<Choice<L>> system, IntSet states, Actor owner) {
        Set<Choice<L>> exits = new HashSet<>();
        var iterator = states.intIterator();
        while (iterator.hasNext()) {
            int s = iterator.nextInt();
            if (system.control(s) == owner) {
                for (Choice<L> choice : system.choices(s)) {
                    choice.removeSuccessors(states).ifPresent(exits::add);
                }
            }
        }
        assert exits.stream().noneMatch(d -> d.someSuccessorIn(states));
        return new ArrayList<>(exits);
    }

    static <H extends HyperEdge> Hypergraph<H> control(
            Component<H> system,
            Actor controller,
            IntFunction<Actor> control,
            IntPredicate filter,
            IntFunction<H> strategy) {
        return state -> {
            if (!filter.test(state)) {
                return List.of();
            }

            List<H> choices = system.choices(state);
            assert !choices.isEmpty();
            if (choices.size() == 1 || control.apply(state) == controller) {
                return choices;
            }
            var witness = strategy.apply(state);
            return (witness != null && system.isStable(witness)) ? List.of(witness) : List.of();
        };
    }

    static <L> boolean mapExits(SEC<L> sec, IntUnaryOperator map, Consumer<Choice<L>> action) {
        assert sec.component().states().intStream().allMatch(s -> s == map.applyAsInt(s));
        boolean anyChanged = false;
        for (Choice<L> exit : sec.ownerExits()) {
            var mapped = exit.mapSuccessors(map);
            if (mapped.isPresent()) {
                var choice = mapped.get();
                anyChanged = anyChanged || !choice.equals(exit);
                action.accept(choice);
            }
        }
        return anyChanged;
    }

    Component<Choice<L>> component();

    Actor owner();

    List<Choice<L>> ownerExits();
}
