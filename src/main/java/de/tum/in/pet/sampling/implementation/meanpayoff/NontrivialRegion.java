package de.tum.in.pet.sampling.implementation.meanpayoff;

import de.tum.in.probmodels.graph.Component;
import de.tum.in.probmodels.model.RewardFunction;
import de.tum.in.probmodels.model.choice.Choice;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;

abstract sealed class NontrivialRegion<L> implements MeanPayoffRegion<L> permits GameRegion, OwnedRegion {
    enum UpdateMark {
        NONE,
        UPDATE,
        UPDATE_AND_FLATE;
    }

    final Component<Choice<L>> component;

    private int updateDelay = 0;
    private UpdateMark mark = UpdateMark.NONE;

    final RewardFunction rewards;
    final Int2DoubleMap totalRewardOne;
    final Int2DoubleMap totalRewardOther;
    boolean swapped = false;

    public NontrivialRegion(
            Component<Choice<L>> component,
            RewardFunction rewards,
            Int2DoubleMap totalRewardOne,
            Int2DoubleMap totalRewardOther) {
        this.component = component;
        this.rewards = rewards;
        this.totalRewardOne = totalRewardOne;
        this.totalRewardOther = totalRewardOther;
    }

    public void markStale(boolean flate) {
        if (mark == UpdateMark.NONE) {
            updateDelay = component.size();
        } else {
            updateDelay -= 1;
        }

        if (flate) {
            mark = UpdateMark.UPDATE_AND_FLATE;
        } else if (mark != UpdateMark.UPDATE_AND_FLATE) {
            mark = UpdateMark.UPDATE;
        }
    }

    public boolean needsUpdate() {
        return mark != UpdateMark.NONE && updateDelay <= 0;
    }

    public UpdateMark getAndResetMark() {
        var mark = this.mark;
        this.mark = UpdateMark.NONE;
        return mark;
    }

    public abstract int update();
}
