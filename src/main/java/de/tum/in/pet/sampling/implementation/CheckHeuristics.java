package de.tum.in.pet.sampling.implementation;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.probmodels.model.HyperSystem;

public class CheckHeuristics {
    private long updates = 0;
    private long updatesUntilComponentSearch = 0;
    private long updatesUntilComponentSearchReset = 0;

    private long exploresBeforeComponentSearch = 0;
    private long exploresBeforeCollapseReset = 100;
    private int failedSearches = 0;
    private int succeededSearches = 0;
    private final HyperSystem<?> system;

    public CheckHeuristics(HyperSystem<?> system) {
        this.system = system;
    }

    public void onStateUpdate() {
        updates += 1;
        updatesUntilComponentSearch -= 1;
    }

    public void onStateUpdates(int count) {
        updates += count;
        updatesUntilComponentSearch -= count;
    }

    public boolean shouldCheckComponents(NatBitSet modifiedStates) {
        if (updatesUntilComponentSearch > 0) {
            return false;
        }
        if (modifiedStates.size() > system.stateCount() / 10) {
            return true;
        }
        if (modifiedStates.isEmpty()) {
            return false;
        }
        if (modifiedStates.size() < exploresBeforeComponentSearch) {
            exploresBeforeComponentSearch -= 10;
            return false;
        }
        return true;
    }

    public void afterComponentCheck(boolean successful) {
        if (successful) {
            succeededSearches += 1;
            failedSearches = 0;
            updatesUntilComponentSearchReset = (long) system.stateCount() * succeededSearches;
        } else {
            succeededSearches = 0;
            failedSearches += 1;
            updatesUntilComponentSearchReset =
                    Math.max((long) system.stateCount() * failedSearches, updatesUntilComponentSearchReset);
            exploresBeforeCollapseReset = Math.max(system.stateCount(), exploresBeforeCollapseReset);
        }

        exploresBeforeComponentSearch = exploresBeforeCollapseReset;
        updatesUntilComponentSearch = updatesUntilComponentSearchReset;
    }

    public long updates() {
        return updates;
    }
}
