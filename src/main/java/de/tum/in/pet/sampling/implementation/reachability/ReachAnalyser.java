package de.tum.in.pet.sampling.implementation.reachability;

import com.google.common.base.Stopwatch;
import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.pet.sampling.SEC;
import de.tum.in.pet.sampling.SimpleSEC;
import de.tum.in.pet.sampling.implementation.CheckHeuristics;
import de.tum.in.pet.sampling.implementation.CollapsingAnalyser;
import de.tum.in.pet.sampling.implementation.ComponentTracker;
import de.tum.in.probmodels.cli.StatisticsVerbosity;
import de.tum.in.probmodels.explorer.ModelBuilder;
import de.tum.in.probmodels.explorer.SelfLoopHandling;
import de.tum.in.probmodels.generator.Generator;
import de.tum.in.probmodels.graph.Component;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.model.Attractor;
import de.tum.in.probmodels.model.choice.Choice;
import de.tum.in.probmodels.model.impl.RestrictedGraphView;
import de.tum.in.probmodels.problem.property.UntilType;
import de.tum.in.probmodels.problem.query.Optimization;
import de.tum.in.probmodels.util.FloatUtil;
import de.tum.in.probmodels.util.Sample;
import de.tum.in.probmodels.values.Bounds;
import de.tum.in.probmodels.values.Selection;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import javax.annotation.Nullable;
import org.tinylog.Logger;

@SuppressWarnings({"PMD.TooManyFields", "PMD.UnusedAssignment", "PMD.UnusedLocalVariable"})
public final class ReachAnalyser<S, L> extends CollapsingAnalyser<S, L> {
    private static final int GOAL_ID = 0;
    private static final int SINK_ID = 1;

    private final Function<S, UntilType> objective;
    private final Optimization optimization;
    private final Actor maximizingActor;

    private final Int2ObjectMap<Bounds> bounds = new Int2ObjectOpenHashMap<>();

    private final NatBitSet modifiedSinceComponentSearch = NatBitSets.set();
    private final ComponentTracker<ReachabilityRegion<L>, Choice<L>> regions =
            new ComponentTracker<>(ReachabilityRegion::component);

    private int goalState = GOAL_ID;
    private final NatBitSet oneStates = NatBitSets.set();
    private int sinkState = SINK_ID;
    private final NatBitSet zeroStates = NatBitSets.set();
    private int stateCounter = 2;
    private int regionIdCounter = 0;

    private final CheckHeuristics checkHeuristics;
    // TODO Iterations is not a good measure for progress
    private long iterations = 0;
    private final ReachabilityStatistics s = new ReachabilityStatistics();

    public ReachAnalyser(Generator<S, L> model, Function<S, UntilType> objective, Optimization optimization) {
        super(model);
        this.objective = objective;
        this.optimization = optimization;
        this.checkHeuristics = new CheckHeuristics(system);

        system.addState(GOAL_ID);
        system.addState(SINK_ID);
        system.setState(GOAL_ID, Actor.PROTAGONIST, Choice.onlySingleton(GOAL_ID));
        system.setState(SINK_ID, Actor.PROTAGONIST, Choice.onlySingleton(SINK_ID));
        oneStates.set(GOAL_ID);
        zeroStates.set(SINK_ID);
        bounds.put(GOAL_ID, Bounds.one());
        bounds.put(SINK_ID, Bounds.zero());

        maximizingActor = optimization.maximizer();

        for (S initialState : model.initialStates()) {
            int state = indexMap.addState(initialState);
            switch (objective.apply(initialState)) {
                case GOAL -> {
                    system.setState(state, Actor.PROTAGONIST, Choice.onlySingleton(goalState));
                    markOne(state);
                }
                case SINK -> {
                    system.setState(state, Actor.PROTAGONIST, Choice.onlySingleton(sinkState));
                    markZero(state);
                }
                case SAFE -> explore(state);
            }
        }
    }

    @Override
    protected int makeState(S state) {
        assert model.initialStates().contains(state) || objective.apply(state) == UntilType.SAFE;
        return stateCounter++;
    }

    @Override
    public void explore(int state) {
        assert goalState == system.representative(GOAL_ID) && sinkState == system.representative(SINK_ID);
        assert system.representative(state) == state : state;
        assert indexMap.check(state);
        assert objective.apply(indexMap.getState(state)) == UntilType.SAFE;
        s.exploredStates += 1;

        var explore = model.explore(indexMap.getState(state));
        Collection<Choice<L>> choices = ModelBuilder.explore(
                state,
                explore,
                SelfLoopHandling.INLINE_LOOPS,
                ModelBuilder.Labels.IGNORE_LABELS,
                s -> switch (objective.apply(s)) {
                    case GOAL -> goalState;
                    case SINK -> sinkState;
                    case SAFE -> system.representative(indexMap.addIfAbsent(s));
                });

        boolean allZeroTransitions = true;
        boolean allOneTransitions = !choices.isEmpty();
        for (Choice<L> choice : choices) {
            if (allZeroTransitions && !choice.isOnlySuccessor(state) && !choice.isSubsetOf(zeroStates)) {
                allZeroTransitions = false;
                if (!allOneTransitions) {
                    break;
                }
            }
            if (allOneTransitions && !choice.isSubsetOf(oneStates)) {
                allOneTransitions = false;
                if (!allZeroTransitions) {
                    break;
                }
            }
        }
        assert !allZeroTransitions || !allOneTransitions;

        if (allZeroTransitions) {
            system.setState(state, Actor.PROTAGONIST, Choice.onlySingleton(sinkState));
            markZero(state);
        } else if (allOneTransitions) {
            system.setState(state, Actor.PROTAGONIST, Choice.onlySingleton(goalState));
            markOne(state);
        } else {
            modifiedSinceComponentSearch.set(state);
            system.setState(state, explore.actor(), choices);
        }
    }

    @Override
    public Bounds bounds(int state) {
        assert checkBounds(state);
        return bounds.getOrDefault(state, Bounds.unknownReach());
    }

    @Override
    public boolean isUnknown(int state) {
        assert checkBounds(state);
        var bounds = this.bounds.get(state);
        return bounds == null || FloatUtil.isOne(bounds.difference());
    }

    private void markOne(int state) {
        assert FloatUtil.isOne(bounds(state).upperBound());
        oneStates.set(state);
        modifiedSinceComponentSearch.clear(state);
        bounds.put(state, Bounds.one());
    }

    private void markZero(int state) {
        assert FloatUtil.isZero(bounds(state).lowerBound());
        zeroStates.set(state);
        modifiedSinceComponentSearch.clear(state);
        bounds.put(state, Bounds.zero());
        assert checkBounds(state);
    }

    private void markZero(IntSet states) {
        assert states.intStream()
                .mapToObj(this::bounds)
                .mapToDouble(Bounds::lowerBound)
                .allMatch(FloatUtil::isZero);
        zeroStates.or(states);
        modifiedSinceComponentSearch.andNot(states);
        states.forEach((int s) -> bounds.put(s, Bounds.zero()));
        assert states.intStream().allMatch(this::checkBounds);
    }

    private boolean checkBounds(int state) {
        assert zeroStates.contains(state)
                        == (bounds.getOrDefault(state, Bounds.unknown()).upperBound() == 0.0)
                : bounds.get(state);
        assert oneStates.contains(state)
                        == (bounds.getOrDefault(state, Bounds.unknown()).lowerBound() == 1.0)
                : bounds.get(state);
        return true;
    }

    @Override
    public Bounds update(int state) {
        assert isExplored(state);
        checkHeuristics.onStateUpdate();
        s.reachUpdates += 1;

        Bounds currentBounds = bounds.get(state);
        if (currentBounds != null && FloatUtil.isZero(currentBounds.difference())) {
            return currentBounds;
        }

        var choices = system.choices(state);
        assert !choices.isEmpty();
        int size = choices.size();
        assert optimization != Optimization.UNIQUE_VALUE || size == 1;
        assert !FloatUtil.isOne(lowerBound(state)) && !FloatUtil.isZero(upperBound(state));

        // TODO Early stopping
        // TODO Delete any action that has has a provably worse upper bound than the best lower bound
        //   -> Likely can save a lot since this shows in every sampling, MEC search, ...
        // TODO ... first figure out whether this actually happens in practice

        Bounds newBounds;
        if (size == 1) {
            newBounds = choices.get(0)
                    .sumWeightedExceptJacobiBounds(this::bounds, state)
                    .orElse(Bounds.zero());
        } else {
            Actor stateControl = system.control(state);
            Function<Choice<L>, Bounds> bounds =
                    c -> c.sumWeightedExceptJacobiBounds(this::bounds, state).orElse(Bounds.zero());
            Selection selection = optimization.selection(stateControl);
            if (stateControl == maximizingActor) {
                newBounds = selection.select(choices, bounds);
            } else {
                var region = regions.component(state);
                if (region == null) {
                    newBounds = selection.select(choices, bounds);
                } else {
                    Selection.Select<Choice<L>> select = selection.selectWithWitness(choices, bounds);
                    Choice<L> witness = select.witness();
                    // TODO It should be enough to compare / store the support of the witness
                    if (!witness.equals(region.strategy.put(state, witness))) {
                        markStale(region);
                    }
                    newBounds = select.optimum();
                }
            }
        }
        if (newBounds.equalsUpTo(Bounds.one())) {
            markOne(state);
            return Bounds.one();
        }
        if (newBounds.equalsUpTo(Bounds.zero())) {
            markZero(state);
            return Bounds.zero();
        }

        Bounds oldBounds = this.bounds.put(state, newBounds);
        assert oldBounds == null || oldBounds.contains(newBounds, FloatUtil.WEAK_EPS) : oldBounds + " " + newBounds;
        assert checkBounds(state);

        var region = regions.component(state);
        if (region != null) {
            SEC<L> sec = region.stateToSEC.get(state);
            if (sec != null) {
                // TODO Maybe rather mark stale?
                deflate(sec);
            }
        }
        return newBounds;
    }

    @Override
    public void afterIteration(int state) {
        iterations += 1;

        // TODO Also perform a "prob1A" / "prob0E" analysis heuristically? Can start with those states that have trivial
        // bounds

        if (checkHeuristics.shouldCheckComponents(modifiedSinceComponentSearch)) {
            checkHeuristics.afterComponentCheck(discoverComponents());
        } else {
            int threshold = system.stateCount() / 10;
            if (zeroStates.size() > threshold) {
                merge(zeroStates, Actor.PROTAGONIST);
            }
            if (oneStates.size() > threshold) {
                merge(oneStates, Actor.PROTAGONIST);
            }
        }

        for (ReachabilityRegion<L> region : regions.regions()) {
            if (region.isStale() && region.strategyStale <= iterations) {
                region.strategyStale = 0;
                updateSECs(region);
            }
        }
    }

    private void markStale(ReachabilityRegion<L> region) {
        if (!region.isStale()) {
            region.strategyStale = iterations + 100;
        }
    }

    private void deflate(SEC<L> sec) {
        s.deflateTime.start();
        s.deflateCount += 1;
        try {
            double bestExit = 0.0;
            for (Choice<L> choice : sec.ownerExits()) {
                double exit = choice.sumWeighted(this::upperBound);
                if (exit > bestExit) {
                    if (FloatUtil.isOne(exit)) {
                        return;
                    }
                    bestExit = exit;
                }
            }

            if (FloatUtil.isZero(bestExit)) {
                markZero(sec.component().states());
                return;
            }

            IntIterator secStateIterator = sec.component().states().intIterator();
            while (secStateIterator.hasNext()) {
                int s = secStateIterator.nextInt();
                var bounds = this.bounds.get(s);
                if (bounds.upperBound() > bestExit) {
                    this.bounds.put(s, bounds.withUpper(bestExit));
                }
                assert checkBounds(s);
            }
        } finally {
            s.deflateTime.stop();
        }
    }

    private void updateSECs(ReachabilityRegion<L> region) {
        assert region.component.stateStream().allMatch(system::isDefinedState);

        region.stateToSEC.clear();
        region.secs.clear();

        s.secUpdateTime.start();
        var component = region.component;
        var componentView =
                SEC.control(component, maximizingActor, system::control, s -> !isSolved(s), region.strategy::get);
        var components = componentAnalyser.findComponents(componentView, component.states());
        for (var secComponent : components) {
            SimpleSEC<L> sec = SimpleSEC.of(system, secComponent, maximizingActor);
            if (sec.ownerExits().isEmpty()) {
                Logger.debug("Found SEC without exits, marking as zero");
                markZero(sec.component().states());
            } else {
                region.secs.add(sec);
                secComponent.states().forEach((int s) -> region.stateToSEC.put(s, sec));
            }
        }
        s.secUpdateTime.stop();
    }

    @Nullable
    @Override
    public Choice<L> selectOptimal(int state) {
        var choices = system.choices(state);
        assert !choices.isEmpty();
        if (choices.size() == 1) {
            Choice<L> distribution = choices.get(0);
            return distribution.isOnlySuccessor(state) ? null : distribution;
        }
        ToDoubleFunction<Choice<L>> scoreFunction = system.control(state) == maximizingActor
                ? c -> c.sumWeightedExceptJacobi(this::upperBound, state)
                : c -> -c.sumWeightedExceptJacobi(this::lowerBound, state);
        // TODO This would be the optimal place to track suboptimal actions
        return Sample.getOptimal(choices, scoreFunction);
    }

    private int merge(IntSet states, Actor control) {
        assert Collections.disjoint(states, oneStates) || Collections.disjoint(states, zeroStates);
        assert system.checkConsistency();
        assert !states.isEmpty();

        IntSet mergedStates;
        if (NatBitSets.intersects(oneStates, states)) {
            oneStates.or(states);
            int before = oneStates.size();
            Attractor.addAttractor(oneStates, system, system::predecessors, maximizingActor);
            assert oneStates
                    .intStream()
                    .mapToDouble(
                            one -> bounds.getOrDefault(one, Bounds.unknown()).upperBound())
                    .allMatch(u -> FloatUtil.isEqual(u, 1.0, FloatUtil.WEAK_EPS));
            assert oneStates.contains(goalState);
            if (oneStates.size() == 1) {
                return goalState;
            }
            int added = oneStates.size() - before;
            Logger.debug("Merging {} one states ({} added by attractor)", oneStates.size(), added);
            mergedStates = oneStates;
            s.statesSolvedByAttractor += added;
            s.statesMergedToOne += mergedStates.size() - 1;
        } else if (NatBitSets.intersects(zeroStates, states)) {
            zeroStates.or(states);
            int before = zeroStates.size();
            Attractor.addAttractor(zeroStates, system, system::predecessors, maximizingActor.opponent());
            assert zeroStates
                    .intStream()
                    .mapToDouble(
                            zero -> bounds.getOrDefault(zero, Bounds.unknown()).lowerBound())
                    .allMatch(u -> FloatUtil.isEqual(u, 0.0, FloatUtil.WEAK_EPS));
            assert zeroStates.contains(sinkState);
            if (zeroStates.size() == 1) {
                return sinkState;
            }
            int added = zeroStates.size() - before;
            Logger.debug("Merging {} zero states ({} added by attractor)", zeroStates.size(), added);
            mergedStates = zeroStates;
            s.statesZeroedByAttractor += added;
            s.statesMergedToZero += mergedStates.size();
        } else {
            Logger.debug("Merging {} equivalent states", states.size());
            mergedStates = states;
            s.collapseCount += 1;
            s.collapsedStates += mergedStates.size();
        }

        assert oneStates.contains(goalState) && zeroStates.contains(sinkState);

        // TODO Better - we can actually rebuild the regions here; need a component.remap() where we pass a
        //      IntUnaryOperator -- However: Need to delete the region if we make a state of it absorbing /
        //      remove an action
        Set<ReachabilityRegion<L>> preRegions = new HashSet<>();
        Set<ReachabilityRegion<L>> affectedRegions = regions.removeAll(mergedStates);
        mergedStates.forEach((int s) -> {
            NatBitSet predecessors = system.predecessors(s);
            predecessors.forEach(p -> {
                var preRegion = regions.component(p);
                if (preRegion != null && !affectedRegions.contains(preRegion)) {
                    preRegions.add(preRegion);
                }
            });
            // TODO Do we need that? Collapsing cannot introduce new components?
            modifiedSinceComponentSearch.or(predecessors);
        });
        for (ReachabilityRegion<L> preRegion : preRegions) {
            preRegion.secs.forEach(sec -> sec.mapExits(system::representative));
        }

        int representative;
        if (mergedStates == zeroStates || mergedStates == oneStates) { // NOPMD
            representative = system.makeAbsorbing(mergedStates);
        } else {
            assert !mergedStates.contains(sinkState) && !mergedStates.contains(goalState);
            representative = system.collapse(mergedStates, control, SelfLoopHandling.INLINE_LOOPS);
        }
        assert mergedStates.contains(representative);

        // TODO Compute bound depending on control?
        bounds.keySet().removeAll(mergedStates);

        modifiedSinceComponentSearch.andNot(mergedStates);
        assert !modifiedSinceComponentSearch.contains(goalState);
        assert !modifiedSinceComponentSearch.contains(sinkState);

        if (mergedStates.contains(goalState)) {
            goalState = representative;
            oneStates.clear();
            oneStates.set(goalState);
            bounds.put(goalState, Bounds.one());
            assert system.successors(goalState).equals(IntSet.of(goalState));
        } else if (mergedStates.contains(sinkState)) {
            sinkState = representative;
            zeroStates.clear();
            zeroStates.set(sinkState);
            bounds.put(sinkState, Bounds.zero());
            assert system.successors(sinkState).equals(IntSet.of(sinkState));
        } else {
            assert representative != goalState && representative != sinkState;
            modifiedSinceComponentSearch.set(representative);
        }
        assert checkBounds(representative);
        return representative;
    }

    private boolean discoverComponents() {
        assert system.areDefinedStates(modifiedSinceComponentSearch);
        merge(oneStates, Actor.PROTAGONIST);
        merge(zeroStates, Actor.PROTAGONIST);
        assert !modifiedSinceComponentSearch.contains(goalState);
        assert !modifiedSinceComponentSearch.contains(sinkState);

        s.componentSearches += 1;
        Logger.debug(
                "Searching for components in overall system with {} states, starting from {} states",
                system.definedStates().size(),
                modifiedSinceComponentSearch.size());
        NatBitSet stateRestriction = NatBitSets.modifiableCopyOf(system.definedStates());
        stateRestriction.clear(goalState);
        stateRestriction.clear(sinkState);

        List<? extends IntSet> maximizerComponents;
        List<? extends IntSet> minimizerComponents;
        s.componentSearchTime.start();
        boolean protagonistMaximizes = maximizingActor == Actor.PROTAGONIST;
        NatBitSet maximizerStates =
                protagonistMaximizes ? system.protagonistControlledStates() : system.antagonistControlledStates();
        NatBitSet minimizerStates =
                protagonistMaximizes ? system.antagonistControlledStates() : system.protagonistControlledStates();
        boolean minimizerEmpty = !minimizerStates.intersects(modifiedSinceComponentSearch);
        boolean maximizerEmpty = !maximizerStates.intersects(modifiedSinceComponentSearch);
        if (minimizerEmpty) {
            maximizerComponents = componentAnalyser.findComponentStates(
                    RestrictedGraphView.of(system, stateRestriction), modifiedSinceComponentSearch);
            minimizerComponents = List.of();
        } else if (maximizerEmpty) {
            maximizerComponents = List.of();
            minimizerComponents = componentAnalyser.findComponentStates(
                    RestrictedGraphView.of(system, stateRestriction), modifiedSinceComponentSearch);
        } else {
            stateRestriction.andNot(minimizerStates);
            var maximizerInitial = NatBitSets.copyOf(modifiedSinceComponentSearch);
            maximizerInitial.andNot(minimizerStates);
            maximizerComponents = componentAnalyser.findComponentStates(
                    RestrictedGraphView.of(system, stateRestriction), maximizerInitial);
            stateRestriction.or(minimizerStates);

            stateRestriction.andNot(maximizerStates);
            var minimizerInitial = NatBitSets.copyOf(modifiedSinceComponentSearch);
            minimizerInitial.andNot(maximizerStates);
            minimizerComponents = componentAnalyser.findComponentStates(
                    RestrictedGraphView.of(system, stateRestriction), minimizerInitial);
            stateRestriction.or(maximizerStates);
        }
        s.componentSearchTime.stop();
        s.discoveredControlledComponents += maximizerComponents.size() + minimizerComponents.size();
        boolean anyComponents = !maximizerComponents.isEmpty() || !minimizerComponents.isEmpty();

        if (anyComponents) {
            Logger.debug(
                    "Found {} maximizer and {} minimizer components",
                    maximizerComponents.size(),
                    minimizerComponents.size());
            for (IntSet maxComponent : maximizerComponents) {
                update(merge(maxComponent, maximizingActor));
            }
            for (IntSet minComponent : minimizerComponents) {
                regions.removeAll(minComponent);
                markZero(minComponent);
            }
        }
        if (minimizerEmpty || maximizerEmpty) {
            modifiedSinceComponentSearch.clear();
            return anyComponents;
        }

        // Search for regions
        Logger.debug("Searching for game regions");
        s.componentSearchTime.start();
        NatBitSet gameRestriction;
        if (anyComponents) {
            gameRestriction = NatBitSets.modifiableCopyOf(system.definedStates());
            gameRestriction.clear(goalState);
            gameRestriction.clear(sinkState);
        } else {
            gameRestriction = stateRestriction;
        }

        var foundComponents = componentAnalyser.findComponents(
                RestrictedGraphView.of(system, gameRestriction), modifiedSinceComponentSearch);
        modifiedSinceComponentSearch.clear();
        List<Component<Choice<L>>> newRegions = regions.filterNew(foundComponents);
        s.componentSearchTime.stop();
        if (newRegions.isEmpty()) {
            return anyComponents;
        }
        Logger.debug("Found {} new regions", newRegions.size());
        s.discoveredRegions += 1;

        for (Component<Choice<L>> component : newRegions) {
            updateSECs(regions.add(new ReachabilityRegion<>(regionIdCounter, component)));
            regionIdCounter += 1;
        }
        return true;
    }

    @Override
    public Optional<?> statistics(StatisticsVerbosity verbosity) {
        return Optional.of(s);
    }

    @Override
    public String progressString() {
        return "Explored %d states%n".formatted(s.exploredStates)
                + "Searched components %d times, found %d components and %d regions, collapsed %d states%n"
                        .formatted(
                                s.componentSearches,
                                s.discoveredControlledComponents,
                                s.discoveredRegions,
                                s.collapsedStates)
                + "Collapsed %d/%d states to zero/one (%d/%d found by attractor)"
                        .formatted(
                                s.statesMergedToZero,
                                s.statesMergedToOne,
                                s.statesZeroedByAttractor,
                                s.statesSolvedByAttractor);
    }

    private static final class ReachabilityStatistics {
        public int exploredStates = 0;
        public long reachUpdates = 0;
        public int componentSearches = 0;
        public int discoveredRegions = 0;
        public int discoveredControlledComponents = 0;
        public int statesMergedToZero = 0;
        public int statesZeroedByAttractor = 0;
        public int statesMergedToOne = 0;
        public int statesSolvedByAttractor = 0;
        public int collapsedStates = 0;
        public int collapseCount = 0;
        public int deflateCount = 0;
        public final Stopwatch componentSearchTime = Stopwatch.createUnstarted();
        public final Stopwatch secUpdateTime = Stopwatch.createUnstarted();
        public final Stopwatch deflateTime = Stopwatch.createUnstarted();
    }
}
