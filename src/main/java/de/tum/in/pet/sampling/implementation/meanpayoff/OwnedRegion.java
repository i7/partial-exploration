package de.tum.in.pet.sampling.implementation.meanpayoff;

import de.tum.in.pet.sampling.SEC;
import de.tum.in.probmodels.graph.Component;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.model.ControlledSystem;
import de.tum.in.probmodels.model.RewardFunction;
import de.tum.in.probmodels.model.choice.Choice;
import de.tum.in.probmodels.util.FloatUtil;
import de.tum.in.probmodels.values.Bounds;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.IntIterator;
import java.util.List;

@SuppressWarnings("PMD.TooManyFields")
final class OwnedRegion<L> extends NontrivialRegion<L> implements MeanPayoffRegion<L>, SEC<L> {
    // TODO Adaptively choose
    private static final double alpha = 0.9;

    private final Actor owner;
    private final boolean maximize;

    private Bounds bounds;
    private final int iterationBound;
    private boolean boundsConvergedPastInitial = false;
    private final List<Choice<L>> exits;
    private double targetPrecision;

    OwnedRegion(
            ControlledSystem<Choice<L>> system,
            Component<Choice<L>> component,
            Actor owner,
            Actor maximizer,
            RewardFunction rewards,
            Bounds rewardBounds,
            Int2DoubleMap rewardOne,
            Int2DoubleMap rewardOther) {
        super(component, rewards, rewardOne, rewardOther);

        // TODO Figure out an adaptive heuristic for the bound
        iterationBound = (component.size() / 2 + 1);
        this.bounds = rewardBounds;
        this.owner = owner;
        this.maximize = owner == maximizer;
        exits = SEC.exits(system, component.states(), owner);
    }

    private Bounds iterate(Component<Choice<L>> component, Int2DoubleMap currentValues, Int2DoubleMap nextValues) {
        IntIterator stateIterator = component.states().iterator();
        double minimalDifference = Double.POSITIVE_INFINITY;
        double maximalDifference = Double.NEGATIVE_INFINITY;

        while (stateIterator.hasNext()) {
            int state = stateIterator.nextInt();
            double currentValue = currentValues.get(state);
            double stateValue = rewards.stateReward(state);

            double optimum = maximize ? Double.NEGATIVE_INFINITY : Double.POSITIVE_INFINITY;
            List<Choice<L>> choices = component.choices(state);
            assert !choices.isEmpty();
            for (Choice<L> choice : choices) {
                double reward = rewards.choiceReward(state, choice.label()) / alpha;
                double value = choice.sumWeighted(currentValues) + reward;
                if (maximize) {
                    if (value > optimum) {
                        optimum = value;
                    }
                } else {
                    if (value < optimum) {
                        optimum = value;
                    }
                }
            }

            FloatUtil.KahanSum valueSum = new FloatUtil.KahanSum(stateValue);
            valueSum.add(alpha * optimum);
            nextValues.put(state, valueSum.add((1 - alpha) * currentValue));
            double difference = valueSum.add(-currentValue);
            if (difference < minimalDifference) {
                minimalDifference = difference;
            }
            if (difference > maximalDifference) {
                maximalDifference = difference;
            }
        }
        return Bounds.of(minimalDifference, maximalDifference);
    }

    private Iteration iterateTotalReward(int iterationBound) {
        Int2DoubleMap currentValues = swapped ? totalRewardOne : totalRewardOther;
        Int2DoubleMap nextValues = swapped ? totalRewardOther : totalRewardOne;

        int steps = 0;
        Bounds currentBounds = this.bounds;
        //noinspection ObjectEquality
        while (steps < iterationBound && currentBounds.difference() >= targetPrecision) {
            Bounds nextBounds = iterate(component, currentValues, nextValues);
            Int2DoubleMap swap = nextValues;
            nextValues = currentValues;
            currentValues = swap;

            if (boundsConvergedPastInitial) {
                assert currentBounds.contains(nextBounds, FloatUtil.WEAK_EPS)
                        : "%s does not contain %s".formatted(currentBounds, nextBounds);
                currentBounds = nextBounds;
            } else {
                if (currentBounds.contains(nextBounds)) {
                    currentBounds = nextBounds;
                    boundsConvergedPastInitial = true;
                } else {
                    currentBounds = nextBounds.shrink(currentBounds);
                }
            }
            steps += 1;
        }
        swapped = currentValues != totalRewardOne; // NOPMD
        return new Iteration(currentBounds, steps);
    }

    @Override
    public Component<Choice<L>> component() {
        return component;
    }

    @Override
    public Actor owner() {
        return owner;
    }

    @Override
    public List<Choice<L>> ownerExits() {
        return exits;
    }

    @Override
    public int update() {
        if (FloatUtil.isZero(bounds.difference())) {
            return 0;
        }
        Iteration iteration = iterateTotalReward(this.iterationBound);
        Bounds resultBounds = iteration.result();
        assert this.bounds.contains(resultBounds, FloatUtil.WEAK_EPS);
        this.bounds = resultBounds;
        /*if (iteration.iterations >= iterationBound) {
            iterationBound += component.size();
        }*/
        if (bounds.difference() < targetPrecision) {
            targetPrecision /= 2;
        }
        return (iteration.iterations + 1) * component.size();
    }

    public Bounds staying() {
        return bounds;
    }

    private record Iteration(Bounds result, int iterations) {}

    @Override
    public String toString() {
        return "NT[%s / %s]".formatted(bounds, component);
    }
}
