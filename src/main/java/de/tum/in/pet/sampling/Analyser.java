package de.tum.in.pet.sampling;

import de.tum.in.probmodels.cli.StatisticsVerbosity;
import java.util.Collection;
import java.util.Optional;
import javax.annotation.Nullable;

public interface Analyser<S, L> extends SelectionGuidance<L> {
    int state(S state);

    Collection<S> initialStates();

    @Nullable
    default String progressString() {
        return null;
    }

    Optional<?> statistics(StatisticsVerbosity verbosity);
}
