package de.tum.in.pet.sampling;

import com.google.gson.annotations.SerializedName;
import java.util.Optional;

public record SamplingStatistics(
        @SerializedName("selection") Optional<?> selectionStatistics,
        @SerializedName("analyser") Optional<?> analyserStatistics,
        @SerializedName("selections") long selections) {}
