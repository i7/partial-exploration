package de.tum.in.pet.sampling;

public record SamplingSelectionStatistics(long loopCount, long backtrackCount, long backtrackToInitialCount) {}
