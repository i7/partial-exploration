package de.tum.in.pet.sampling;

import de.tum.in.probmodels.model.choice.Choice;
import de.tum.in.probmodels.util.FloatUtil;
import de.tum.in.probmodels.values.Bounds;
import it.unimi.dsi.fastutil.ints.IntSet;
import javax.annotation.Nullable;

public interface SelectionGuidance<L> {
    boolean isExplored(int state);

    void explore(int state);

    IntSet exploredStates();

    /**
     * The current bounds for the given state.
     */
    Bounds bounds(int state);

    boolean isUnknown(int state);

    default boolean isSolved(int state) {
        return FloatUtil.isZero(difference(state));
    }

    /**
     * The currently stored lower bound for the given state.
     */
    default double lowerBound(int state) {
        return bounds(state).lowerBound();
    }

    /**
     * The currently stored upper bound for the given state.
     */
    default double upperBound(int state) {
        return bounds(state).upperBound();
    }

    /**
     * The difference between the currently stored upper and lower bound for the given state.
     */
    default double difference(int state) {
        return bounds(state).difference();
    }

    Bounds update(int state);

    void afterIteration(int state);

    @Nullable
    Choice<L> selectOptimal(int state);
}
