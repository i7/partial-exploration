package de.tum.in.pet.sampling.implementation.core;

import static de.tum.in.probmodels.explorer.SelfLoopHandling.INLINE_LOOPS;
import static de.tum.in.probmodels.model.Actor.PROTAGONIST;

import com.google.common.base.Stopwatch;
import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.pet.sampling.implementation.CheckHeuristics;
import de.tum.in.pet.sampling.implementation.CollapsingAnalyser;
import de.tum.in.probmodels.cli.StatisticsVerbosity;
import de.tum.in.probmodels.explorer.ModelBuilder;
import de.tum.in.probmodels.generator.Generator;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.model.Attractor;
import de.tum.in.probmodels.model.choice.Choice;
import de.tum.in.probmodels.model.impl.RestrictedGraphView;
import de.tum.in.probmodels.util.FloatUtil;
import de.tum.in.probmodels.util.Sample;
import de.tum.in.probmodels.values.Bounds;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import javax.annotation.Nullable;
import org.tinylog.Logger;

@SuppressWarnings({"PMD.TooManyFields", "PMD.UnusedAssignment", "PMD.UnusedLocalVariable"})
public final class CoreBuilder<S, L> extends CollapsingAnalyser<S, L> {
    private static final int SINK_ID = 0;

    private int stateCounter = 1;
    private int sinkState = SINK_ID;
    private final NatBitSet modifiedSinceComponentSearch = NatBitSets.set();
    private final Int2DoubleMap bounds = new Int2DoubleOpenHashMap();
    private final NatBitSet zeroStates = NatBitSets.set();
    private final CheckHeuristics checkHeuristics;
    private final CoreAnalysisStatistics s = new CoreAnalysisStatistics();
    private boolean fullyExplored = false;

    public CoreBuilder(Generator<S, L> model) {
        super(model);
        this.checkHeuristics = new CheckHeuristics(system);
        system.addState(SINK_ID);
        system.setState(SINK_ID, PROTAGONIST, Choice.onlySingleton(SINK_ID));
        zeroStates.add(SINK_ID);
        bounds.put(SINK_ID, 0.0);
        bounds.defaultReturnValue(Double.NaN);

        for (S initialState : model.initialStates()) {
            explore(indexMap.addState(initialState));
        }
    }

    @Override
    protected int makeState(S state) {
        return stateCounter++;
    }

    @Override
    public void explore(int state) {
        assert system.representative(state) == state : state;
        assert indexMap.check(state);
        assert !system.isDefinedState(state);
        assert !fullyExplored;
        s.exploredStates += 1;

        var explore = model.explore(indexMap.getState(state));
        List<Choice<L>> choices =
                ModelBuilder.explore(state, explore, INLINE_LOOPS, ModelBuilder.Labels.IGNORE_LABELS, s -> {
                    int index = system.representative(indexMap.addIfAbsent(s));
                    return upperBound(index) == 0.0 ? sinkState : index;
                });

        if (choices.isEmpty() || choices.get(0).isOnlySuccessor(state)) {
            system.setState(state, PROTAGONIST, Choice.onlySingleton(sinkState));
            markZero(state);
        } else {
            modifiedSinceComponentSearch.set(state);
            system.setState(state, PROTAGONIST, choices);
        }

        if (s.exploredStates == stateCounter - 1) {
            assert system.definedStates().equals(system.states());
            fullyExplored = true;
        }

        assert system.isDefinedState(state);
    }

    @Override
    public Bounds bounds(int state) {
        assert zeroStates.contains(state) == (bounds.get(state) == 0.0) : bounds.get(state);
        // zeroStates.contains(state) ? Bounds.zero() :
        return fullyExplored ? Bounds.zero() : Bounds.of(0.0, bounds.getOrDefault(state, 1.0));
    }

    @Override
    public boolean isUnknown(int state) {
        assert zeroStates.contains(state) == (bounds.get(state) == 0.0) : bounds.get(state);
        // !zeroStates.contains(state) &&
        return !fullyExplored && FloatUtil.isOne(bounds.getOrDefault(state, 1.0));
    }

    @Override
    public double upperBound(int state) {
        assert zeroStates.contains(state) == (bounds.get(state) == 0.0) : bounds.get(state);
        // return zeroStates.contains(state) ? 0.0 : bounds.getOrDefault(state, 1.0);
        return fullyExplored ? 0.0 : bounds.getOrDefault(state, 1.0);
    }

    private void markZero(int state) {
        zeroStates.set(state);
        modifiedSinceComponentSearch.clear(state);
        bounds.put(state, 0.0);
    }

    @Nullable
    @Override
    public Choice<L> selectOptimal(int state) {
        return Sample.getOptimal(system.choices(state), c -> c.sumWeighted(this::upperBound));
    }

    @Override
    public Bounds update(int state) {
        assert isExplored(state);
        checkHeuristics.onStateUpdate();
        s.updates += 1;

        if (fullyExplored || bounds.get(state) == 0.0) { // zeroStates.contains(state)
            return Bounds.zero();
        }
        assert !FloatUtil.isZero(upperBound(state));

        var choices = system.choices(state);
        assert !choices.isEmpty();

        double newBound = 0.0d;
        for (var choice : choices) {
            if (choice.isOnlySuccessor(state)) {
                continue;
            }
            assert !choice.hasSuccessor(state);
            double value = choice.sumWeighted(this::upperBound);
            assert !Double.isNaN(value);
            if (newBound < value) {
                if (FloatUtil.isOne(value)) {
                    return Bounds.unknownReach();
                }
                newBound = value;
            }
        }
        if (FloatUtil.isZero(newBound)) {
            markZero(state);
            return Bounds.zero();
        }
        double oldBounds = this.bounds.put(state, newBound);
        assert Double.isNaN(oldBounds) || FloatUtil.lessOrEqual(newBound, oldBounds);
        return Bounds.of(0.0, newBound);
    }

    public Set<S> coreStates() {
        Set<S> states = indexMap.getStates();
        if (fullyExplored) {
            return states;
        }

        Set<S> coreStates = new HashSet<>();
        for (S state : states) {
            int stateId = system.representative(indexMap.indexOf(state));
            if (system.isDefinedState(stateId) && !isUnknown(stateId)) {
                coreStates.add(state);
            }
        }
        return coreStates;
    }

    @Override
    public void afterIteration(int state) {
        if (fullyExplored) {
            return;
        }
        if (checkHeuristics.shouldCheckComponents(modifiedSinceComponentSearch)) {
            // Prune away known zero states
            collapseZeroStates();
            boolean successful = discoverComponents();
            checkHeuristics.afterComponentCheck(successful);
            // Propagate information about collapsed states
            if (successful && zeroStates.size() > 1) {
                collapseZeroStates();
            }
        } else if (zeroStates.size() > system.definedStates().size() / 5) {
            // TODO Find a better heuristic
            collapseZeroStates();
        }
    }

    private void collapseZeroStates() {
        // Merge all zero states
        s.zeroStateAttempts += 1;
        int zeroStatesBefore = zeroStates.size();
        Attractor.addAttractor(zeroStates, system, system::predecessors, Actor.ANTAGONIST);
        int zeroStatesAfter = zeroStates.size();
        if (zeroStatesAfter > 1) {
            int addedByAttractor = zeroStatesAfter - zeroStatesBefore;
            s.statesZeroedByAttractor += addedByAttractor;
            s.statesMergedToZero += zeroStatesAfter - 1;
            Logger.debug("Merging {} zero states ({} added by attractor)", zeroStatesAfter, addedByAttractor);

            int representative = system.makeAbsorbing(zeroStates);
            modifiedSinceComponentSearch.andNot(zeroStates);

            sinkState = representative;

            zeroStates.clear();
            zeroStates.set(sinkState);
            bounds.keySet().removeAll(zeroStates);
            bounds.put(sinkState, 0.0);

            assert system.successors(sinkState).equals(IntSet.of(sinkState));
        }
        assert !modifiedSinceComponentSearch.contains(sinkState);
        assert zeroStates.size() == 1;
    }

    private boolean discoverComponents() {
        assert system.areDefinedStates(modifiedSinceComponentSearch);
        // Now search for components to merge
        Logger.debug(
                "Searching for components in overall system with {} states, starting from {} states",
                system.definedStates().size(),
                modifiedSinceComponentSearch.size());
        s.componentSearches += 1;
        s.componentSearchTime.start();
        var components = componentAnalyser.findComponentStates(
                RestrictedGraphView.of(system, system.definedStates()), modifiedSinceComponentSearch);
        s.componentSearchTime.stop();
        modifiedSinceComponentSearch.clear();

        components.removeIf(c -> c.contains(sinkState));
        if (components.isEmpty()) {
            Logger.debug("Found no new components");
            return false;
        }

        s.successfulComponentSearches += 1;
        s.discoveredComponents += components.size();
        Logger.debug("Found {} new components", components.size());
        system.collapse(components, Function.identity(), c -> PROTAGONIST, INLINE_LOOPS);

        for (IntSet component : components) {
            int representative = system.representative(component.iterator().nextInt());

            if (system.allSuccessorsIn(representative, NatBitSets.singleton(representative))
                    || NatBitSets.intersects(component, zeroStates)) {
                bounds.keySet().removeAll(component);
                zeroStates.andNot(component);
                markZero(representative);
            } else {
                // Notes:
                // 1) All states in a component have the same value hence we can actually update them to the minimal
                // upper bound instead of maximum!
                // 2) No need to zeroStates.andNot since we check for intersection above
                // 3) All states are removed from bounds in the loop
                double minimum = 1.0;
                var iterator = component.intIterator();
                while (iterator.hasNext()) {
                    int state = iterator.nextInt();
                    double bound = bounds.remove(state);
                    if (!Double.isNaN(bound) && bound < minimum) {
                        minimum = bound;
                    }
                    if (FloatUtil.isZero(minimum)) {
                        break;
                    }
                }

                // TODO Check that this holds and draw necessary conclusions
                assert component.size() == 1 || minimum == 1.0;

                if (FloatUtil.isZero(minimum)) {
                    markZero(representative);
                } else {
                    bounds.put(representative, minimum);
                }
                assert bounds.containsKey(representative);
            }
        }
        return true;
    }

    @Override
    public Optional<?> statistics(StatisticsVerbosity verbosity) {
        return Optional.of(s);
    }

    @Override
    public String progressString() {
        return "Explored %d states%n".formatted(s.exploredStates)
                + "Searched components %d times (%d successful), found %d%n"
                        .formatted(s.componentSearches, s.successfulComponentSearches, s.discoveredComponents)
                + "Collapsed %d states to zero in %d tries (%d found by attractor)"
                        .formatted(s.statesMergedToZero, s.zeroStateAttempts, s.statesZeroedByAttractor);
    }

    public static class CoreAnalysisStatistics {
        public int exploredStates = 0;
        public long updates = 0;
        public int componentSearches = 0;
        public int successfulComponentSearches = 0;
        public int discoveredComponents = 0;
        public int zeroStateAttempts = 0;
        public int statesMergedToZero = 0;
        public int statesZeroedByAttractor = 0;
        public Stopwatch componentSearchTime = Stopwatch.createUnstarted();
    }
}
