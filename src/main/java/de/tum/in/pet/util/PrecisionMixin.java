package de.tum.in.pet.util;

import static picocli.CommandLine.Option;

import de.tum.in.pet.Main;
import de.tum.in.probmodels.problem.query.Comparison;
import de.tum.in.probmodels.problem.query.OptimizingQuery;
import de.tum.in.probmodels.problem.query.QualitativeQuery;
import de.tum.in.probmodels.problem.query.Query;
import de.tum.in.probmodels.problem.verdict.BoundHandler;
import de.tum.in.probmodels.problem.verdict.QualitativeVerdict;
import de.tum.in.probmodels.problem.verdict.QuantitativeVerdict;
import de.tum.in.probmodels.util.Precision;
import org.tinylog.Logger;

@SuppressWarnings("PMD.ImmutableField")
public class PrecisionMixin {
    @Option(
            names = "--precision",
            description =
                    "Precision (default: ${DEFAULT-VALUE}). This means that the (absolute / relative) difference of the result to the correct value is at most this much.")
    private double precision = Main.DEFAULT_PRECISION;

    @Option(names = "--relative", description = "Relative Error (default: ${DEFAULT-VALUE})", negatable = true)
    private boolean relativeError = false;

    @Option(names = "--force-exact-qualitative", hidden = true)
    private boolean forceExactSampling = false;

    public static BoundHandler<?> defaultBoundHandler(Query query, PrecisionMixin precisionOption) {
        var precision = precisionOption.parse();

        if (query instanceof QualitativeQuery qualitative) {
            if (precisionOption.relativeError) {
                // TODO Does it?
                Logger.warn("Relative error has no effect for qualitative queries!");
            }

            Comparison comparison = qualitative.comparison();
            double threshold;
            String warning =
                    "Value iteration may not converge if the qualitative threshold is very close to the correct value.";
            if (precisionOption.forceExactSampling) {
                Logger.warn("{} Doing it anyway since you asked for it.", warning);
                threshold = qualitative.threshold();
            } else {
                threshold = switch (comparison) {
                    case GREATER_OR_EQUAL, GREATER_THAN -> qualitative.threshold() - precision.bound();
                    case LESS_OR_EQUAL, LESS_THAN -> qualitative.threshold() + precision.bound();};
                Logger.warn(
                        "{} Widening bounds by specified precision (from {} to {}), specify --force-exact-sampling to disable.",
                        warning,
                        qualitative.threshold(),
                        threshold);
            }

            return new QualitativeVerdict(comparison, threshold);
        }
        assert query instanceof OptimizingQuery;
        return new QuantitativeVerdict(precision.bound(), precision.relativeError());
    }

    public Precision parse() {
        assert precision >= 0.0;
        return new Precision(precision, relativeError);
    }
}
