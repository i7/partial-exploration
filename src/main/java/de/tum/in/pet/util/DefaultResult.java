package de.tum.in.pet.util;

import java.util.Map;
import java.util.Optional;

public record DefaultResult<S>(String name, Optional<?> statistics, Map<S, ?> values) {}
