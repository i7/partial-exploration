package de.tum.in.pet.global;

import de.tum.in.probmodels.problem.verdict.Result;
import java.util.Optional;

public record GlobalResult<S, R>(Result<S, R> result, Optional<?> statistics) {}
