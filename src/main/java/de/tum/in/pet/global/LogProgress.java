package de.tum.in.pet.global;

import de.tum.in.probmodels.model.HyperSystem;
import de.tum.in.probmodels.values.Bounds;
import java.util.Collection;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import org.tinylog.Logger;

public class LogProgress {
    private final long updateDelay;
    private final HyperSystem<?> system;
    private final IntFunction<Bounds> bounds;
    private long nextUpdate;
    private int step = 0;

    public LogProgress(long updateDelay, HyperSystem<?> system, IntFunction<Bounds> bounds) {
        this.updateDelay = updateDelay;
        this.system = system;
        this.bounds = bounds;
        nextUpdate = System.currentTimeMillis() + updateDelay;
    }

    public static <I, S> String formatInitialStates(
            Collection<I> initialStates, Function<I, S> stateFunction, Function<I, Bounds> bounds) {
        if (initialStates.size() > 10) {
            return "(too many initial states)";
        }
        return initialStates.stream()
                .map(state -> String.format("%s: %s", stateFunction.apply(state), bounds.apply(state)))
                .collect(Collectors.joining(", "));
    }

    public void step() {
        step += 1;
        logProgress(false);
    }

    public void force() {
        logProgress(true);
    }

    private void logProgress(boolean force) {
        if (!Logger.isInfoEnabled()) {
            return;
        }
        long now = System.currentTimeMillis();
        if (!force && now < nextUpdate) {
            return;
        }
        nextUpdate = now + updateDelay;
        Logger.info(
                "Step {} Progress report:\nBounds {}",
                step,
                formatInitialStates(system.initialStates(), Function.identity(), bounds::apply));
    }
}
