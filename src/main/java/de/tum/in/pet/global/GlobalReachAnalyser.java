package de.tum.in.pet.global;

import static de.tum.in.probmodels.explorer.ModelBuilder.DuplicateStateIndices.IGNORE_DUPLICATE_INDICES;
import static de.tum.in.probmodels.explorer.SelfLoopHandling.INLINE_LOOPS;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import com.google.gson.annotations.SerializedName;
import de.tum.in.naturals.map.Nat2IntDenseArrayMap;
import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.probmodels.cli.StatisticsVerbosity;
import de.tum.in.probmodels.explorer.ModelBuilder;
import de.tum.in.probmodels.generator.Generator;
import de.tum.in.probmodels.graph.Component;
import de.tum.in.probmodels.graph.ComponentAnalyser;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.model.ControlledSystem;
import de.tum.in.probmodels.model.HyperSystem;
import de.tum.in.probmodels.model.choice.Choice;
import de.tum.in.probmodels.model.impl.DenseCollapsingSystem;
import de.tum.in.probmodels.model.impl.RestrictedGraphView;
import de.tum.in.probmodels.model.impl.RestrictedSystemView;
import de.tum.in.probmodels.model.index.DefaultStateIndexer;
import de.tum.in.probmodels.model.index.StateIndexer;
import de.tum.in.probmodels.output.DefaultStatistics;
import de.tum.in.probmodels.problem.property.UntilQuantity;
import de.tum.in.probmodels.problem.property.UntilType;
import de.tum.in.probmodels.problem.verdict.BoundHandler;
import de.tum.in.probmodels.problem.verdict.BoundVerdict;
import de.tum.in.probmodels.problem.verdict.Result;
import de.tum.in.probmodels.util.FloatUtil;
import de.tum.in.probmodels.values.Bounds;
import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrayFIFOQueue;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntPriorityQueue;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.function.Function;
import org.tinylog.Logger;

public final class GlobalReachAnalyser<L> {
    // Given input
    private final ControlledSystem<Choice<L>> system;
    private final Actor maximizer;
    private final BoundVerdict verdict;
    private final NatBitSet prob1states;

    // Iteration data
    private final double[] lower;
    private final double[] upper;
    private final IntSet activeStates;
    private final Int2IntMap minimizerWitnesses;

    // Component handling
    private final ComponentAnalyser<Choice<L>, ? extends Component<Choice<L>>> analyser;
    private final List<GameComponent<L>> components = new ArrayList<>();
    private final Int2ObjectMap<GameComponent<L>> stateToComponentMap;
    private final ComponentUpdateManager componentUpdates = new ComponentUpdateManager();

    private int step = 0;
    private final LogProgress logProgress;
    private final ReachabilityStatistics statistics;

    public static <S, L> GlobalResult<S, ?> solve(
            Generator<S, L> generator,
            UntilQuantity<S> untilQuantity,
            Actor maximizer,
            BoundHandler<?> verdict,
            StatisticsVerbosity verbosity) {
        DenseCollapsingSystem<Choice<L>> system = DenseCollapsingSystem.choiceBased();
        ReachabilityStatistics statistics = new ReachabilityStatistics();
        statistics.solveTime.start();
        Logger.info("Building the model");
        statistics.modelConstructionTime.start();
        int goal = 0;
        int sink = 1;
        system.addState(goal);
        system.setState(goal, Actor.PROTAGONIST, Choice.onlySingleton(goal));
        system.addState(sink);
        system.setState(sink, Actor.PROTAGONIST, Choice.onlySingleton(sink));

        boolean negated = untilQuantity.isNegated();
        int[] stateCount = {2};
        boolean[] anyGoal = {false};

        DefaultStateIndexer<S> stateIndices = new DefaultStateIndexer<>(s -> {
            var untilType = untilQuantity.type().apply(s);
            if (untilType == UntilType.SAFE) {
                int state = stateCount[0]++;
                assert !system.containsState(state);
                system.addState(state);
                return state;
            }
            if (untilType == UntilType.GOAL) {
                anyGoal[0] = true;
                return goal;
            }
            assert untilType == UntilType.SINK;
            return sink;
        });

        StateIndexer<S> index =
                ModelBuilder.buildUnlabelled(system, generator, INLINE_LOOPS, IGNORE_DUPLICATE_INDICES, stateIndices);
        statistics.modelConstructionTime.stop();
        statistics.modelSize = system.stateCount();
        Logger.info("Done after {}, model has {} states", statistics.modelConstructionTime, system.stateCount());
        assert system.allSuccessorsIn(goal, NatBitSets.singleton(goal))
                && system.allSuccessorsIn(sink, NatBitSets.singleton(sink));

        // TODO Is this correct with negation?
        if (!anyGoal[0]) {
            Logger.debug("Found no goal state, returning trivial result");
            statistics.solutionType = SolutionType.TRIVIAL;
            Bounds bounds = untilQuantity.isNegated() ? Bounds.one() : Bounds.zero();
            return new GlobalResult<>(
                    Result.of(generator.initialStates(), verdict, s -> bounds), Optional.of(statistics));
        }

        var goalStates = NatBitSets.set();
        goalStates.set(goal);
        Function<S, Bounds> boundsFunction;
        if (negated) {
            Function<Bounds, Bounds> boundsNegation = b -> Bounds.of(1.0 - b.upperBound(), 1.0 - b.lowerBound());
            var result = solve(
                    system,
                    maximizer.opponent(),
                    goalStates,
                    bounds -> verdict.isSolved(boundsNegation.apply(bounds)),
                    verbosity,
                    statistics);
            boundsFunction = s -> boundsNegation.apply(result.get(index.indexOf(s)));
        } else {
            var result = solve(system, maximizer, goalStates, verdict, verbosity, statistics);
            boundsFunction = s -> result.get(index.indexOf(s));
        }

        Result<S, ?> result = Result.of(generator.initialStates(), verdict, boundsFunction);
        statistics.solveTime.stop();
        return new GlobalResult<>(result, Optional.of(statistics));
    }

    public static <L> Int2ObjectMap<Bounds> solve(
            HyperSystem<Choice<L>> system, Actor maximizer, NatBitSet goalStates, BoundVerdict verdict) {
        if (goalStates.isEmpty()) {
            Int2ObjectMap<Bounds> result = new Int2ObjectOpenHashMap<>();
            system.initialStates().forEach(s -> result.put(s, Bounds.zero()));
            return result;
        }
        return solve(
                DenseCollapsingSystem.lazyCopy(system),
                maximizer,
                goalStates,
                verdict,
                StatisticsVerbosity.NONE,
                new ReachabilityStatistics());
    }

    private static <L> Int2ObjectMap<Bounds> solve(
            DenseCollapsingSystem<Choice<L>> system,
            Actor maximizer,
            NatBitSet goalStates,
            BoundVerdict verdict,
            StatisticsVerbosity verbosity,
            ReachabilityStatistics statistics) {
        assert !goalStates.isEmpty();
        Logger.info("Running reachability pre-computations on model with {} states", system.stateCount());

        IntSet queued = new IntOpenHashSet();
        IntPriorityQueue queue = new IntArrayFIFOQueue();

        statistics.prob0time.start();
        NatBitSet prob0states = NatBitSets.modifiableCopyOf(system.states());
        prob0states.andNot(goalStates);
        goalStates.forEach(s -> system.predecessors(s).forEach(t -> {
            if (!goalStates.contains(t) && queued.add(t)) {
                queue.enqueue(t);
            }
        }));

        while (!queue.isEmpty()) {
            int s = queue.dequeueInt();
            queued.remove(s);
            assert prob0states.contains(s);

            boolean keep = system.control(s) == maximizer
                    ? system.allSuccessorsIn(s, prob0states)
                    : system.anySupportIn(s, prob0states);
            if (!keep && prob0states.contains(s)) {
                prob0states.clear(s);
                system.predecessors(s).forEach(t -> {
                    if (prob0states.contains(t) && queued.add(t)) {
                        queue.enqueue(t);
                    }
                });
            }
        }
        statistics.prob0time.stop();
        statistics.prob0states = prob0states.size();
        assert !prob0states.intersects(goalStates);
        if (prob0states.isEmpty()) {
            Logger.info("Found no prob0 states!");
            statistics.solutionType = SolutionType.NO_PROB0;
            assert system.states()
                    .intStream()
                    .allMatch(s -> !system.allSuccessorsIn(s, NatBitSets.singleton(s)) || goalStates.contains(s));
            Int2ObjectMap<Bounds> result = new Int2ObjectOpenHashMap<>();
            system.initialStates().forEach(s -> result.put(s, Bounds.one()));
            return result;
        }

        statistics.prob1time.start();
        NatBitSet safeStates = NatBitSets.modifiableCopyOf(prob0states);
        safeStates.flip(0, system.stateCount());

        while (true) {
            NatBitSet safeAndPathToGoal = NatBitSets.modifiableCopyOf(goalStates);
            NatBitSet currentSafeStates = safeStates;
            safeAndPathToGoal.forEach(s -> system.predecessors(s).forEach(t -> {
                if (!goalStates.contains(t)
                        && currentSafeStates.contains(t)
                        && !safeAndPathToGoal.contains(t)
                        && queued.add(t)) {
                    queue.enqueue(t);
                }
            }));

            while (!queue.isEmpty()) {
                int s = queue.dequeueInt();
                queued.remove(s);
                assert currentSafeStates.contains(s);
                assert !safeAndPathToGoal.contains(s);

                boolean maximizerState = system.control(s) == maximizer;
                boolean add = !maximizerState;
                for (Choice<L> choice : system.choices(s)) {
                    boolean isSafe = choice.isSubsetOf(currentSafeStates) && choice.someSuccessorIn(safeAndPathToGoal);
                    if (maximizerState == isSafe) {
                        add = maximizerState;
                        break;
                    }
                }
                if (add) {
                    safeAndPathToGoal.add(s);
                    system.predecessors(s).forEach(t -> {
                        if (currentSafeStates.contains(t) && !safeAndPathToGoal.contains(t) && queued.add(t)) {
                            queue.enqueue(t);
                        }
                    });
                }

                assert currentSafeStates.containsAll(safeAndPathToGoal);
            }
            if (safeStates.equals(safeAndPathToGoal)) {
                break;
            }
            safeStates = safeAndPathToGoal;
        }

        NatBitSet prob1states = safeStates;
        statistics.prob1time.stop();
        statistics.prob1states = prob1states.size();
        assert !prob0states.intersects(prob1states);
        Logger.info(
                "Found {} prob1 states and {} prob0 states from {} goal states in {} and {}, leaving {} remaining states",
                prob1states.size(),
                prob0states.size(),
                goalStates.size(),
                statistics.prob1time,
                statistics.prob0time,
                system.stateCount() - prob0states.size() - prob1states.size());

        if (system.initialStates().intStream().allMatch(s -> prob1states.contains(s) || prob0states.contains(s))) {
            statistics.solutionType = SolutionType.GRAPH_ANALYSIS;
            Int2ObjectMap<Bounds> result = new Int2ObjectOpenHashMap<>();
            system.initialStates().forEach(s -> result.put(s, prob1states.contains(s) ? Bounds.one() : Bounds.zero()));
            return result;
        }

        int sink = system.makeAbsorbing(prob0states);
        int goal = system.makeAbsorbing(prob1states);
        ComponentAnalyser<Choice<L>, ?> analyser = ComponentAnalyser.defaultAnalyser(system);

        if (system.hasAntagonistControlledStates()) {
            var maximizerComponents = analyser.findComponentStates(
                    s -> {
                        List<Choice<L>> choices = system.choices(s);
                        return choices.size() == 1 || system.control(s) == maximizer ? choices : List.of();
                    },
                    system.states());
            system.collapse(maximizerComponents, Function.identity(), c -> Actor.PROTAGONIST, INLINE_LOOPS);

            var minimizerComponents = analyser.findComponentStates(
                    s -> {
                        List<Choice<L>> choices = system.choices(s);
                        return choices.size() > 1 && system.control(s) == maximizer ? List.of() : choices;
                    },
                    system.antagonistControlledStates());
            for (IntSet minimizerComponent : minimizerComponents) {
                system.makeAbsorbing(minimizerComponent);
            }
            if (!(maximizerComponents.isEmpty() && minimizerComponents.isEmpty())) {
                Logger.debug(
                        "Collapsed {} maximizer and {} minimizer components",
                        maximizerComponents.size(),
                        minimizerComponents.size());
            }
        } else {
            var components = analyser.findComponentStates(system);
            system.collapse(components, Function.identity(), c -> Actor.PROTAGONIST, INLINE_LOOPS);
            if (!components.isEmpty()) {
                Logger.debug("Collapsed {} components", components.size());
            }
        }

        return new GlobalReachAnalyser<>(system, maximizer, goal, sink, verdict, analyser, verbosity, statistics)
                .solveGame();
    }

    private GlobalReachAnalyser(
            DenseCollapsingSystem<Choice<L>> system,
            Actor maximizer,
            int goal,
            int sink,
            BoundVerdict verdict,
            ComponentAnalyser<Choice<L>, ? extends Component<Choice<L>>> analyser,
            StatisticsVerbosity verbosity,
            ReachabilityStatistics statistics) {
        this.maximizer = maximizer;
        this.system = system;
        this.statistics = statistics;
        this.prob1states = NatBitSets.set();
        prob1states.set(goal);
        this.verdict = verdict;
        this.analyser = analyser;

        int stateCount = system.states().lastInt() + 1;
        var unsolvedStates = NatBitSets.copyOf(system.states());
        unsolvedStates.clear(goal);
        unsolvedStates.clear(sink);

        Stopwatch componentTimer = Stopwatch.createStarted();
        if (system.hasAntagonistControlledStates()) {
            var components =
                    analyser.findComponents(RestrictedGraphView.of(system, unsolvedStates), system.initialStates());
            for (Component<Choice<L>> component : components) {
                assert component.size() > 1;
                this.components.add(new GameComponent<>(this.components.size(), component, new ArrayList<>()));
            }
        } else {
            assert analyser.findComponents(RestrictedGraphView.of(system, unsolvedStates), system.initialStates())
                    .isEmpty();
        }
        var componentTime = componentTimer.elapsed();

        stateToComponentMap = new Int2ObjectOpenHashMap<>();
        stateToComponentMap.defaultReturnValue(null);
        for (GameComponent<L> subsystem : this.components) {
            subsystem.component.states().forEach(s -> stateToComponentMap.put(s, subsystem));
        }

        minimizerWitnesses = new Nat2IntDenseArrayMap(stateCount);
        minimizerWitnesses.defaultReturnValue(0);

        activeStates = new IntOpenHashSet(system.states());
        activeStates.remove(goal);
        activeStates.remove(sink);

        lower = new double[stateCount];
        lower[goal] = 1.0;
        upper = new double[stateCount];
        Arrays.fill(upper, 1.0);
        upper[sink] = 0.0;

        if (this.components.isEmpty()) {
            Logger.info("Game has no non-trivial subsystems");
        } else if (Logger.isInfoEnabled() || verbosity.doMore()) {
            var componentStatistics =
                    DefaultStatistics.extract(Lists.transform(components, GameComponent::states), componentTime);
            Logger.info(
                    "Game has {} non-goal subsystems with {}/{}/{} (min/avg/max) states, {} in total",
                    this.components.size(),
                    componentStatistics.minimumSize(),
                    componentStatistics.averageSize(),
                    componentStatistics.maximumSize(),
                    componentStatistics.sumSize());
            statistics.components = componentStatistics;
        }

        statistics.solutionType = SolutionType.ITERATION;
        logProgress = new LogProgress(5000, system, s -> Bounds.of(lower[s], upper[s]));

        Logger.trace("Initial SEC discovery");
        statistics.secDiscoveryTime.start();
        for (GameComponent<L> component : components) {
            updateSECs(component);
        }
        statistics.secDiscoveryTime.stop();
    }

    private boolean hasActiveState(IntSet set) {
        return NatBitSets.intersects(set, activeStates);
    }

    private void updateSECs(GameComponent<L> subsystem) {
        statistics.secDiscoveries += 1;
        var gameComponent = subsystem.component;
        subsystem.secs.clear();

        var componentView = RestrictedSystemView.of(
                system,
                gameComponent.states(),
                state -> {
                    if (!activeStates.contains(state)) {
                        return List.of();
                    }
                    List<Choice<L>> choices = gameComponent.choices(state);
                    if (choices.size() == 1 || maximizes(state)) {
                        return choices;
                    }
                    Choice<L> choice = system.choices(state).get(minimizerWitnesses.get(state));
                    return gameComponent.isStable(choice) ? List.of(choice) : List.of();
                },
                gameComponent.states());

        for (Component<Choice<L>> inducedComponent : analyser.findComponents(componentView)) {
            assert hasActiveState(inducedComponent.states());
            assert !prob1states.intersects(inducedComponent.states());

            Set<Choice<L>> exits = new HashSet<>();
            IntIterator componentIterator = inducedComponent.states().intIterator();
            while (componentIterator.hasNext()) {
                int s = componentIterator.nextInt();
                if (maximizes(s)) {
                    for (Choice<L> choice : system.choices(s)) {
                        choice.removeSuccessors(inducedComponent.states()).ifPresent(exits::add);
                    }
                }
            }
            if (exits.isEmpty()) {
                assert inducedComponent.stateStream().allMatch(s -> lower[s] == 0.0);
                inducedComponent.states().forEach(s -> upper[s] = 0.0);
                activeStates.removeAll(inducedComponent.states());
            } else {
                subsystem.secs.add(new ReachSEC<>(inducedComponent, exits));
            }
        }
    }

    private boolean maximizes(int state) {
        return system.control(state) == maximizer;
    }

    private double updateBounds(int state) {
        List<Choice<L>> choices = system.choices(state);
        int choiceCount = choices.size();
        statistics.reachUpdates += 1;

        double optLower;
        double optUpper;

        if (choiceCount == 1) {
            Choice<L> choice = choices.get(0);
            optLower = choice.sumWeighted(this.lower);
            optUpper = choice.sumWeighted(this.upper);
        } else if (choiceCount == 2) {
            // This is a hot area of the code and states with few choices are common.
            // Having this explicit avoids construction of iterators
            Choice<L> one = choices.get(0);
            Choice<L> other = choices.get(1);
            double oneLower = one.sumWeighted(this.lower);
            double oneUpper = one.sumWeighted(this.upper);
            double otherLower = other.sumWeighted(this.lower);
            double otherUpper = other.sumWeighted(this.upper);
            if (maximizes(state)) {
                optLower = Math.max(oneLower, otherLower);
                optUpper = Math.max(oneUpper, otherUpper);
            } else {
                int witness;
                if (oneLower < otherLower) {
                    witness = 0;
                    optLower = oneLower;
                } else {
                    witness = 1;
                    optLower = otherLower;
                }

                var component = stateToComponentMap.get(state);
                if (component != null) {
                    int oldWitness = minimizerWitnesses.put(state, witness);
                    if (oldWitness != witness) {
                        componentUpdates.markStale(component);
                    }
                }

                optUpper = Math.min(oneUpper, otherUpper);
            }
        } else if (maximizes(state)) {
            double maxLower = Double.NEGATIVE_INFINITY;
            double maxUpper = Double.NEGATIVE_INFINITY;
            for (int i = 0; i < choiceCount; i++) { // Avoid foreach -> avoid iterator
                Choice<L> choice = choices.get(i);
                double lowerBound = choice.sumWeighted(lower);
                if (lowerBound > maxLower) {
                    maxLower = lowerBound;
                }
                double upperBound = choice.sumWeighted(upper);
                if (upperBound > maxUpper) {
                    maxUpper = upperBound;
                }
            }
            assert Double.isFinite(maxLower) && Double.isFinite(maxUpper);

            optLower = maxLower;
            optUpper = maxUpper;
        } else {
            int witness = -1;
            double minLower = Double.POSITIVE_INFINITY;
            double minUpper = Double.POSITIVE_INFINITY;
            for (int i = 0; i < choiceCount; i++) {
                Choice<L> choice = choices.get(i);
                double lowerBound = choice.sumWeighted(lower);
                if (lowerBound < minLower) {
                    minLower = lowerBound;
                    witness = i;
                }
                double upperBound = choice.sumWeighted(upper);
                if (upperBound < minUpper) {
                    minUpper = upperBound;
                }
            }
            assert Double.isFinite(minLower) && Double.isFinite(minUpper);
            assert witness >= 0;

            var component = stateToComponentMap.get(state);
            if (component != null) {
                int oldWitness = minimizerWitnesses.put(state, witness);
                if (oldWitness != witness) {
                    componentUpdates.markStale(component);
                }
            }

            optLower = minLower;
            optUpper = minUpper;
        }
        lower[state] = optLower;
        upper[state] = optUpper;
        return optUpper - optLower;
    }

    private boolean deflate(ReachSEC<L> sec) {
        assert !sec.maximizerExits.isEmpty();

        double bestExit = 0.0;
        for (Choice<L> choice : sec.maximizerExits) {
            double exit = choice.sumWeighted(upper);
            if (exit > bestExit) {
                bestExit = exit;
                if (FloatUtil.isOne(bestExit)) {
                    return false;
                }
            }
        }

        Logger.trace("Deflating {} with {}", sec, bestExit);
        IntIterator secStateIterator = sec.component.states().intIterator();
        while (secStateIterator.hasNext()) {
            int s = secStateIterator.nextInt();
            if (upper[s] > bestExit) {
                upper[s] = bestExit;
            }
        }

        if (bestExit == 0.0) {
            activeStates.removeAll(sec.component.states());
            return true;
        }
        return false;
    }

    private Int2ObjectMap<Bounds> solveGame() {
        Logger.debug("Beginning iteration");
        var overallIterationTime = Stopwatch.createStarted();

        IntIterator initialStates = system.initialStates().intIterator();
        while (initialStates.hasNext()) {
            int initialState = initialStates.nextInt();
            while (!verdict.isSolved(Bounds.of(lower[initialState], upper[initialState]))) {
                // Basic value iteration
                statistics.reachIterationTime.start();
                var activeStateIterator = activeStates.iterator();
                while (activeStateIterator.hasNext()) {
                    int state = activeStateIterator.nextInt();
                    if (FloatUtil.isZero(updateBounds(state))) {
                        activeStateIterator.remove();
                    }
                }
                statistics.reachIterationTime.stop();

                // Detect new SECs
                statistics.secDiscoveryTime.start();
                componentUpdates.performUpdates();
                statistics.secDiscoveryTime.stop();

                // Deflate
                statistics.flateCount += components.size();
                statistics.flateTime.start();
                for (GameComponent<L> component : components) {
                    component.secs.removeIf(this::deflate);
                }
                statistics.flateTime.stop();

                step += 1;
                statistics.reachIterations += 1;
                logProgress.step();
            }
        }

        Logger.info(
                "Converged after iterating for {} steps in {} (VI {}, SEC {} in {} subsystems, Deflate {})",
                step,
                overallIterationTime.stop(),
                statistics.reachIterationTime,
                statistics.secDiscoveryTime,
                componentUpdates.updatedSubsystems,
                statistics.flateTime);

        Int2ObjectMap<Bounds> results = new Int2ObjectOpenHashMap<>(system.initialStateCount());
        IntIterator initialStatesResultIterator = system.initialStates().intIterator();
        while (initialStatesResultIterator.hasNext()) {
            int initialState = initialStatesResultIterator.nextInt();
            results.put(initialState, Bounds.of(lower[initialState], upper[initialState]));
        }
        return results;
    }

    public record ReachSEC<L>(Component<Choice<L>> component, Set<Choice<L>> maximizerExits) {
        @Override
        public String toString() {
            return component.toString();
        }
    }

    public record GameComponent<L>(int index, Component<Choice<L>> component, List<ReachSEC<L>> secs) {
        @Override
        public boolean equals(Object o) {
            return this == o || o instanceof GameComponent<?> that && index == that.index;
        }

        @Override
        public int hashCode() {
            return index;
        }

        public IntSet states() {
            return component.states();
        }
    }

    public final class ComponentUpdateManager {
        private final Set<GameComponent<L>> queued = new HashSet<>();
        private final Queue<UpdateEntry<L>> queue = new PriorityQueue<>(Comparator.comparing(UpdateEntry::step));
        private final Set<GameComponent<L>> immediateUpdates = new HashSet<>();
        private final Object2IntMap<GameComponent<L>> componentDelays = new Object2IntOpenHashMap<>();
        private int updatedSubsystems = 0;

        record UpdateEntry<L>(int step, GameComponent<L> component) {}

        void markStale(GameComponent<L> component) {
            if (component.component.size() <= step) {
                immediateUpdates.add(component);
                return;
            }

            if (!queued.add(component)) {
                return;
            }
            int updateStep = step
                    + Integer.highestOneBit(component.component.size()) // Logarithmic
                    + queue.size()
                    + componentDelays.computeInt(component, (c, i) -> (i == null || i == 0) ? 1 : i + 10);
            queue.add(new UpdateEntry<>(updateStep, component));
        }

        void performUpdates() {
            int count = 0;
            for (GameComponent<L> component : immediateUpdates) {
                count += 1;
                updateSECs(component);
            }
            immediateUpdates.clear();
            while (!queue.isEmpty() && queue.peek().step <= step) {
                GameComponent<L> component = queue.poll().component;
                count += 1;
                queued.remove(component);
                updateSECs(component);
            }

            if (count > 0) {
                updatedSubsystems += count;
                Logger.info("Updated {} subsystems", count);
            }
        }
    }

    public enum SolutionType {
        @SerializedName("trivial")
        TRIVIAL,
        @SerializedName("no_prob_zero")
        NO_PROB0,
        @SerializedName("graph")
        GRAPH_ANALYSIS,
        @SerializedName("iteration")
        ITERATION
    }

    public static final class ReachabilityStatistics extends GlobalStatistics {
        @SerializedName("solution")
        public SolutionType solutionType = SolutionType.ITERATION;

        public Stopwatch prob0time = Stopwatch.createUnstarted();
        public Stopwatch prob1time = Stopwatch.createUnstarted();
        public int prob0states;
        public int prob1states;
    }
}
