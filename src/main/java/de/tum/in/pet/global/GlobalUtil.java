package de.tum.in.pet.global;

import de.tum.in.probmodels.model.choice.Choice;
import java.util.List;

public final class GlobalUtil {
    private GlobalUtil() {}

    public static double updateBounds(
            int state, List<? extends Choice<?>> choices, boolean maximizes, double[] lower, double[] upper) {
        int choiceCount = choices.size();
        double optLower;
        double optUpper;

        if (choiceCount == 1) {
            Choice<?> distribution = choices.get(0);
            optLower = distribution.sumWeighted(lower);
            optUpper = distribution.sumWeighted(upper);
        } else if (choiceCount == 2) {
            // This is a hot area of the code and states with few choices are common.
            // Having this explicit avoids construction of iterators
            Choice<?> one = choices.get(0);
            Choice<?> other = choices.get(1);
            double oneLower = one.sumWeighted(lower);
            double oneUpper = one.sumWeighted(upper);
            double otherLower = other.sumWeighted(lower);
            double otherUpper = other.sumWeighted(upper);
            if (maximizes) {
                optLower = Math.max(oneLower, otherLower);
                optUpper = Math.max(oneUpper, otherUpper);
            } else {
                optLower = Math.min(oneLower, otherLower);
                optUpper = Math.min(oneUpper, otherUpper);
            }
        } else if (maximizes) {
            double maxLower = Double.NEGATIVE_INFINITY;
            double maxUpper = Double.NEGATIVE_INFINITY;
            for (int i = 0; i < choiceCount; i++) { // Avoid foreach -> avoid iterator
                Choice<?> distribution = choices.get(i);
                double lowerBound = distribution.sumWeighted(lower);
                if (lowerBound > maxLower) {
                    maxLower = lowerBound;
                }
                double upperBound = distribution.sumWeighted(upper);
                if (upperBound > maxUpper) {
                    maxUpper = upperBound;
                }
            }
            assert Double.isFinite(maxLower) && Double.isFinite(maxUpper);

            optLower = maxLower;
            optUpper = maxUpper;
        } else {
            double minLower = Double.POSITIVE_INFINITY;
            double minUpper = Double.POSITIVE_INFINITY;
            for (int i = 0; i < choiceCount; i++) {
                Choice<?> distribution = choices.get(i);
                double lowerBound = distribution.sumWeighted(lower);
                if (lowerBound < minLower) {
                    minLower = lowerBound;
                }
                double upperBound = distribution.sumWeighted(upper);
                if (upperBound < minUpper) {
                    minUpper = upperBound;
                }
            }
            assert Double.isFinite(minLower) && Double.isFinite(minUpper);

            optLower = minLower;
            optUpper = minUpper;
        }
        lower[state] = optLower;
        upper[state] = optUpper;
        return optUpper - optLower;
    }
}
