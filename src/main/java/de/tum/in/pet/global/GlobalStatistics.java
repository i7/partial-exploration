package de.tum.in.pet.global;

import com.google.common.base.Stopwatch;
import de.tum.in.probmodels.output.ComponentsStatistics;
import javax.annotation.Nullable;

public class GlobalStatistics {
    public Stopwatch modelConstructionTime = Stopwatch.createUnstarted();
    public int modelSize;

    public Stopwatch solveTime = Stopwatch.createUnstarted();

    public Stopwatch flateTime = Stopwatch.createUnstarted();
    public int flateCount;

    public Stopwatch secDiscoveryTime = Stopwatch.createUnstarted();
    public int secDiscoveries;

    public Stopwatch reachIterationTime = Stopwatch.createUnstarted();
    public int reachUpdates;
    public int reachIterations;

    @Nullable
    public ComponentsStatistics components;
}
