package de.tum.in.pet.global;

import static de.tum.in.probmodels.explorer.ModelBuilder.DuplicateStateIndices.UNIQUE_INDICES;
import static de.tum.in.probmodels.explorer.SelfLoopHandling.KEEP_LOOPS;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import de.tum.in.pet.sampling.SEC;
import de.tum.in.pet.sampling.SimpleTrackingSEC;
import de.tum.in.pet.sampling.TrackingSEC;
import de.tum.in.probmodels.cli.StatisticsVerbosity;
import de.tum.in.probmodels.explorer.IncreasingIndexProvider;
import de.tum.in.probmodels.explorer.ModelBuilder;
import de.tum.in.probmodels.explorer.RewardCache;
import de.tum.in.probmodels.generator.Generator;
import de.tum.in.probmodels.graph.Component;
import de.tum.in.probmodels.graph.ComponentAnalyser;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.model.ControlledSystem;
import de.tum.in.probmodels.model.RewardFunction;
import de.tum.in.probmodels.model.choice.Choice;
import de.tum.in.probmodels.model.impl.DenseCollapsingSystem;
import de.tum.in.probmodels.model.index.DefaultStateIndexer;
import de.tum.in.probmodels.output.DefaultStatistics;
import de.tum.in.probmodels.problem.property.MeanPayoffQuantity;
import de.tum.in.probmodels.problem.verdict.BoundHandler;
import de.tum.in.probmodels.problem.verdict.BoundVerdict;
import de.tum.in.probmodels.problem.verdict.QuantitativeVerdict;
import de.tum.in.probmodels.problem.verdict.Result;
import de.tum.in.probmodels.util.FloatUtil;
import de.tum.in.probmodels.values.Bounds;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.annotation.Nullable;
import org.tinylog.Logger;

@SuppressWarnings("PMD.TooManyFields") // LOL
public final class GlobalMeanPayoffAnalyser<L> {
    // Given input
    private final ControlledSystem<Choice<L>> system;
    private final RewardFunction rewards;
    private final Actor maximizer;
    private final BoundVerdict verdict;

    // Total reward data
    boolean totalRewardSwapped = false;
    private final Int2DoubleMap totalRewardOne;
    private final Int2DoubleMap totalRewardOther;
    private final Int2ObjectMap<Choice<L>> strategy;

    // Propagation data
    private final Bounds rewardBounds;
    private final double[] lower;
    private final double[] upper;
    private final IntSet activeStates;

    // Component handling
    private final ComponentAnalyser<Choice<L>, ? extends Component<Choice<L>>> analyser;
    private final List<Region<L>> components = new ArrayList<>();

    private final LogProgress logProgress;
    private final MeanPayoffStatistics statistics;

    public static <S, L> GlobalResult<S, ?> solve(
            Generator<S, L> generator,
            MeanPayoffQuantity<S> quantity,
            Actor maximizer,
            BoundHandler<?> verdict,
            StatisticsVerbosity verbosity) {
        Logger.info("Building the model");
        var statistics = new MeanPayoffStatistics();
        statistics.solveTime.start();
        DenseCollapsingSystem<Choice<L>> system = DenseCollapsingSystem.choiceBased();
        statistics.modelConstructionTime.start();
        var index = ModelBuilder.buildLabelled(
                system,
                generator,
                KEEP_LOOPS,
                UNIQUE_INDICES,
                new DefaultStateIndexer<>(new IncreasingIndexProvider<>(system::addState)));
        statistics.modelConstructionTime.stop();
        statistics.modelSize = system.stateCount();
        Logger.info("Model has {} states", system.stateCount());

        statistics.rewardConstructionTime.start();
        RewardCache<S> rewardFunction = new RewardCache<>(quantity.reward());
        index.forEach((s, i) -> rewardFunction.exploreReward(s, i, system.choices(i)));
        statistics.rewardConstructionTime.stop();
        if (Logger.isInfoEnabled()) {
            Logger.info("Reward bounds of overall model: {}", rewardFunction.rewardBounds(system.states()));
        }
        var bounds = solve(system, maximizer, rewardFunction, verdict, statistics, verbosity);
        Result<S, ?> result = Result.of(generator.initialStates(), verdict, s -> bounds.get(index.indexOf(s)));
        statistics.solveTime.stop();
        return new GlobalResult<>(result, Optional.of(statistics));
    }

    private static <L> Int2ObjectMap<Bounds> solve(
            DenseCollapsingSystem<Choice<L>> system,
            Actor maximizer,
            RewardFunction rewardFunction,
            BoundVerdict verdict,
            MeanPayoffStatistics statistics,
            StatisticsVerbosity verbosity) {
        // TODO Some kind of attractor pre-computation?
        return new GlobalMeanPayoffAnalyser<>(system, maximizer, rewardFunction, verdict, statistics, verbosity)
                .solveGame();
    }

    private GlobalMeanPayoffAnalyser(
            DenseCollapsingSystem<Choice<L>> system,
            Actor maximizer,
            RewardFunction rewardFunction,
            BoundVerdict verdict,
            MeanPayoffStatistics statistics,
            StatisticsVerbosity verbosity) {
        // TODO Construct copies of the game: One *only* holds the components, and one collapsing one that does the
        //     propagation - dynamically create one representative for each SEC

        this.system = system;
        this.rewards = rewardFunction;
        this.maximizer = maximizer;
        this.verdict = verdict;
        this.analyser = ComponentAnalyser.defaultAnalyser(system);
        this.statistics = statistics;
        int stateCount = system.stateCount();
        activeStates = new IntOpenHashSet(system.states());

        double initialPrecision;
        if (verdict instanceof QuantitativeVerdict quantitative) {
            initialPrecision = quantitative.precision();
        } else {
            initialPrecision = 0.1;
        }

        // TODO Search & collapse components controlled by one actor
        //      Careful: We cannot collapse components that are part of true regions!
        assert system.states().intStream().allMatch(system::isDefinedState);
        Stopwatch componentTimer = Stopwatch.createStarted();
        var components = analyser.findComponents(system, ComponentAnalyser.ComponentCopyType.EAGER);

        double minimalReward = Double.POSITIVE_INFINITY;
        double maximalReward = Double.NEGATIVE_INFINITY;
        for (Component<Choice<L>> component : components) {
            Region<L> region = new Region<>(component, initialPrecision);
            this.components.add(region);
            var stateIterator = component.states().iterator();
            while (stateIterator.hasNext()) {
                int state = stateIterator.nextInt();

                double reward = rewards.stateReward(state);
                if (rewards.mayHaveChoiceRewards()) {
                    double minimalChoiceReward = Double.POSITIVE_INFINITY;
                    double maximalChoiceReward = Double.NEGATIVE_INFINITY;
                    for (Choice<L> choice : component.choices(state)) {
                        double choiceReward = rewards.choiceReward(state, choice.label());
                        if (choiceReward < minimalChoiceReward) {
                            minimalChoiceReward = choiceReward;
                        }
                        if (choiceReward > maximalChoiceReward) {
                            maximalChoiceReward = choiceReward;
                        }
                    }
                    if (minimalReward > reward + minimalChoiceReward) {
                        minimalReward = reward + minimalChoiceReward;
                    }
                    if (maximalReward < reward + maximalChoiceReward) {
                        maximalReward = reward + maximalChoiceReward;
                    }
                } else {
                    if (minimalReward > reward) {
                        minimalReward = reward;
                    }
                    if (maximalReward < reward) {
                        maximalReward = reward;
                    }
                }
            }
        }
        rewardBounds = Bounds.of(minimalReward, maximalReward);
        var componentTime = componentTimer.stop().elapsed();

        if (this.components.isEmpty()) {
            Logger.info("Game has no non-trivial subsystems");
        } else if (Logger.isInfoEnabled() || verbosity.doMore()) {
            var componentStatistics =
                    DefaultStatistics.extract(Lists.transform(components, Component::states), componentTime);
            Logger.info(
                    "Game has {} subsystems with {}/{}/{} (min/avg/max) states, {} in total",
                    this.components.size(),
                    componentStatistics.minimumSize(),
                    componentStatistics.averageSize(),
                    componentStatistics.maximumSize(),
                    componentStatistics.sumSize());
            statistics.components = componentStatistics;
        }

        // TODO If most states are in regions, use dense array
        totalRewardOne = new Int2DoubleOpenHashMap(stateCount);
        totalRewardOther = new Int2DoubleOpenHashMap(stateCount);
        strategy = new Int2ObjectOpenHashMap<>(stateCount);
        lower = new double[stateCount];
        upper = new double[stateCount];
        assert Double.isFinite(rewardBounds.lowerBound()) && Double.isFinite(rewardBounds.upperBound());
        if (rewardBounds.lowerBound() != 0.0) {
            Arrays.fill(lower, rewardBounds.lowerBound());
        }
        if (rewardBounds.upperBound() != 0.0) {
            Arrays.fill(upper, rewardBounds.upperBound());
        }

        logProgress = new LogProgress(5000, system, s -> Bounds.of(lower[s], upper[s]));
    }

    private Bounds totalRewardStep(
            Component<Choice<L>> component,
            Int2DoubleMap current,
            Int2DoubleMap next,
            boolean updateValue,
            boolean updateStrategy,
            @Nullable Runnable strategyChangeAction) {
        double alpha = 0.9;
        IntIterator stateIterator = component.states().iterator();
        double minimalDifference = Double.POSITIVE_INFINITY;
        double maximalDifference = Double.NEGATIVE_INFINITY;
        boolean anyChange = false;

        while (stateIterator.hasNext()) {
            int state = stateIterator.nextInt();
            double currentValue = current.get(state);
            double stateValue = rewards.stateReward(state);

            boolean maximizing = system.control(state) == maximizer;
            double optimum = maximizing ? Double.NEGATIVE_INFINITY : Double.POSITIVE_INFINITY;
            Choice<L> witness = null;
            List<Choice<L>> choices = component.choices(state);
            assert !choices.isEmpty();
            for (Choice<L> choice : choices) {
                double reward = rewards.choiceReward(state, choice.label()) / alpha;
                double value = choice.sumWeighted(current) + reward;
                if (maximizing) {
                    if (value > optimum) {
                        optimum = value;
                        witness = choice;
                    }
                } else {
                    if (value < optimum) {
                        optimum = value;
                        witness = choice;
                    }
                }
            }
            if (updateStrategy && choices.size() > 1) {
                Choice<L> oldWitness = strategy.put(state, witness);
                anyChange = anyChange || !Objects.equals(witness, oldWitness);
            }

            FloatUtil.KahanSum valueSum = new FloatUtil.KahanSum(stateValue);
            valueSum.add(alpha * optimum);
            double nextValue = valueSum.add((1 - alpha) * currentValue);
            if (updateValue) {
                next.put(state, nextValue);
            }
            double difference = valueSum.add(-currentValue);
            if (difference < minimalDifference) {
                minimalDifference = difference;
            }
            if (difference > maximalDifference) {
                maximalDifference = difference;
            }
        }
        statistics.totalRewardUpdates += component.size();
        if (anyChange && strategyChangeAction != null) {
            strategyChangeAction.run();
        }
        return Bounds.of(minimalDifference, maximalDifference);
    }

    private void iterateTotalReward(Region<L> region) {
        Int2DoubleMap current = totalRewardSwapped ? this.totalRewardOther : this.totalRewardOne;
        Int2DoubleMap next = totalRewardSwapped ? this.totalRewardOne : this.totalRewardOther;

        int stepBound = region.component.size();
        int steps = 0;

        boolean converged = true;
        boolean exit = false;
        double achieved = Double.POSITIVE_INFINITY;
        boolean[] strategyChanged = {false};

        statistics.rewardIterationTime.start();
        if (region.targetPrecision > 0.0) {
            while (!exit) {
                // TODO Early stopping?
                if (steps < stepBound) {
                    Bounds bounds = totalRewardStep(region.component, current, next, true, false, null);
                    achieved = bounds.difference();
                    converged = bounds.difference() < region.targetPrecision;
                    exit = converged;
                } else {
                    exit = true;
                }
                if (exit) {
                    totalRewardStep(region.component, current, next, true, true, () -> strategyChanged[0] = true);
                }
                steps += 1;

                Int2DoubleMap swap = current;
                current = next;
                next = swap;
            }
            if (FloatUtil.isZero(achieved)) {
                region.targetPrecision = 0.0;
            } else if (converged) {
                // TODO Change this heuristically
                region.targetPrecision /= 2.0;
            }
        }
        statistics.rewardIterationTime.stop();

        if (strategyChanged[0] || region.secs.isEmpty()) {
            statistics.secDiscoveries += 1;
            statistics.secDiscoveryTime.start();
            region.secs.clear();
            Component<Choice<L>> component = region.component;

            for (Component<Choice<L>> sec : analyser.findComponents(
                    SEC.control(component, maximizer, system::control, activeStates::contains, strategy::get),
                    component.states())) {
                region.secs.add(SimpleTrackingSEC.of(system, sec, maximizer, rewardBounds));
            }
            for (Component<Choice<L>> sec : analyser.findComponents(
                    SEC.control(
                            component, maximizer.opponent(), system::control, activeStates::contains, strategy::get),
                    component.states())) {
                region.secs.add(SimpleTrackingSEC.of(system, sec, maximizer.opponent(), rewardBounds));
            }
            statistics.secDiscoveryTime.stop();
        }

        var iterator = region.secs.iterator();
        while (iterator.hasNext()) {
            var bSEC = iterator.next();
            bSEC.set(totalRewardStep(bSEC.component(), next, current, false, false, null));
            // assert FloatUtil.lessOrEqual(bSEC.get().difference(), achieved);
            // TODO The above doesn't always hold -- maybe with rounding or if the SEC had a sub-optimal decision?
            statistics.flateCount += 1;
            statistics.flateTime.start();
            boolean flate = flate(bSEC);
            statistics.flateTime.stop();
            if (flate) {
                iterator.remove();
                activeStates.removeAll(bSEC.component().states());
            } else if (FloatUtil.isZero(bSEC.get().difference())) {
                iterator.remove();
            }
        }

        totalRewardSwapped = current == this.totalRewardOther; // NOPMD
    }

    private boolean flate(TrackingSEC<L, Bounds> sec) {
        Bounds stayingBounds = sec.get();
        boolean allConverged = true;
        if (sec.owner() == maximizer) {
            double bestValue = stayingBounds.upperBound();
            for (Choice<L> distribution : sec.ownerExits()) {
                double exit = distribution.sumWeighted(upper);
                if (exit > bestValue) {
                    if (FloatUtil.isEqual(bestValue, rewardBounds.upperBound())) {
                        return sec.component()
                                .states()
                                .intStream()
                                .allMatch(s -> FloatUtil.isEqual(lower[s], rewardBounds.upperBound()));
                    }
                    bestValue = exit;
                }
            }

            Logger.trace("Deflating SEC to {} (staying {}): {}", bestValue, stayingBounds, sec);
            IntIterator secStateIterator = sec.component().states().intIterator();
            while (secStateIterator.hasNext()) {
                int s = secStateIterator.nextInt();
                if (upper[s] > bestValue) {
                    upper[s] = bestValue;
                }
                allConverged = allConverged && (FloatUtil.isZero(upper[s] - lower[s]));
            }
        } else {
            double bestValue = stayingBounds.lowerBound();
            for (Choice<L> distribution : sec.ownerExits()) {
                double exit = distribution.sumWeighted(lower);
                if (exit < bestValue) {
                    if (FloatUtil.isEqual(bestValue, rewardBounds.lowerBound())) {
                        return sec.component()
                                .states()
                                .intStream()
                                .allMatch(s -> FloatUtil.isEqual(upper[s], rewardBounds.lowerBound()));
                    }
                    bestValue = exit;
                }
            }

            Logger.trace("Inflating SEC to {} (staying {}): {}", bestValue, stayingBounds, sec);
            IntIterator secStateIterator = sec.component().states().intIterator();
            while (secStateIterator.hasNext()) {
                int s = secStateIterator.nextInt();
                if (lower[s] < bestValue) {
                    lower[s] = bestValue;
                }
                allConverged = allConverged && (FloatUtil.isZero(upper[s] - lower[s]));
            }
        }
        return allConverged;
    }

    public Int2ObjectMap<Bounds> solveGame() {
        IntIterator initialStates = system.initialStates().intIterator();
        while (initialStates.hasNext()) {
            int initialState = initialStates.nextInt();
            while (!verdict.isSolved(Bounds.of(lower[initialState], upper[initialState]))) {
                // Basic value iteration
                for (Region<L> region : this.components) {
                    iterateTotalReward(region);
                }

                int step = 0;
                int bound = system.stateCount();
                while (true) {
                    boolean anyConverged = false;
                    boolean anyChanged = false;
                    statistics.reachIterationTime.start();
                    var stateIterator = activeStates.iterator();
                    while (stateIterator.hasNext()) {
                        int state = stateIterator.nextInt();

                        double oldDifference = anyChanged ? 0.0 : upper[state] - lower[state];
                        double difference = GlobalUtil.updateBounds(
                                state, system.choices(state), system.control(state) == maximizer, lower, upper);
                        anyChanged = anyChanged || FloatUtil.strictlyLess(difference, oldDifference);

                        if (FloatUtil.isZero(difference)) {
                            stateIterator.remove();
                            anyConverged = true;
                        }
                    }
                    statistics.reachIterationTime.stop();
                    statistics.reachIterations += 1;
                    statistics.reachUpdates += activeStates.size();
                    if (!anyChanged) {
                        break;
                    }
                    if (anyConverged) {
                        continue;
                    }
                    step += 1;
                    if (step >= bound) {
                        break;
                    }
                }

                logProgress.step();
            }
        }
        logProgress.force();

        Int2ObjectMap<Bounds> bounds = new Int2ObjectOpenHashMap<>(system.initialStateCount());
        system.initialStates().forEach((int s) -> bounds.put(s, Bounds.of(lower[s], upper[s])));
        return bounds;
    }

    public static final class Region<L> {
        final Component<Choice<L>> component;
        final List<SimpleTrackingSEC<L, Bounds>> secs;
        double targetPrecision;

        private Region(
                Component<Choice<L>> component, List<SimpleTrackingSEC<L, Bounds>> secs, double targetPrecision) {
            this.component = component;
            this.secs = secs;
            this.targetPrecision = targetPrecision;
        }

        Region(Component<Choice<L>> component, double targetPrecision) {
            this(component, new ArrayList<>(), targetPrecision);
        }
    }

    public static final class MeanPayoffStatistics extends GlobalStatistics {
        public Stopwatch rewardConstructionTime = Stopwatch.createUnstarted();
        public Stopwatch rewardIterationTime = Stopwatch.createUnstarted();
        public int totalRewardUpdates;
    }
}
