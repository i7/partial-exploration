# Partial Exploration Tool

## Set-up Instructions

1. Clone the repository
2. Run `git submodule update --init --recursive` to populate `lib/models`
3. Run `./gradlew compileJava` to verify that the initial build succeeds.

## User Guide

Run `./gradlew distZip` to build and package the tool.
The package can be found under `build/distributions/`.
Unzip the package to a folder of your liking.
Inside, there is the `lib/` folder, which contains all the source code etc., and `bin/pet` which is the main run script for PET.

PET has three main modes of operation.

*Regular PRISM queries*: If you have a PRISM query that is supported by PET (currently reachability / safety on stochastic games), run
```
  pet prism --model <path to model.prism> --const <constants> --property <property name>
```

*Mean payoff queries*: PRISM does not support specifying a mean payoff / long run average reward query.
For these, write
```
  pet mean-payoff --model <path to model.prism> --const <constants> --reward <reward structure name> --min <min reward> --max <max reward>
```

*Core*: To compute the core of a model, run
```
  pet core --model <path to model.prism> --const <constants>
```

The output will always be a JSON object with the key `values:`, containing the result for each initial state. 

### Generic Options

The following generic options are available
* `--precision <epsilon>` sets the required precision (default `1e-6`)
* `--relative` sets relative error requirement (where sensible)
* `--global` use the complete exploration approach (instead of partial exploration)
* `--pp` pretty print the results 
* `--log LEVEL` sets the logging level (`OFF`, `INFO`, and `DEBUG` might be interesting)


## Developer Guide

Below follows some information on how to extend PET

In case you want to run some of the Python scripts, you need a recent Python installation (3.9+) capable of creating virtual environments.
Then, do the following:

1. (inside the root folder) `python -m venv venv` to create a virtual environment (you may need to write `python3` on Ubuntu/Debian systems)
2. `source venv/bin/activate` to activate the venv
3. `pip install -r scripts/requirements.txt`

### Running Tests

1. Run `scripts/make-distribution.sh` to compile and package all dependencies
2. Run `python scripts/evaluate.py` (with the venv activated), explanations below

`evaluate.py` has many options, run `evaluate.py --help` to view them.

The most important options are:
 * `--models data/models.yml` Definition of models
 * `--solvers data/solvers.yml` Definition of solvers
 * `--model <name>` Filter models with the given name
 * `--include-tag <tag>` / `--exclude-tag <tag>` / `--require-tag <tag>` Include / exclude models based on the given tag
 * `--solver <name>` Filter solver with the given name
 * `--timeout <t>` Timeout for the experiments
 * `--validate` Exit with non-zero exit code if any tool produces an invalid result (for models with known reference results)

A standard invocation would be
```
  python scripts/evaluate.py --models data/models.yml --solvers data/solvers.yml --timeout 30 --include-tag "benchmark-g" --solver PETg
```
running the solver `PETg` on all models tagged with `benchmark-g` and timeout of 30s.

### Building the Docker Image

1. Run `scripts/make-sources.sh` to package all necessary sources
2. Run `docker build -t <tag> . -f dockerfiles/Dockerfile` in root folder

This will create a docker image with the tag `<tag>`, containing the compiled sources and the executable `pet` in the PATH.

## Structure of the Repository

This artefact contains several top-level files / folders:

* `data`: All model files (in `data/models`) together with model and tool definitions (the `.yml` files in `data`).
  The organization of both the `models*.yml` files as well as the models in `data/models` is mostly arbitrary to the outside and mainly intended for maintainability (the splitting is mostly due to the source from which we obtained the models).
* `scripts`: Contains evaluation and data processing scripts.
* `src`: Contains the source files of PET
* `lib/models`: Contains the `probabilistic-models` library, which takes care of generic model representation tasks etc.

### Structure of the Code

A brief overview of the structure of PET and the library `probabilistic-models` follows.
All Java modules are within the namespace `de.tum.in`, which we omit for brevity.

* `pet.commandline` contains the different commandline interfaces / sub-commands
* `pet.global` contains the complete-exploration / global approaches to solving reachability and mean payoff
* `pet.sampling` contains the partial-exploration / sampling based approaches to solving these queries, together with several utility classes for tracking SEC-candidates etc.
* `pet.util` contains smaller utility classes
* `probmodels.cli` provides some base classes to unify commandline interfaces
* `probmodels.explorer` provides a simple implementation of a BFS-style model explorer
* `probmodels.generator` contains an abstract "first-state next-state" style model generator
* `probmodels.graph` provides several graph algorithms (mainly BSCC / MEC detection)
* `probmodels.impl` provides specific implementations of abstract interfaces (in particular for the abstract model generator and problem representation to interface with PRISM)
* `probmodels.model` contains all model-representation classes (e.g. Distribution, MDP, etc.)
* `probmodels.output` contains some simple utility classes to unify the output
* `probmodels.problem` contains abstract representations of queries (e.g. threshold reachability, maximal mean payoff, etc.)
* `probmodels.util` contains minor utility classes
* `probmodels.values` contains small utility classes to work with value intervals
