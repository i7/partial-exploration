pluginManagement {
    repositories {
        mavenCentral()
        gradlePluginPortal()
    }
}

rootProject.name = "pet"
include("lib:models")
include("lib:models:lib:parser")