# docker build -t gitlab.lrz.de:5005/i7/partial-exploration/ci --compress -f ci.Dockerfile .

FROM eclipse-temurin:22 as jre-build
RUN $JAVA_HOME/bin/jlink \
         --add-modules ALL-MODULE-PATH \
         --strip-debug \
         --no-man-pages \
         --no-header-files \
         --compress=1 \
         --output /javaruntime

FROM debian:bookworm-slim

ENV JAVA_HOME=/opt/java/openjdk
ENV PATH "${JAVA_HOME}/bin:${PATH}"
COPY --from=jre-build /javaruntime $JAVA_HOME

RUN apt-get update && apt-get install -y python3-yaml python3-tabulate && rm -rf /var/lib/apt/lists/*