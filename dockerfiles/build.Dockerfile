# docker build -t gitlab.lrz.de:5005/i7/partial-exploration/build --compress -f build.Dockerfile .

FROM debian:bookworm-slim

RUN apt-get update && apt-get install -y \
    python3-minimal \
    openjdk-17-jdk-headless \
  && rm -rf /var/lib/apt/lists/*