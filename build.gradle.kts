plugins {
    java
    distribution
    application

    pmd
    idea

    // https://plugins.gradle.org/plugin/com.diffplug.spotless
    id("com.diffplug.spotless") version "6.25.0"
}

group = "de.tum.in"
version = "2.0"

base {
    archivesName.set("pet")
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17

    withSourcesJar()
    withJavadocJar()
}

tasks.test {
    useJUnitPlatform()
    systemProperty("java.library.path", project.layout.buildDirectory.dir("lib"))
}

tasks.javadoc {
    (options as StandardJavadocDocletOptions).addBooleanOption("Xdoclint:none", true)
}

idea {
    module {
        isDownloadJavadoc = true
        isDownloadSources = true

        sourceDirs.add(project.layout.projectDirectory.dir("scripts").asFile)
    }
}

repositories {
    mavenCentral()
}

val modelsProject = ":lib:models"
dependencies {
    implementation(project(modelsProject))
    implementation("org.tinylog:tinylog-impl:2.6.2")
}

spotless {
    java {
        palantirJavaFormat()
    }
    groovyGradle {
        greclipse()
    }
}

// PMD
// https://docs.gradle.org/current/dsl/org.gradle.api.plugins.quality.Pmd.html

pmd {
    toolVersion = "7.1.0" // https://pmd.github.io/
    reportsDir = project.layout.buildDirectory.dir("reports/pmd").get().asFile
    ruleSetFiles = project.layout.projectDirectory.files("config/pmd-rules.xml")
    ruleSets = listOf() // We specify all rules in rules.xml
    isConsoleOutput = false
    isIgnoreFailures = false
}

tasks.withType<Pmd> {
    reports {
        xml.required.set(false)
        html.required.set(true)
    }
}

application {
    mainClass.set("de.tum.in.pet.Main")
}

val startScripts = tasks.getByName("startScripts", CreateStartScripts::class)

tasks.register("extractTemplate") {
    rootProject.layout.buildDirectory.asFile.get().mkdirs()
    rootProject.layout.buildDirectory.file("template-unix.txt").get().asFile.writeText(
        (startScripts.unixStartScriptGenerator as TemplateBasedScriptGenerator).template.asString()
    )
}

startScripts.doFirst {
    (startScripts.unixStartScriptGenerator as TemplateBasedScriptGenerator).template =
        project.resources.text.fromFile(rootProject.layout.projectDirectory.file("config/template-unix.txt"))
}

distributions {
    main {
    }
}